/*
Navicat MySQL Data Transfer

Source Server         : cooldreamer.com
Source Server Version : 50163
Source Host           : cooldreamer.com:3306
Source Database       : 5ihelp

Target Server Type    : MYSQL
Target Server Version : 50163
File Encoding         : 65001

Date: 2016-11-06 14:35:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `aws_active_data`
-- ----------------------------
DROP TABLE IF EXISTS `aws_active_data`;
CREATE TABLE `aws_active_data` (
  `active_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `expire_time` int(10) DEFAULT NULL,
  `active_code` varchar(32) DEFAULT NULL,
  `active_type_code` varchar(16) DEFAULT NULL,
  `add_time` int(10) DEFAULT NULL,
  `add_ip` bigint(12) DEFAULT NULL,
  `active_time` int(10) DEFAULT NULL,
  `active_ip` bigint(12) DEFAULT NULL,
  PRIMARY KEY (`active_id`),
  KEY `active_code` (`active_code`),
  KEY `active_type_code` (`active_type_code`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_active_data
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_answer`
-- ----------------------------
DROP TABLE IF EXISTS `aws_answer`;
CREATE TABLE `aws_answer` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '回答id',
  `question_id` int(11) NOT NULL COMMENT '问题id',
  `answer_content` text COMMENT '回答内容',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  `against_count` int(11) NOT NULL DEFAULT '0' COMMENT '反对人数',
  `agree_count` int(11) NOT NULL DEFAULT '0' COMMENT '支持人数',
  `uid` int(11) DEFAULT '0' COMMENT '发布问题用户ID',
  `comment_count` int(11) DEFAULT '0' COMMENT '评论总数',
  `uninterested_count` int(11) DEFAULT '0' COMMENT '不感兴趣',
  `thanks_count` int(11) DEFAULT '0' COMMENT '感谢数量',
  `category_id` int(11) DEFAULT '0' COMMENT '分类id',
  `has_attach` tinyint(1) DEFAULT '0' COMMENT '是否存在附件',
  `ip` bigint(11) DEFAULT NULL,
  `force_fold` tinyint(1) DEFAULT '0' COMMENT '强制折叠',
  `anonymous` tinyint(1) DEFAULT '0',
  `publish_source` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `question_id` (`question_id`),
  KEY `agree_count` (`agree_count`),
  KEY `against_count` (`against_count`),
  KEY `add_time` (`add_time`),
  KEY `uid` (`uid`),
  KEY `uninterested_count` (`uninterested_count`),
  KEY `force_fold` (`force_fold`),
  KEY `anonymous` (`anonymous`),
  KEY `publich_source` (`publish_source`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='回答';

-- ----------------------------
-- Records of aws_answer
-- ----------------------------
INSERT INTO `aws_answer` VALUES ('1', '1', '呵呵。新域名还在备案中，所有这个很多东西都没管，图片是没开权限', '1477788491', '0', '0', '2', '2', '0', '0', '2', '0', '249266804', '0', '0', null);

-- ----------------------------
-- Table structure for `aws_answer_comments`
-- ----------------------------
DROP TABLE IF EXISTS `aws_answer_comments`;
CREATE TABLE `aws_answer_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `answer_id` int(11) DEFAULT '0',
  `uid` int(11) DEFAULT '0',
  `message` text,
  `time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `answer_id` (`answer_id`),
  KEY `time` (`time`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_answer_comments
-- ----------------------------
INSERT INTO `aws_answer_comments` VALUES ('1', '1', '4', '多久了，我也要备案。怕太久。', '1478168701');
INSERT INTO `aws_answer_comments` VALUES ('2', '1', '2', '很久了。。。', '1478231150');

-- ----------------------------
-- Table structure for `aws_answer_thanks`
-- ----------------------------
DROP TABLE IF EXISTS `aws_answer_thanks`;
CREATE TABLE `aws_answer_thanks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `answer_id` int(11) DEFAULT '0',
  `user_name` varchar(255) DEFAULT NULL,
  `time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `answer_id` (`answer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_answer_thanks
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_answer_uninterested`
-- ----------------------------
DROP TABLE IF EXISTS `aws_answer_uninterested`;
CREATE TABLE `aws_answer_uninterested` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `answer_id` int(11) DEFAULT '0',
  `user_name` varchar(255) DEFAULT NULL,
  `time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `answer_id` (`answer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_answer_uninterested
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_answer_vote`
-- ----------------------------
DROP TABLE IF EXISTS `aws_answer_vote`;
CREATE TABLE `aws_answer_vote` (
  `voter_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自动ID',
  `answer_id` int(11) DEFAULT NULL COMMENT '回复id',
  `answer_uid` int(11) DEFAULT NULL COMMENT '回复作者id',
  `vote_uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  `vote_value` tinyint(4) NOT NULL COMMENT '-1反对 1 支持',
  `reputation_factor` int(10) DEFAULT '0',
  PRIMARY KEY (`voter_id`),
  KEY `answer_id` (`answer_id`),
  KEY `vote_value` (`vote_value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_answer_vote
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_approval`
-- ----------------------------
DROP TABLE IF EXISTS `aws_approval`;
CREATE TABLE `aws_approval` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `type` varchar(16) DEFAULT NULL,
  `data` mediumtext NOT NULL,
  `uid` int(11) NOT NULL DEFAULT '0',
  `time` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `uid` (`uid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_approval
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_article`
-- ----------------------------
DROP TABLE IF EXISTS `aws_article`;
CREATE TABLE `aws_article` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text,
  `comments` int(10) DEFAULT '0',
  `views` int(10) DEFAULT '0',
  `add_time` int(10) DEFAULT NULL,
  `has_attach` tinyint(1) NOT NULL DEFAULT '0',
  `lock` int(1) NOT NULL DEFAULT '0',
  `votes` int(10) DEFAULT '0',
  `title_fulltext` text,
  `category_id` int(10) DEFAULT '0',
  `is_recommend` tinyint(1) DEFAULT '0',
  `chapter_id` int(10) unsigned DEFAULT NULL,
  `sort` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `has_attach` (`has_attach`),
  KEY `uid` (`uid`),
  KEY `comments` (`comments`),
  KEY `views` (`views`),
  KEY `add_time` (`add_time`),
  KEY `lock` (`lock`),
  KEY `votes` (`votes`),
  KEY `category_id` (`category_id`),
  KEY `is_recommend` (`is_recommend`),
  KEY `chapter_id` (`chapter_id`),
  KEY `sort` (`sort`),
  FULLTEXT KEY `title_fulltext` (`title_fulltext`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_article
-- ----------------------------
INSERT INTO `aws_article` VALUES ('1', '1', 'PHP', 'PHP原始为Personal Home Page的缩写，已经正式更名为 &quot;PHP: Hypertext Preprocessor&quot;。注意不是“Hypertext Preprocessor”的缩写，这种将名称放到定义中的写法被称作递归缩写。PHP于1994年由Rasmus Lerdorf创建，刚刚开始是Rasmus Lerdorf为了要维护个人网页而制作的一个简单的用Perl语言编写的程序。这些工具程序用来显示 Rasmus Lerdorf 的个人履历，以及统计网页流量。后来又用C语言重新编写，包括可以访问数据库。他将这些程序和一些表单直译器整合起来，称为 PHP/FI。PHP/FI 可以和数据库连接，产生简单的动态网页程序。\n在1995年以Personal Home Page Tools (PHP Tools) 开始对外发表第一个版本，Lerdorf写了一些介绍此程序的文档。并且发布了PHP1.0！在这的版本中，提供了访客留言本、访客计数器等简单的功能。以后越来越多的网站使用了PHP，并且强烈要求增加一些特性。比如循环语句和数组变量等等；在新的成员加入开发行列之后，Rasmus Lerdorf 在1995年6月8日将 PHP/FI 公开发布，希望可以透过社群来加速程序开发与寻找错误。这个发布的版本命名为 PHP 2，已经有 PHP 的一些雏型，像是类似 Perl的变量命名方式、表单处理功能、以及嵌入到 HTML 中执行的能力。程序语法上也类似 Perl，有较多的限制，不过更简单、更有弹性。PHP/FI加入了对MySQL的支持，从此建立了PHP在动态网页开发上的地位。到了1996年底，有15000个网站使用 PHP/FI。\n[ISAPI筛选器] ISAPI筛选器\n在1997年，任职于 Technion IIT公司的两个以色列程序设计师：Zeev Suraski 和 Andi Gutmans，重写了 PHP 的剖析器，成为 PHP 3 的基础。而 PHP 也在这个时候改称为PHP：Hypertext Preprocessor。经过几个月测试，开发团队在1997年11月发布了 PHP/FI 2。随后就开始 PHP 3 的开放测试，最后在1998年6月正式发布 PHP 3。Zeev Suraski 和 Andi Gutmans 在 PHP 3 发布后开始改写PHP 的核心，这个在1999年发布的剖析器称为 Zend Engine，他们也在以色列的 Ramat Gan 成立了 Zend Technologies 来管理 PHP 的开发。\n在2000年5月22日，以Zend Engine 1.0为基础的PHP 4正式发布，2004年7月13日则发布了PHP 5，PHP 5则使用了第二代的Zend Engine。PHP包含了许多新特色，像是强化的面向对象功能、引入PDO（PHP Data Objects，一个存取数据库的延伸函数库）、以及许多效能上的增强。PHP 4已经不会继续\n[PHP] PHP\n更新，以鼓励用户转移到PHP 5。\n2008年PHP 5成为了PHP唯一的有在开发的PHP版本。将来的PHP 5.3将会加入Late static binding和一些其他的功能强化。PHP 6 的开发也正在进行中，主要的改进有移除register_globals、magic quotes 和 Safe mode的功能。\nPHP最新稳定版本：5.4.30(2013.6.26)\nPHP最新发布的正式版本：5.5.14(2014.6.24)\nPHP最新测试版本：5.6.0 RC2(2014.6.03)\n2013年6月20日，PHP开发团队自豪地宣布推出PHP 5.5.0。此版本包含了大量的新功能和bug修复。需要开发者特别注意的一点是不再支持 Windows XP 和 2003 系统。\n2014年10月16日，PHP开发团队宣布PHP 5.6.2可用。四安全相关的错误是固定在这个版本，包括修复cve-2014-3668，cve-2014-3669和cve-2014-3670。所有的PHP 5.6鼓励用户升级到这个版本。\n ', '0', '6', '1477553402', '0', '0', '0', 'php', '2', '0', null, '0');
INSERT INTO `aws_article` VALUES ('2', '2', 'wecenter 文章的上一篇，下一篇', '以下为以前写的上一篇，下一篇现帖出来，如果有想法或者问题，欢迎评论\n文章\n1，改程序文件\n找开app/article/main.php\n\n 在$article_info[\'message\'] = FORMAT::parse_attachs(nl2br(FORMAT::parse_bbcode($article_info[\'message\'])));后面加上\n// 上一页\n        $last_pagesdb = $this-&gt;model(\'article\')-&gt;lastpages();\n        //下一页\n        $next_pagesdb =$this-&gt;model(\'article\')-&gt;nextpages();\n\n找到models/article.php  加上后面两句\n\n// 上一页\n    public function lastpages(){\n        // $this-&gt;dao-&gt;where(&quot;catid=$data[catid] and id&lt;$id&quot;)-&gt;order(\'id desc\')-&gt;find();\n         $id = intval($_GET[\'id\']);\n        $sql = &quot;SELECT id,title FROM &quot; . get_table(\'article\').&quot; WHERE id&lt; &quot;.$id.&quot; ORDER BY id DESC LIMIT 1&quot;;\n        $result = $this-&gt;query_all($sql);\n        if($result){\n            return $result;\n        }else{\n            return $result=false;\n        }\n    }\n==================================================================================================\n    // 下一页\n    public function nextpages(){\n         $id = intval($_GET[\'id\']);\n        $sql = &quot;SELECT id,title FROM &quot; . get_table(\'article\').&quot; WHERE id&gt; &quot;.$id.&quot; ORDER BY id ASC LIMIT 1&quot;;\n        $result = $this-&gt;query_all($sql);\n        if($result){\n            return $result;\n        }else{\n            return $result=false;\n        }\n    }\n==================================================================================================\n模板可以用&lt;?php echo $article_info[\'next_pagesdb\'] ?&gt;//调用。\n里面有两个值一个是title  一个是id  直接ymf调取就行', '0', '8', '1477575772', '0', '0', '0', 'wecenter 2599131456 1996831687', '6', '0', null, '0');
INSERT INTO `aws_article` VALUES ('3', '2', '把WECENTER安装在BBS或者其它二级目录，管理员登录返回地址错误解决方法 ', '[quote]\n找到/system/functions.inc.php\n第48行，把\nfunction base64_current_path()\n{\n  return base64_encode(\'/\' . str_replace(\'/\' . G_INDEX_SCRIPT, \'\', substr($_SERVER[\'REQUEST_URI\'], strlen(dirname($_SERVER[\'PHP_SELF\'])))));\n}\n改成\nfunction base64_current_path()\n{\n    return base64_encode($_SERVER[\'REQUEST_URI\']);\n}\n改了，暂时没有发现问题，如果有问题可以反缀\n[/quote]\n', '0', '5', '1477618858', '0', '0', '0', 'wecenter 3501322312 bbs 2010832423 3044624405 316492970221592 3033124405 3682022238 2232022336 3816935823 3529920915 2604127861', '6', '0', null, '0');
INSERT INTO `aws_article` VALUES ('4', '2', 'wecenter指导入文章或者问答', '//菜单\n找到system/config/admin_menu.php - 这个文件控制你的后台菜单在你想要的位置加入到菜单\n我是加到了 工具这个菜单下面\n[quote]\n$config[] = array(\n    \'title\' =&gt; AWS_APP::lang()-&gt;_t(\'工具\'),\n    \'cname\' =&gt; \'job\',\n    \'children\' =&gt; array(\n        array(\n            \'id\' =&gt; 501,\n            \'title\' =&gt; AWS_APP::lang()-&gt;_t(\'系统维护\'),\n            \'url\' =&gt; \'admin/tools/\',\n        )\n        ,\n\n        array(\n            \'id\' =&gt; 5002,\n            \'title\' =&gt; AWS_APP::lang()-&gt;_t(\'导入数据\'),\n            \'url\' =&gt; \'admin/import/\'\n        )\n    )\n);\n[/quote]\n[quote]\n//导入核心文件Phpexcel我是放在system\n[/quote]\n[quote]\n新建立一个 app/admin/import.php 的控制器\n[/quote]\n[quote]\n views/default/admin/import.tpl.htm 后台管理页面\n[/quote]\n\n* views/default/admin/data_import_progress.tpl.htm - View 处理进程显示页面\n* uploads/data_import - 上传文件放置目录\n* uploads/data_import/data_import_demo.xlsx - Excel演示文件格式', '0', '9', '1477635297', '0', '0', '0', 'wecenter 2354820837 2599131456 3838231572', '6', '0', null, '0');
INSERT INTO `aws_article` VALUES ('5', '2', '指定某个分组才可以访问文章', '//指定某个分组才可以访问文章 --》可以用做VIP文章或者章节\n==================\n首先找到/views/default/publish/article.tpl.htm\n在约48行，也就是 &lt;!-- end 文章标题 --&gt; 下面加入\n[quote]\n&lt;!-- 指定分组  -By Jerry 2016.11.1--&gt;\n                                &lt;h3&gt;&lt;?php _e(\'文章标题\'); ?&gt;:&lt;/h3&gt;\n                                &lt;?php if ($this-&gt;groups) { ?&gt;\n                                &lt;div class=&quot;aw-publish-title&quot;&gt;\n                                    &lt;select name=&quot;power_group&quot; id=&quot;&quot; class=&quot;form-control&quot;&gt;\n                                    &lt;option value=&quot;0&quot;&gt;不指定用户组&lt;/option&gt;\n                                    &lt;?php foreach ($this-&gt;groups as $key =&gt; $v): ?&gt;\n                                        &lt;option value=&quot;&lt;?php _e($v[\'group_id\']) ?&gt;&quot;&gt;&lt;?php _e($v[\'group_name\']) ?&gt;&lt;/option&gt;\n                                        &lt;?php endforeach ?&gt;\n                                    &lt;/select&gt;\n                                    \n                                &lt;/div&gt;\n                                &lt;?php } ?&gt;\n                                    &lt;!-- end 指定分组  -By Jerry 2016.11.1--&gt;\n[/quote]\n===================================================\n找到/app/publish/main.php\n在约 18行  加入\n[quote]\n//用户组查询 S  -By Jerry 2016.11.1\n        $groups = $this-&gt;model(\'usergroup\')-&gt;get_groups();\n        TPL::assign(\'groups\', $groups);\n        // var_dump($groups);\n        //用户组查询 E  -By Jerry 2016.11.1\n       \n \n[/quote]\n找到/app/publish/ajax.php 把publish_article_action方法改为改为\n[quote]\n public function publish_article_action()\n    {\n        if (!$this-&gt;user_info[\'permission\'][\'publish_article\'])\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'你没有权限发布文章\')));\n        }\n\n        if (!$_POST[\'title\'])\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, - 1, AWS_APP::lang()-&gt;_t(\'请输入文章标题\')));\n        }\n\n        if (get_setting(\'category_enable\') == \'N\')\n        {\n            $_POST[\'category_id\'] = 1;\n        }\n\n        if (!$_POST[\'category_id\'])\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, -1, AWS_APP::lang()-&gt;_t(\'请选择文章分类\')));\n        }\n\n        if (get_setting(\'question_title_limit\') &gt; 0 AND cjk_strlen($_POST[\'title\']) &gt; get_setting(\'question_title_limit\'))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'文章标题字数不得大于 %s 字节\', get_setting(\'question_title_limit\'))));\n        }\n\n        if (!$this-&gt;user_info[\'permission\'][\'publish_url\'] AND FORMAT::outside_url_exists($_POST[\'message\']))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'你所在的用户组不允许发布站外链接\')));\n        }\n\n        if (!$this-&gt;model(\'publish\')-&gt;insert_attach_is_self_upload($_POST[\'message\'], $_POST[\'attach_ids\']))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'只允许插入当前页面上传的附件\')));\n        }\n\n        if (human_valid(\'question_valid_hour\') AND !AWS_APP::captcha()-&gt;is_validate($_POST[\'seccode_verify\']))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'请填写正确的验证码\')));\n        }\n\n        if ($_POST[\'topics\'])\n        {\n            foreach ($_POST[\'topics\'] AS $key =&gt; $topic_title)\n            {\n                $topic_title = trim($topic_title);\n\n                if (!$topic_title)\n                {\n                    unset($_POST[\'topics\'][$key]);\n                }\n                else\n                {\n                    $_POST[\'topics\'][$key] = $topic_title;\n                }\n            }\n\n            if (get_setting(\'question_topics_limit\') AND sizeof($_POST[\'topics\']) &gt; get_setting(\'question_topics_limit\'))\n            {\n                H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'单个文章话题数量最多为 %s 个, 请调整话题数量\', get_setting(\'question_topics_limit\'))));\n            }\n        }\n        if (get_setting(\'new_question_force_add_topic\') == \'Y\' AND !$_POST[\'topics\'])\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'请为文章添加话题\')));\n        }\n\n        // !注: 来路检测后面不能再放报错提示\n        if (!valid_post_hash($_POST[\'post_hash\']))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'页面停留时间过长,或内容已提交,请刷新页面\')));\n        }\n\n        $this-&gt;model(\'draft\')-&gt;delete_draft(1, \'article\', $this-&gt;user_id);\n\n        if ($this-&gt;publish_approval_valid(array(\n                $_POST[\'title\'],\n                $_POST[\'message\']\n            )))\n        {\n            $this-&gt;model(\'publish\')-&gt;publish_approval(\'article\', array(\n                \'title\' =&gt; $_POST[\'title\'],\n                \'message\' =&gt; $_POST[\'message\'],\n                \'category_id\' =&gt; $_POST[\'category_id\'],\n                \'topics\' =&gt; $_POST[\'topics\'],\n                \'permission_create_topic\' =&gt; $this-&gt;user_info[\'permission\'][\'create_topic\'],\n            ), $this-&gt;user_id, $_POST[\'attach_access_key\']);\n            H::ajax_json_output(AWS_APP::RSM(array(\n                \'url\' =&gt; get_js_url(\'/publish/wait_approval/\')\n            ), 1, null));\n        }\n        else\n        {\n            //changed ==by Jerry 2016.11.1\n            $article_id = $this-&gt;model(\'publish\')-&gt;publish_article($_POST[\'title\'],$_POST[\'power_group\'], $_POST[\'message\'], $this-&gt;user_id, $_POST[\'topics\'], $_POST[\'category_id\'], $_POST[\'attach_access_key\'], $this-&gt;user_info[\'permission\'][\'create_topic\']);\n             //changed ==by Jerry 2016.11.1\n\n            if ($_POST[\'_is_mobile\'])\n            {\n                $url = get_js_url(\'/m/article/\' . $article_id);\n            }\n            else\n            {\n                $url = get_js_url(\'/article/\' . $article_id);\n            }\n\n            H::ajax_json_output(AWS_APP::RSM(array(\n                \'url\' =&gt; $url\n            ), 1, null));\n        }\n    }\n[/quote]\n在models下面新建个模型，名字为usergroup.php\n[quote]\n&lt;?php\n\nif (!defined(\'IN_ANWSION\'))\n{\n    die;\n}\n\nclass usergroup_class extends AWS_MODEL\n{\n \n\n    public function get_groups()\n    {\n       \n\n        return $this-&gt;fetch_all(\'users_group\');\n\n    }\n\n   \n}\n\n \n[/quote]\n//找到models/publish.php  更改publish_article方法为\n[quote]\npublic function publish_article($title, $power_group,$message, $uid, $topics = null, $category_id = null, $attach_access_key = null, $create_topic = true)\n    {\n        if ($article_id = $this-&gt;insert(\'article\', array(\n            \'uid\' =&gt; intval($uid),\n            \'title\' =&gt; htmlspecialchars($title),\n            \'message\' =&gt; htmlspecialchars($message),\n            \'category_id\' =&gt; intval($category_id),\n             //changed ==by Jerry 2016.11.1\n            \'power_group\'=&gt;intval($power_group),\n             //changed ==by Jerry 2016.11.1\n            \'add_time\' =&gt; time()\n        )))\n        {\n            set_human_valid(\'question_valid_hour\');\n\n            if (is_array($topics))\n            {\n                foreach ($topics as $key =&gt; $topic_title)\n                {\n                    $topic_id = $this-&gt;model(\'topic\')-&gt;save_topic($topic_title, $uid, $create_topic);\n\n                    $this-&gt;model(\'topic\')-&gt;save_topic_relation($uid, $topic_id, $article_id, \'article\');\n                }\n            }\n\n            if ($attach_access_key)\n            {\n                $this-&gt;model(\'publish\')-&gt;update_attach(\'article\', $article_id, $attach_access_key);\n            }\n\n            $this-&gt;model(\'search_fulltext\')-&gt;push_index(\'article\', $title, $article_id);\n\n            // 记录日志\n            ACTION_LOG::save_action($uid, $article_id, ACTION_LOG::CATEGORY_QUESTION, ACTION_LOG::ADD_ARTICLE, $title, $message, 0);\n\n            $this-&gt;model(\'posts\')-&gt;set_posts_index($article_id, \'article\');\n\n            $this-&gt;shutdown_update(\'users\', array(\n                \'article_count\' =&gt; $this-&gt;count(\'article\', \'uid = \' . intval($uid))\n            ), \'uid = \' . intval($uid));\n        }\n\n        return $article_id;\n    }\n \n[/quote]\n加入以下代码\n[quote]\nsql升级\n \nALTER TABLE `aws_article`\nADD COLUMN `power_group`  int(11) NULL AFTER `sort`;\n[/quote]\n到此处，发布文章权限就搞定了，下面就是验证组了。\n=====================================================\n找到app/article/main.php\n把\n[quote]\n &lt;?php //echo $this-&gt;article_info[\'message\']; ?&gt;\n改成\n  &lt;?php if($this-&gt;article_info[\'user_info\'][\'group_id\']!=$article_info[\'power_group\']){ echo &quot;没有查看权限&quot;;}else{echo $this-&gt;article_info[\'message\'];} ?&gt;\n \n \n=============================\n如果想超级管理员和前台管理员不在些限制，就改成\n&lt;?php if($this-&gt;article_info[\'user_info\'][\'group_id\']!=$article_info[\'power_group\']&amp;&amp;$this-&gt;article_info[\'user_info\'][\'group_id\']!=1&amp;&amp;$this-&gt;article_info[\'user_info\'][\'group_id\']!=2){ echo &quot;没有查看权限&quot;;}else{echo $this-&gt;article_info[\'message\'];} ?&gt;\n[/quote]\n', '0', '10', '1477995072', '0', '0', '0', '2535123450 2099832452 3577538382 2599131456', '6', '0', null, '0');

-- ----------------------------
-- Table structure for `aws_article_comments`
-- ----------------------------
DROP TABLE IF EXISTS `aws_article_comments`;
CREATE TABLE `aws_article_comments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `article_id` int(10) NOT NULL,
  `message` text NOT NULL,
  `add_time` int(10) NOT NULL,
  `at_uid` int(10) DEFAULT NULL,
  `votes` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `article_id` (`article_id`),
  KEY `add_time` (`add_time`),
  KEY `votes` (`votes`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_article_comments
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_article_vote`
-- ----------------------------
DROP TABLE IF EXISTS `aws_article_vote`;
CREATE TABLE `aws_article_vote` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `type` varchar(16) DEFAULT NULL,
  `item_id` int(10) NOT NULL,
  `rating` tinyint(1) DEFAULT '0',
  `time` int(10) NOT NULL,
  `reputation_factor` int(10) DEFAULT '0',
  `item_uid` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `type` (`type`),
  KEY `item_id` (`item_id`),
  KEY `time` (`time`),
  KEY `item_uid` (`item_uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_article_vote
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_attach`
-- ----------------------------
DROP TABLE IF EXISTS `aws_attach`;
CREATE TABLE `aws_attach` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) DEFAULT NULL COMMENT '附件名称',
  `access_key` varchar(32) DEFAULT NULL COMMENT '批次 Key',
  `add_time` int(10) DEFAULT '0' COMMENT '上传时间',
  `file_location` varchar(255) DEFAULT NULL COMMENT '文件位置',
  `is_image` int(1) DEFAULT '0',
  `item_type` varchar(32) DEFAULT '0' COMMENT '关联类型',
  `item_id` bigint(20) DEFAULT '0' COMMENT '关联 ID',
  `wait_approval` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `access_key` (`access_key`),
  KEY `is_image` (`is_image`),
  KEY `fetch` (`item_id`,`item_type`),
  KEY `wait_approval` (`wait_approval`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_attach
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_category`
-- ----------------------------
DROP TABLE IF EXISTS `aws_category`;
CREATE TABLE `aws_category` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `sort` smallint(6) DEFAULT '0',
  `url_token` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `url_token` (`url_token`),
  KEY `title` (`title`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_category
-- ----------------------------
INSERT INTO `aws_category` VALUES ('1', '默认分类', 'question', null, '0', '0', null);
INSERT INTO `aws_category` VALUES ('2', 'php', 'question', null, '0', '0', null);
INSERT INTO `aws_category` VALUES ('3', 'linux', 'question', null, '0', '0', null);
INSERT INTO `aws_category` VALUES ('4', '5ihelp', 'question', null, '0', '0', null);
INSERT INTO `aws_category` VALUES ('5', '微信', 'question', null, '0', '0', null);
INSERT INTO `aws_category` VALUES ('6', 'wecenter二次开发', 'question', null, '0', '0', null);

-- ----------------------------
-- Table structure for `aws_draft`
-- ----------------------------
DROP TABLE IF EXISTS `aws_draft`;
CREATE TABLE `aws_draft` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `type` varchar(16) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `data` text,
  `time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `item_id` (`item_id`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_draft
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_edm_task`
-- ----------------------------
DROP TABLE IF EXISTS `aws_edm_task`;
CREATE TABLE `aws_edm_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `message` mediumtext NOT NULL,
  `subject` varchar(255) NOT NULL,
  `from_name` varchar(255) NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_edm_task
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_edm_taskdata`
-- ----------------------------
DROP TABLE IF EXISTS `aws_edm_taskdata`;
CREATE TABLE `aws_edm_taskdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskid` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sent_time` int(10) NOT NULL,
  `view_time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `taskid` (`taskid`),
  KEY `sent_time` (`sent_time`),
  KEY `view_time` (`view_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_edm_taskdata
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_edm_unsubscription`
-- ----------------------------
DROP TABLE IF EXISTS `aws_edm_unsubscription`;
CREATE TABLE `aws_edm_unsubscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_edm_unsubscription
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_edm_userdata`
-- ----------------------------
DROP TABLE IF EXISTS `aws_edm_userdata`;
CREATE TABLE `aws_edm_userdata` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usergroup` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usergroup` (`usergroup`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_edm_userdata
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_edm_usergroup`
-- ----------------------------
DROP TABLE IF EXISTS `aws_edm_usergroup`;
CREATE TABLE `aws_edm_usergroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_edm_usergroup
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_education_experience`
-- ----------------------------
DROP TABLE IF EXISTS `aws_education_experience`;
CREATE TABLE `aws_education_experience` (
  `education_id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '用户UID',
  `education_years` int(11) DEFAULT NULL COMMENT '入学年份',
  `school_name` varchar(64) DEFAULT NULL COMMENT '学校名',
  `school_type` tinyint(4) DEFAULT NULL COMMENT '学校类别',
  `departments` varchar(64) DEFAULT NULL COMMENT '院系',
  `add_time` int(10) DEFAULT NULL COMMENT '记录添加时间',
  PRIMARY KEY (`education_id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='教育经历';

-- ----------------------------
-- Records of aws_education_experience
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_favorite`
-- ----------------------------
DROP TABLE IF EXISTS `aws_favorite`;
CREATE TABLE `aws_favorite` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `item_id` int(11) DEFAULT '0',
  `time` int(10) DEFAULT '0',
  `type` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `time` (`time`),
  KEY `item_id` (`item_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_favorite
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_favorite_tag`
-- ----------------------------
DROP TABLE IF EXISTS `aws_favorite_tag`;
CREATE TABLE `aws_favorite_tag` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `title` varchar(128) DEFAULT NULL,
  `item_id` int(11) DEFAULT '0',
  `type` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `title` (`title`),
  KEY `type` (`type`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_favorite_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_feature`
-- ----------------------------
DROP TABLE IF EXISTS `aws_feature`;
CREATE TABLE `aws_feature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) DEFAULT NULL COMMENT '专题标题',
  `description` varchar(255) DEFAULT NULL COMMENT '专题描述',
  `icon` varchar(255) DEFAULT NULL COMMENT '专题图标',
  `topic_count` int(11) NOT NULL DEFAULT '0' COMMENT '话题计数',
  `css` text COMMENT '自定义CSS',
  `url_token` varchar(32) DEFAULT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `url_token` (`url_token`),
  KEY `title` (`title`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_feature
-- ----------------------------
INSERT INTO `aws_feature` VALUES ('1', '5ihelp', '5ihelp管理系统', null, '0', '', '5ihelp', '5ihelp管理系统', '0');

-- ----------------------------
-- Table structure for `aws_feature_topic`
-- ----------------------------
DROP TABLE IF EXISTS `aws_feature_topic`;
CREATE TABLE `aws_feature_topic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feature_id` int(11) NOT NULL DEFAULT '0' COMMENT '专题ID',
  `topic_id` int(11) NOT NULL DEFAULT '0' COMMENT '话题ID',
  PRIMARY KEY (`id`),
  KEY `feature_id` (`feature_id`),
  KEY `topic_id` (`topic_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_feature_topic
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_geo_location`
-- ----------------------------
DROP TABLE IF EXISTS `aws_geo_location`;
CREATE TABLE `aws_geo_location` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `item_type` varchar(32) NOT NULL,
  `item_id` int(10) NOT NULL,
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  `add_time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `item_type` (`item_type`),
  KEY `add_time` (`add_time`),
  KEY `geo_location` (`latitude`,`longitude`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_geo_location
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_help_chapter`
-- ----------------------------
DROP TABLE IF EXISTS `aws_help_chapter`;
CREATE TABLE `aws_help_chapter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `url_token` varchar(32) DEFAULT NULL,
  `sort` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `url_token` (`url_token`),
  KEY `sort` (`sort`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='帮助中心';

-- ----------------------------
-- Records of aws_help_chapter
-- ----------------------------
INSERT INTO `aws_help_chapter` VALUES ('1', '5ihelp', '5ihelp操作指南', '5ihelp', '0');

-- ----------------------------
-- Table structure for `aws_inbox`
-- ----------------------------
DROP TABLE IF EXISTS `aws_inbox`;
CREATE TABLE `aws_inbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL COMMENT '发送者 ID',
  `dialog_id` int(11) DEFAULT NULL COMMENT '对话id',
  `message` text COMMENT '内容',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  `sender_remove` tinyint(1) DEFAULT '0',
  `recipient_remove` tinyint(1) DEFAULT '0',
  `receipt` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `dialog_id` (`dialog_id`),
  KEY `uid` (`uid`),
  KEY `add_time` (`add_time`),
  KEY `sender_remove` (`sender_remove`),
  KEY `recipient_remove` (`recipient_remove`),
  KEY `sender_receipt` (`receipt`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_inbox
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_inbox_dialog`
-- ----------------------------
DROP TABLE IF EXISTS `aws_inbox_dialog`;
CREATE TABLE `aws_inbox_dialog` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '对话ID',
  `sender_uid` int(11) DEFAULT NULL COMMENT '发送者UID',
  `sender_unread` int(11) DEFAULT NULL COMMENT '发送者未读',
  `recipient_uid` int(11) DEFAULT NULL COMMENT '接收者UID',
  `recipient_unread` int(11) DEFAULT NULL COMMENT '接收者未读',
  `add_time` int(11) DEFAULT NULL COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL COMMENT '最后更新时间',
  `sender_count` int(11) DEFAULT NULL COMMENT '发送者显示对话条数',
  `recipient_count` int(11) DEFAULT NULL COMMENT '接收者显示对话条数',
  PRIMARY KEY (`id`),
  KEY `recipient_uid` (`recipient_uid`),
  KEY `sender_uid` (`sender_uid`),
  KEY `update_time` (`update_time`),
  KEY `add_time` (`add_time`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_inbox_dialog
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_integral_log`
-- ----------------------------
DROP TABLE IF EXISTS `aws_integral_log`;
CREATE TABLE `aws_integral_log` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `action` varchar(16) DEFAULT NULL,
  `integral` int(11) DEFAULT NULL,
  `note` varchar(128) DEFAULT NULL,
  `balance` int(11) DEFAULT '0',
  `item_id` int(11) DEFAULT '0',
  `time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `action` (`action`),
  KEY `time` (`time`),
  KEY `integral` (`integral`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_integral_log
-- ----------------------------
INSERT INTO `aws_integral_log` VALUES ('1', '1', 'REGISTER', '2000', '初始资本', '2000', '0', '1477537163');
INSERT INTO `aws_integral_log` VALUES ('2', '1', 'UPLOAD_AVATAR', '20', '上传头像', '2020', '0', '1477554430');
INSERT INTO `aws_integral_log` VALUES ('3', '2', 'REGISTER', '2000', '初始资本', '2000', '0', '1477574537');
INSERT INTO `aws_integral_log` VALUES ('5', '2', 'UPLOAD_AVATAR', '20', '上传头像', '2020', '0', '1477575604');
INSERT INTO `aws_integral_log` VALUES ('6', '2', 'BIND_OPENID', '20', '绑定 OPEN ID', '2040', '0', '1477577891');
INSERT INTO `aws_integral_log` VALUES ('7', '4', 'REGISTER', '2000', '初始资本', '2000', '0', '1477724347');
INSERT INTO `aws_integral_log` VALUES ('8', '4', 'NEW_QUESTION', '-20', '发起问题 #1', '1980', '1', '1477724625');
INSERT INTO `aws_integral_log` VALUES ('9', '2', 'ANSWER_QUESTION', '-5', '回答问题 #1', '2035', '1', '1477788491');
INSERT INTO `aws_integral_log` VALUES ('10', '4', 'QUESTION_ANSWER', '5', '问题被回答 #1', '1985', '1', '1477788492');
INSERT INTO `aws_integral_log` VALUES ('11', '5', 'REGISTER', '2000', '初始资本', '2000', '0', '1477875258');

-- ----------------------------
-- Table structure for `aws_invitation`
-- ----------------------------
DROP TABLE IF EXISTS `aws_invitation`;
CREATE TABLE `aws_invitation` (
  `invitation_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '激活ID',
  `uid` int(11) DEFAULT '0' COMMENT '用户ID',
  `invitation_code` varchar(32) DEFAULT NULL COMMENT '激活码',
  `invitation_email` varchar(255) DEFAULT NULL COMMENT '激活email',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  `add_ip` bigint(12) DEFAULT NULL COMMENT '添加IP',
  `active_expire` tinyint(1) DEFAULT '0' COMMENT '激活过期',
  `active_time` int(10) DEFAULT NULL COMMENT '激活时间',
  `active_ip` bigint(12) DEFAULT NULL COMMENT '激活IP',
  `active_status` tinyint(4) DEFAULT '0' COMMENT '1已使用0未使用-1已删除',
  `active_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`invitation_id`),
  KEY `uid` (`uid`),
  KEY `invitation_code` (`invitation_code`),
  KEY `invitation_email` (`invitation_email`),
  KEY `active_time` (`active_time`),
  KEY `active_ip` (`active_ip`),
  KEY `active_status` (`active_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_invitation
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_jobs`
-- ----------------------------
DROP TABLE IF EXISTS `aws_jobs`;
CREATE TABLE `aws_jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_name` varchar(64) DEFAULT NULL COMMENT '职位名',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_jobs
-- ----------------------------
INSERT INTO `aws_jobs` VALUES ('1', '销售');
INSERT INTO `aws_jobs` VALUES ('2', '市场/市场拓展/公关');
INSERT INTO `aws_jobs` VALUES ('3', '商务/采购/贸易');
INSERT INTO `aws_jobs` VALUES ('4', '计算机软、硬件/互联网/IT');
INSERT INTO `aws_jobs` VALUES ('5', '电子/半导体/仪表仪器');
INSERT INTO `aws_jobs` VALUES ('6', '通信技术');
INSERT INTO `aws_jobs` VALUES ('7', '客户服务/技术支持');
INSERT INTO `aws_jobs` VALUES ('8', '行政/后勤');
INSERT INTO `aws_jobs` VALUES ('9', '人力资源');
INSERT INTO `aws_jobs` VALUES ('10', '高级管理');
INSERT INTO `aws_jobs` VALUES ('11', '生产/加工/制造');
INSERT INTO `aws_jobs` VALUES ('12', '质控/安检');
INSERT INTO `aws_jobs` VALUES ('13', '工程机械');
INSERT INTO `aws_jobs` VALUES ('14', '技工');
INSERT INTO `aws_jobs` VALUES ('15', '财会/审计/统计');
INSERT INTO `aws_jobs` VALUES ('16', '金融/银行/保险/证券/投资');
INSERT INTO `aws_jobs` VALUES ('17', '建筑/房地产/装修/物业');
INSERT INTO `aws_jobs` VALUES ('18', '交通/仓储/物流');
INSERT INTO `aws_jobs` VALUES ('19', '普通劳动力/家政服务');
INSERT INTO `aws_jobs` VALUES ('20', '零售业');
INSERT INTO `aws_jobs` VALUES ('21', '教育/培训');
INSERT INTO `aws_jobs` VALUES ('22', '咨询/顾问');
INSERT INTO `aws_jobs` VALUES ('23', '学术/科研');
INSERT INTO `aws_jobs` VALUES ('24', '法律');
INSERT INTO `aws_jobs` VALUES ('25', '美术/设计/创意');
INSERT INTO `aws_jobs` VALUES ('26', '编辑/文案/传媒/影视/新闻');
INSERT INTO `aws_jobs` VALUES ('27', '酒店/餐饮/旅游/娱乐');
INSERT INTO `aws_jobs` VALUES ('28', '化工');
INSERT INTO `aws_jobs` VALUES ('29', '能源/矿产/地质勘查');
INSERT INTO `aws_jobs` VALUES ('30', '医疗/护理/保健/美容');
INSERT INTO `aws_jobs` VALUES ('31', '生物/制药/医疗器械');
INSERT INTO `aws_jobs` VALUES ('32', '翻译（口译与笔译）');
INSERT INTO `aws_jobs` VALUES ('33', '公务员');
INSERT INTO `aws_jobs` VALUES ('34', '环境科学/环保');
INSERT INTO `aws_jobs` VALUES ('35', '农/林/牧/渔业');
INSERT INTO `aws_jobs` VALUES ('36', '兼职/临时/培训生/储备干部');
INSERT INTO `aws_jobs` VALUES ('37', '在校学生');
INSERT INTO `aws_jobs` VALUES ('38', '其他');

-- ----------------------------
-- Table structure for `aws_mail_queue`
-- ----------------------------
DROP TABLE IF EXISTS `aws_mail_queue`;
CREATE TABLE `aws_mail_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `send_to` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `is_error` tinyint(1) NOT NULL DEFAULT '0',
  `error_message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `is_error` (`is_error`),
  KEY `send_to` (`send_to`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_mail_queue
-- ----------------------------
INSERT INTO `aws_mail_queue` VALUES ('1', '85277s7213@qq.com', 'Jerrychen 在 5ihelp 上给您发送了私信', '<html style=\"border: medium none;font-size: 1em; list-style: none outside none; margin: 0; outline: medium none; padding: 0; text-decoration: none;\">\r\n<div class=\"wrapper\"  style=\"margin: 20px auto 0; width: 500px; padding-top:16px; padding-bottom:10px;\">\r\n<div class=\"header clearfix\">\r\n<a class=\"logo\" href=\"http://www.cooldreamer.com/ask\" target=\"_blank\"><b>5ihelp</b></a>\r\n</div>\r\n<br style=\"clear:both; height:0\" />\r\n<div class=\"content\" style=\"background: none repeat scroll 0 0 #FFFFFF; border: 1px solid #E9E9E9; margin: 2px 0 0; padding: 30px;\">\r\n\r\n<p>Jerrychen，您好: </p>\r\n\r\n<p>Jerrychen 在 5ihelp 上给您发送了私信<br /><br />----- 私信内容 -----<br /><br />尊敬的Jerrychen，您已经注册成为5ihelp的会员，请您在发表言论时，遵守当地法律法规。<br />\n如果您有什么疑问可以联系管理员。<br />\n<br />\n5ihelp<br /><br /></p>\r\n\r\n<p style=\"border-top: 1px solid #DDDDDD;margin: 15px 0 25px;padding: 15px;\">\r\n请点击链接继续: <a href=\"http://www.cooldreamer.com/ask/?/inbox/\" target=\"_blank\">http://www.cooldreamer.com/ask/?/inbox/</a>\r\n<p>\r\n\r\n<p class=\"footer\" style=\"border-top: 1px solid #DDDDDD; padding-top:6px; margin-top:25px; color:#838383;\">请勿回复本邮件, 此邮箱未受监控, 您不会得到任何回复. 要获得帮助, 请登录网站 | <a href=\"http://www.cooldreamer.com/ask/?/account/setting/privacy/\" target=\"_blank\">修改通知设置</a><br /><br /><a href=\"http://www.cooldreamer.com/ask\" target=\"_blank\">5ihelp</a></p>\r\n</div>\r\n</div>\r\n</html>', '0', null);

-- ----------------------------
-- Table structure for `aws_nav_menu`
-- ----------------------------
DROP TABLE IF EXISTS `aws_nav_menu`;
CREATE TABLE `aws_nav_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(128) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(16) DEFAULT NULL,
  `type_id` int(11) DEFAULT '0',
  `link` varchar(255) DEFAULT NULL COMMENT '链接',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `sort` smallint(6) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`link`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_nav_menu
-- ----------------------------
INSERT INTO `aws_nav_menu` VALUES ('1', '默认分类', '默认分类描述', 'category', '1', null, null, '0');

-- ----------------------------
-- Table structure for `aws_notification`
-- ----------------------------
DROP TABLE IF EXISTS `aws_notification`;
CREATE TABLE `aws_notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `sender_uid` int(11) DEFAULT NULL COMMENT '发送者ID',
  `recipient_uid` int(11) DEFAULT '0' COMMENT '接收者ID',
  `action_type` int(4) DEFAULT NULL COMMENT '操作类型',
  `model_type` smallint(11) NOT NULL DEFAULT '0',
  `source_id` varchar(16) NOT NULL DEFAULT '0' COMMENT '关联 ID',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  `read_flag` tinyint(1) DEFAULT '0' COMMENT '阅读状态',
  PRIMARY KEY (`notification_id`),
  KEY `recipient_read_flag` (`recipient_uid`,`read_flag`),
  KEY `sender_uid` (`sender_uid`),
  KEY `model_type` (`model_type`),
  KEY `source_id` (`source_id`),
  KEY `action_type` (`action_type`),
  KEY `add_time` (`add_time`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='系统通知';

-- ----------------------------
-- Records of aws_notification
-- ----------------------------
INSERT INTO `aws_notification` VALUES ('1', '2', '4', '102', '1', '1', '1477788492', '1');
INSERT INTO `aws_notification` VALUES ('2', '4', '2', '105', '1', '1', '1478168701', '1');

-- ----------------------------
-- Table structure for `aws_notification_data`
-- ----------------------------
DROP TABLE IF EXISTS `aws_notification_data`;
CREATE TABLE `aws_notification_data` (
  `notification_id` int(11) unsigned NOT NULL,
  `data` text,
  PRIMARY KEY (`notification_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='系统通知数据表';

-- ----------------------------
-- Records of aws_notification_data
-- ----------------------------
INSERT INTO `aws_notification_data` VALUES ('1', 'a:4:{s:11:\"question_id\";i:1;s:8:\"from_uid\";i:2;s:7:\"item_id\";s:1:\"1\";s:9:\"anonymous\";i:0;}');
INSERT INTO `aws_notification_data` VALUES ('2', 'a:4:{s:8:\"from_uid\";i:4;s:11:\"question_id\";i:1;s:7:\"item_id\";i:1;s:10:\"comment_id\";s:1:\"1\";}');

-- ----------------------------
-- Table structure for `aws_pages`
-- ----------------------------
DROP TABLE IF EXISTS `aws_pages`;
CREATE TABLE `aws_pages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `url_token` varchar(32) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `contents` text,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `url_token` (`url_token`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_pages
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_posts_index`
-- ----------------------------
DROP TABLE IF EXISTS `aws_posts_index`;
CREATE TABLE `aws_posts_index` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `post_id` int(10) NOT NULL,
  `post_type` varchar(16) NOT NULL DEFAULT '',
  `add_time` int(10) NOT NULL,
  `update_time` int(10) DEFAULT '0',
  `category_id` int(10) DEFAULT '0',
  `is_recommend` tinyint(1) DEFAULT '0',
  `view_count` int(10) DEFAULT '0',
  `anonymous` tinyint(1) DEFAULT '0',
  `popular_value` int(10) DEFAULT '0',
  `uid` int(10) NOT NULL,
  `lock` tinyint(1) DEFAULT '0',
  `agree_count` int(10) DEFAULT '0',
  `answer_count` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `post_type` (`post_type`),
  KEY `add_time` (`add_time`),
  KEY `update_time` (`update_time`),
  KEY `category_id` (`category_id`),
  KEY `is_recommend` (`is_recommend`),
  KEY `anonymous` (`anonymous`),
  KEY `popular_value` (`popular_value`),
  KEY `uid` (`uid`),
  KEY `lock` (`lock`),
  KEY `agree_count` (`agree_count`),
  KEY `answer_count` (`answer_count`),
  KEY `view_count` (`view_count`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_posts_index
-- ----------------------------
INSERT INTO `aws_posts_index` VALUES ('1', '1', 'article', '1477553402', '1477553402', '2', '0', '0', '0', '0', '1', '0', '0', '0');
INSERT INTO `aws_posts_index` VALUES ('2', '2', 'article', '1477575772', '1477575772', '6', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `aws_posts_index` VALUES ('3', '3', 'article', '1477618858', '1477618858', '6', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `aws_posts_index` VALUES ('4', '4', 'article', '1477635297', '1477635297', '6', '0', '0', '0', '0', '2', '0', '0', '0');
INSERT INTO `aws_posts_index` VALUES ('5', '1', 'question', '1477724625', '1477788491', '2', '0', '4', '0', '1', '4', '0', '0', '1');
INSERT INTO `aws_posts_index` VALUES ('6', '5', 'article', '1477995072', '1477995072', '6', '0', '2', '0', '0', '2', '0', '0', '0');

-- ----------------------------
-- Table structure for `aws_question`
-- ----------------------------
DROP TABLE IF EXISTS `aws_question`;
CREATE TABLE `aws_question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_content` varchar(255) NOT NULL DEFAULT '' COMMENT '问题内容',
  `question_detail` text COMMENT '问题说明',
  `add_time` int(11) NOT NULL COMMENT '添加时间',
  `update_time` int(11) DEFAULT NULL,
  `published_uid` int(11) DEFAULT NULL COMMENT '发布用户UID',
  `answer_count` int(11) NOT NULL DEFAULT '0' COMMENT '回答计数',
  `answer_users` int(11) NOT NULL DEFAULT '0' COMMENT '回答人数',
  `view_count` int(11) NOT NULL DEFAULT '0' COMMENT '浏览次数',
  `focus_count` int(11) NOT NULL DEFAULT '0' COMMENT '关注数',
  `comment_count` int(11) NOT NULL DEFAULT '0' COMMENT '评论数',
  `action_history_id` int(11) NOT NULL DEFAULT '0' COMMENT '动作的记录表的关连id',
  `category_id` int(11) NOT NULL DEFAULT '0' COMMENT '分类 ID',
  `agree_count` int(11) NOT NULL DEFAULT '0' COMMENT '回复赞同数总和',
  `against_count` int(11) NOT NULL DEFAULT '0' COMMENT '回复反对数总和',
  `best_answer` int(11) NOT NULL DEFAULT '0' COMMENT '最佳回复 ID',
  `has_attach` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否存在附件',
  `unverified_modify` text,
  `unverified_modify_count` int(10) NOT NULL DEFAULT '0',
  `ip` bigint(11) DEFAULT NULL,
  `last_answer` int(11) NOT NULL DEFAULT '0' COMMENT '最后回答 ID',
  `popular_value` double NOT NULL DEFAULT '0',
  `popular_value_update` int(10) NOT NULL DEFAULT '0',
  `lock` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否锁定',
  `anonymous` tinyint(1) NOT NULL DEFAULT '0',
  `thanks_count` int(10) NOT NULL DEFAULT '0',
  `question_content_fulltext` text,
  `is_recommend` tinyint(1) NOT NULL DEFAULT '0',
  `weibo_msg_id` bigint(20) DEFAULT NULL,
  `received_email_id` int(10) DEFAULT NULL,
  `chapter_id` int(10) unsigned DEFAULT NULL,
  `sort` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`question_id`),
  KEY `category_id` (`category_id`),
  KEY `update_time` (`update_time`),
  KEY `add_time` (`add_time`),
  KEY `published_uid` (`published_uid`),
  KEY `answer_count` (`answer_count`),
  KEY `agree_count` (`agree_count`),
  KEY `question_content` (`question_content`),
  KEY `lock` (`lock`),
  KEY `thanks_count` (`thanks_count`),
  KEY `anonymous` (`anonymous`),
  KEY `popular_value` (`popular_value`),
  KEY `best_answer` (`best_answer`),
  KEY `popular_value_update` (`popular_value_update`),
  KEY `against_count` (`against_count`),
  KEY `is_recommend` (`is_recommend`),
  KEY `weibo_msg_id` (`weibo_msg_id`),
  KEY `received_email_id` (`received_email_id`),
  KEY `unverified_modify_count` (`unverified_modify_count`),
  KEY `chapter_id` (`chapter_id`),
  KEY `sort` (`sort`),
  FULLTEXT KEY `question_content_fulltext` (`question_content_fulltext`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='问题列表';

-- ----------------------------
-- Records of aws_question
-- ----------------------------
INSERT INTO `aws_question` VALUES ('1', '嗨。我来逛逛、', '邮箱验证有问题。额，不支持图片插入。', '1477724625', '1477788491', '4', '1', '1', '11', '2', '0', '0', '2', '0', '0', '0', '0', null, '0', '717610247', '1', '3', '1478231119', '0', '0', '0', '2510526469 3689136891', '0', null, null, null, '0');

-- ----------------------------
-- Table structure for `aws_question_comments`
-- ----------------------------
DROP TABLE IF EXISTS `aws_question_comments`;
CREATE TABLE `aws_question_comments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT '0',
  `uid` int(11) DEFAULT '0',
  `message` text,
  `time` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_question_comments
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_question_focus`
-- ----------------------------
DROP TABLE IF EXISTS `aws_question_focus`;
CREATE TABLE `aws_question_focus` (
  `focus_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `question_id` int(11) DEFAULT NULL COMMENT '话题ID',
  `uid` int(11) DEFAULT NULL COMMENT '用户UID',
  `add_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`focus_id`),
  KEY `question_id` (`question_id`),
  KEY `question_uid` (`question_id`,`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='问题关注表';

-- ----------------------------
-- Records of aws_question_focus
-- ----------------------------
INSERT INTO `aws_question_focus` VALUES ('1', '1', '4', '1477724625');
INSERT INTO `aws_question_focus` VALUES ('2', '1', '2', '1477788491');

-- ----------------------------
-- Table structure for `aws_question_invite`
-- ----------------------------
DROP TABLE IF EXISTS `aws_question_invite`;
CREATE TABLE `aws_question_invite` (
  `question_invite_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `question_id` int(11) NOT NULL COMMENT '问题ID',
  `sender_uid` int(11) NOT NULL,
  `recipients_uid` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL COMMENT '受邀Email',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  `available_time` int(10) DEFAULT '0' COMMENT '生效时间',
  PRIMARY KEY (`question_invite_id`),
  KEY `question_id` (`question_id`),
  KEY `sender_uid` (`sender_uid`),
  KEY `recipients_uid` (`recipients_uid`),
  KEY `add_time` (`add_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='邀请问答';

-- ----------------------------
-- Records of aws_question_invite
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_question_thanks`
-- ----------------------------
DROP TABLE IF EXISTS `aws_question_thanks`;
CREATE TABLE `aws_question_thanks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `question_id` int(11) DEFAULT '0',
  `user_name` varchar(255) DEFAULT NULL,
  `time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `question_id` (`question_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_question_thanks
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_question_uninterested`
-- ----------------------------
DROP TABLE IF EXISTS `aws_question_uninterested`;
CREATE TABLE `aws_question_uninterested` (
  `interested_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `question_id` int(11) DEFAULT NULL COMMENT '话题ID',
  `uid` int(11) DEFAULT NULL COMMENT '用户UID',
  `add_time` int(10) DEFAULT NULL,
  PRIMARY KEY (`interested_id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='问题不感兴趣表';

-- ----------------------------
-- Records of aws_question_uninterested
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_received_email`
-- ----------------------------
DROP TABLE IF EXISTS `aws_received_email`;
CREATE TABLE `aws_received_email` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `config_id` int(10) NOT NULL,
  `message_id` varchar(255) NOT NULL,
  `date` int(10) NOT NULL,
  `from` varchar(255) NOT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `content` text,
  `question_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `config_id` (`config_id`),
  KEY `message_id` (`message_id`),
  KEY `date` (`date`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='已导入邮件列表';

-- ----------------------------
-- Records of aws_received_email
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_receiving_email_config`
-- ----------------------------
DROP TABLE IF EXISTS `aws_receiving_email_config`;
CREATE TABLE `aws_receiving_email_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `protocol` varchar(10) NOT NULL,
  `server` varchar(255) NOT NULL,
  `ssl` tinyint(1) NOT NULL DEFAULT '0',
  `port` smallint(5) DEFAULT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `uid` int(10) NOT NULL,
  `access_key` varchar(32) NOT NULL,
  `has_attach` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `server` (`server`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='邮件账号列表';

-- ----------------------------
-- Records of aws_receiving_email_config
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_redirect`
-- ----------------------------
DROP TABLE IF EXISTS `aws_redirect`;
CREATE TABLE `aws_redirect` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT '0',
  `target_id` int(11) DEFAULT '0',
  `time` int(10) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `item_id` (`item_id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_redirect
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_related_links`
-- ----------------------------
DROP TABLE IF EXISTS `aws_related_links`;
CREATE TABLE `aws_related_links` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `item_type` varchar(32) NOT NULL,
  `item_id` int(10) NOT NULL,
  `link` varchar(255) NOT NULL,
  `add_time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `item_type` (`item_type`),
  KEY `item_id` (`item_id`),
  KEY `add_time` (`add_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_related_links
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_related_topic`
-- ----------------------------
DROP TABLE IF EXISTS `aws_related_topic`;
CREATE TABLE `aws_related_topic` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `topic_id` int(11) DEFAULT '0' COMMENT '话题 ID',
  `related_id` int(11) DEFAULT '0' COMMENT '相关话题 ID',
  PRIMARY KEY (`id`),
  KEY `topic_id` (`topic_id`),
  KEY `related_id` (`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_related_topic
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_report`
-- ----------------------------
DROP TABLE IF EXISTS `aws_report`;
CREATE TABLE `aws_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0' COMMENT '举报用户id',
  `type` varchar(50) DEFAULT NULL COMMENT '类别',
  `target_id` int(11) DEFAULT '0' COMMENT 'ID',
  `reason` varchar(255) DEFAULT NULL COMMENT '举报理由',
  `url` varchar(255) DEFAULT NULL,
  `add_time` int(11) DEFAULT '0' COMMENT '举报时间',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否处理',
  PRIMARY KEY (`id`),
  KEY `add_time` (`add_time`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_report
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_reputation_category`
-- ----------------------------
DROP TABLE IF EXISTS `aws_reputation_category`;
CREATE TABLE `aws_reputation_category` (
  `auto_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) DEFAULT '0',
  `category_id` smallint(4) DEFAULT '0',
  `update_time` int(10) DEFAULT '0',
  `reputation` int(10) DEFAULT '0',
  `thanks_count` int(10) DEFAULT '0',
  `agree_count` int(10) DEFAULT '0',
  `question_count` int(10) DEFAULT '0',
  PRIMARY KEY (`auto_id`),
  UNIQUE KEY `uid_category_id` (`uid`,`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_reputation_category
-- ----------------------------
INSERT INTO `aws_reputation_category` VALUES ('1', '2', '2', '1478351528', '0', '0', '0', '1');

-- ----------------------------
-- Table structure for `aws_reputation_topic`
-- ----------------------------
DROP TABLE IF EXISTS `aws_reputation_topic`;
CREATE TABLE `aws_reputation_topic` (
  `auto_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `topic_id` int(11) DEFAULT '0' COMMENT '话题ID',
  `topic_count` int(10) DEFAULT '0' COMMENT '威望问题话题计数',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间',
  `agree_count` int(10) DEFAULT '0' COMMENT '赞成',
  `thanks_count` int(10) DEFAULT '0' COMMENT '感谢',
  `reputation` int(10) DEFAULT '0',
  PRIMARY KEY (`auto_id`),
  KEY `topic_count` (`topic_count`),
  KEY `uid` (`uid`),
  KEY `topic_id` (`topic_id`),
  KEY `reputation` (`reputation`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_reputation_topic
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_school`
-- ----------------------------
DROP TABLE IF EXISTS `aws_school`;
CREATE TABLE `aws_school` (
  `school_id` int(11) NOT NULL COMMENT '自增ID',
  `school_type` tinyint(4) DEFAULT NULL COMMENT '学校类型ID',
  `school_code` int(11) DEFAULT NULL COMMENT '学校编码',
  `school_name` varchar(64) DEFAULT NULL COMMENT '学校名称',
  `area_code` int(11) DEFAULT NULL COMMENT '地区代码',
  PRIMARY KEY (`school_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='学校';

-- ----------------------------
-- Records of aws_school
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_search_cache`
-- ----------------------------
DROP TABLE IF EXISTS `aws_search_cache`;
CREATE TABLE `aws_search_cache` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `hash` varchar(32) NOT NULL,
  `data` mediumtext NOT NULL,
  `time` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_search_cache
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `aws_sessions`;
CREATE TABLE `aws_sessions` (
  `id` varchar(32) NOT NULL,
  `modified` int(10) NOT NULL,
  `data` text NOT NULL,
  `lifetime` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `modified` (`modified`),
  KEY `lifetime` (`lifetime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_sessions
-- ----------------------------
INSERT INTO `aws_sessions` VALUES ('5g4g4e6hr06q9msp5v57m9vc87', '1478324220', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('je7esqs14a429l97ob3hmhlf81', '1478324237', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('j862avtgn94f0clb494lmua734', '1478324242', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('8lk0912799jnm217fof1u51nj2', '1478324244', 'lgp__Anwsion|a:2:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('qp9sgh52kfj7255ab14htl4p87', '1478324245', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('5o5t64ogh4uop4h6chertq03r5', '1478324281', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('lvrevpupst5ehhmcu4couh42u3', '1478324282', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('v9s4c960qnc95l7n8smc4rkga3', '1478324292', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('tb541p43bfv43dgojtnib14ts1', '1478316800', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('m6gi69ke1odnkht5k30jvrpvi1', '1478309347', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('3v6uo48sstfjngmg26ks480ra3', '1478324175', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('h6g0cn3skgiofm6cpo0r9p7as0', '1478324186', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('s0kp2epvkhut17r4o03a2s6j24', '1478324188', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('nt9i8g6qae214djforj976e6u7', '1478324190', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('ebu29mn8jli09i2kn006fsc3f1', '1478324192', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('0ln6sipeun1u0gvjoogkccb3p0', '1478324194', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('e4gv52pcf6b19m6u6e1b2bd0f2', '1478324195', 'lgp__Anwsion|a:2:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('ki7q3egjl4id655jc37an89b54', '1478324196', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('boisgtefhfrutg1pp0v4mn4c82', '1478324197', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('mnut54oqn3ctpm24q0s2hhlhr6', '1478324197', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('ibtmcm704lbf17eapfjgijjcv1', '1478324199', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('8fejfd1jgtfcfe7ghjgq8prkg3', '1478324201', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('9ohu8m7aabnqce802ilhsnluk6', '1478324203', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('efot3qs4q8ol8i99uk2932kim0', '1478324205', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('e8gtq1e5hfjqvpqsn9gdfk1285', '1478324207', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('18kpqj83l51a164b52n6k5ki94', '1478324208', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('53h069hcb36blbhj5tc3lspkt2', '1478324210', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('9fl6kqnd9lct7uoag4i6v8i941', '1478324211', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('mkhhqsjf9tp0ohigr592hi27n3', '1478324212', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('l0nh33a028glt577a2immcv3o6', '1478324218', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('s94oabd1b3136m7okkjb5bjs07', '1478324217', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('sgrel4mo0ssunipbs9kp9llkb5', '1478324215', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('g594taf8hu4j3mqkbduok5udo1', '1478324214', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('tii74lrjs7mjbvrn9nagu4sta6', '1478324294', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('j591foavesv6topoa9a7h9ura0', '1478324298', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('oke6fmnchpmcriqtor0teqq4d7', '1478324300', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('b39hur4cfga54tu1spgbb1aq35', '1478324300', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('gdhkqh36voqlmnd8inj6m2c1c2', '1478324306', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('bt85rciafasu5smt8p7bct32n1', '1478324327', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('2kcb74hjgll27vt09tns1u1u85', '1478324348', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('rndgvvg4u9poo4uqlnegjn8522', '1478324374', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('i5cqa927gr0f7ji21pof0du5q4', '1478324375', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('5rmu2npbo96hjrn9re7a9q4qh0', '1478324383', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('re7h2cg8vdvprbmc970m9tsej7', '1478324386', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('lnk2vjqdo4j5fomeds7n133s43', '1478324401', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('a0ug378fsi17c3e61lqr4kkl12', '1478324464', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('eissd0bmhdckkted58eusdrhe6', '1478324496', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('7mfoijg7voq342gg1rj4b0gev7', '1478324543', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:7:\"qq_user\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('pi6fvmrh8q9mmhgq8m3m3gmpe5', '1478324547', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('6d76brci36jkdngjv5ceru7dp0', '1478324578', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('cr2lsngoou8upv18p9n73f3df1', '1478352313', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:15:{s:16:\"is_administortar\";s:1:\"1\";s:12:\"is_moderator\";s:1:\"1\";s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:13:\"edit_question\";s:1:\"1\";s:10:\"edit_topic\";s:1:\"1\";s:12:\"manage_topic\";s:1:\"1\";s:12:\"create_topic\";s:1:\"1\";s:17:\"redirect_question\";s:1:\"1\";s:13:\"upload_attach\";s:1:\"1\";s:11:\"publish_url\";s:1:\"1\";s:15:\"publish_article\";s:1:\"1\";s:12:\"edit_article\";s:1:\"1\";s:19:\"edit_question_topic\";s:1:\"1\";s:15:\"publish_comment\";s:1:\"1\";}s:11:\"client_info\";a:3:{s:12:\"__CLIENT_UID\";i:2;s:18:\"__CLIENT_USER_NAME\";s:5:\"Jerry\";s:17:\"__CLIENT_PASSWORD\";s:32:\"d6cdeba41fb23f69748138b43c33849b\";}s:11:\"admin_login\";s:265:\"cast-256|1518EE404AD93C655D4C33B4C6055B86D32E2B65D7FF16B0C2C5E9E18DB92F9D38ED30B311458EF8927BBA6FE22B04FD3734537AA7F0497280E835BE921B1559A05360F14E76D73C186A42EA7D67C560ECF8C1E8EBCC5EE12AC841C5D77CE768018CB551C8235FE15B77ED46102604CEA834BCFB7183AF2DE677A55277817F3D\";}lgp__Captcha|a:1:{s:4:\"word\";s:4:\"s776\";}', '1440');
INSERT INTO `aws_sessions` VALUES ('0peao5cs9rnt6rfpi65t60nrd6', '1478330457', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('52hq2etkhja91j339oaaidnd40', '1478330451', 'lgp__Anwsion|a:2:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('tcnstut2ge8bhh7cjjub0proa2', '1478330457', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('b551b3o0c4r9euf4gj6q4vlft4', '1478330468', 'lgp__Anwsion|a:2:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('h3l18fjgiv9s6n9iu3d8i41iu6', '1478330476', 'lgp__Anwsion|a:2:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('61u3nni4bac439di97e8cku966', '1478333726', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('qje5t0ftndim1vcos91toqel32', '1478339336', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('h7rbfhgiaha4a4v12vcrniasl6', '1478349496', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('0uhk4gk5nb0tfo5u9u2l5m1pf0', '1478356422', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('rugm6ol30fd7iqsao180i2kpt2', '1478357092', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('89lgg4520qcoat92knorbqlpp4', '1478359102', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('qd12qli2lnju65v46cpp54glh4', '1478360105', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('moadac1m5g3ko8mmo27qeppdn4', '1478360205', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('3ssof7fcfsbuh61kvhkrrimd97', '1478360925', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('tdhl6qcsjc86o3rfgfu76r2u57', '1478363695', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('6o2eu8i9a3cvsqaaqob5ueg6r3', '1478363855', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('f3d5sq7h7lqbjogl7hhgso1ve0', '1478367853', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('j9lj19gqovepaua4gjuap2b9p7', '1478373612', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('vvdvlon5aedd2ksjfipik3e223', '1478378868', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('2b20o4jaj3va57h4grn937aiu7', '1478381788', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('43b2hve65onsjlusm3epmt9ii4', '1478381992', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('nddnjkp8031la32plhtbr3q620', '1478382150', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('8ka3jqe5crc63tg6qrfvr8rdp2', '1478382901', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('a9mc74sdooqe24vt2umf4pj611', '1478403001', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');
INSERT INTO `aws_sessions` VALUES ('23t4udpdnlr7alsip9288ajte4', '1478409804', 'lgp__Anwsion|a:3:{s:10:\"permission\";a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}s:11:\"client_info\";N;s:11:\"human_valid\";N;}', '1440');

-- ----------------------------
-- Table structure for `aws_system_setting`
-- ----------------------------
DROP TABLE IF EXISTS `aws_system_setting`;
CREATE TABLE `aws_system_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `varname` varchar(255) NOT NULL COMMENT '字段名',
  `value` text COMMENT '变量值',
  PRIMARY KEY (`id`),
  UNIQUE KEY `varname` (`varname`)
) ENGINE=MyISAM AUTO_INCREMENT=130 DEFAULT CHARSET=utf8 COMMENT='系统设置';

-- ----------------------------
-- Records of aws_system_setting
-- ----------------------------
INSERT INTO `aws_system_setting` VALUES ('1', 'db_engine', 's:6:\"MyISAM\";');
INSERT INTO `aws_system_setting` VALUES ('2', 'site_name', 's:36:\"5ihelp专注于PHPweb系统的开发\";');
INSERT INTO `aws_system_setting` VALUES ('3', 'description', 's:84:\"5ihelp专注于PHPweb系统的开发，致力于让网站开发更简单，快捷。\";');
INSERT INTO `aws_system_setting` VALUES ('4', 'keywords', 's:73:\"5ihelp 5ihelp开源社区 5ihelp天源系统 5ihelpCMS 5ihelp问答社区\";');
INSERT INTO `aws_system_setting` VALUES ('5', 'sensitive_words', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('6', 'def_focus_uids', 's:1:\"1\";');
INSERT INTO `aws_system_setting` VALUES ('7', 'answer_edit_time', 's:2:\"30\";');
INSERT INTO `aws_system_setting` VALUES ('8', 'cache_level_high', 's:2:\"60\";');
INSERT INTO `aws_system_setting` VALUES ('9', 'cache_level_normal', 's:3:\"600\";');
INSERT INTO `aws_system_setting` VALUES ('10', 'cache_level_low', 's:4:\"1800\";');
INSERT INTO `aws_system_setting` VALUES ('11', 'unread_flush_interval', 's:3:\"100\";');
INSERT INTO `aws_system_setting` VALUES ('12', 'newer_invitation_num', 's:1:\"5\";');
INSERT INTO `aws_system_setting` VALUES ('13', 'index_per_page', 's:2:\"20\";');
INSERT INTO `aws_system_setting` VALUES ('14', 'from_email', 's:16:\"502443279@qq.com\";');
INSERT INTO `aws_system_setting` VALUES ('15', 'img_url', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('16', 'upload_url', 's:34:\"http://cooldreamer.com/ask/uploads\";');
INSERT INTO `aws_system_setting` VALUES ('17', 'upload_dir', 's:39:\"/www/web/5ihelp/public_html/ask/uploads\";');
INSERT INTO `aws_system_setting` VALUES ('18', 'ui_style', 's:7:\"default\";');
INSERT INTO `aws_system_setting` VALUES ('19', 'uninterested_fold', 's:1:\"5\";');
INSERT INTO `aws_system_setting` VALUES ('20', 'sina_akey', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('21', 'sina_skey', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('22', 'sina_weibo_enabled', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('23', 'answer_unique', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('24', 'notifications_per_page', 's:2:\"10\";');
INSERT INTO `aws_system_setting` VALUES ('25', 'contents_per_page', 's:2:\"10\";');
INSERT INTO `aws_system_setting` VALUES ('26', 'hot_question_period', 's:1:\"7\";');
INSERT INTO `aws_system_setting` VALUES ('27', 'category_display_mode', 's:4:\"list\";');
INSERT INTO `aws_system_setting` VALUES ('28', 'recommend_users_number', 's:1:\"6\";');
INSERT INTO `aws_system_setting` VALUES ('29', 'ucenter_enabled', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('30', 'register_valid_type', 's:5:\"email\";');
INSERT INTO `aws_system_setting` VALUES ('31', 'best_answer_day', 's:2:\"30\";');
INSERT INTO `aws_system_setting` VALUES ('32', 'answer_self_question', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('33', 'censoruser', 's:43:\"admin,jerry,管理员,5ihelp,关键字,php,\";');
INSERT INTO `aws_system_setting` VALUES ('34', 'best_answer_min_count', 's:1:\"3\";');
INSERT INTO `aws_system_setting` VALUES ('35', 'reputation_function', 's:78:\"[最佳答案]*3+[赞同]*1-[反对]*1+[发起者赞同]*2-[发起者反对]*1\";');
INSERT INTO `aws_system_setting` VALUES ('36', 'db_version', 's:8:\"20160523\";');
INSERT INTO `aws_system_setting` VALUES ('37', 'statistic_code', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('38', 'upload_enable', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('39', 'answer_length_lower', 's:1:\"2\";');
INSERT INTO `aws_system_setting` VALUES ('40', 'quick_publish', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('41', 'register_type', 's:4:\"open\";');
INSERT INTO `aws_system_setting` VALUES ('42', 'question_title_limit', 's:3:\"100\";');
INSERT INTO `aws_system_setting` VALUES ('43', 'register_seccode', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('44', 'admin_login_seccode', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('45', 'comment_limit', 's:1:\"0\";');
INSERT INTO `aws_system_setting` VALUES ('46', 'backup_dir', '');
INSERT INTO `aws_system_setting` VALUES ('47', 'best_answer_reput', 's:2:\"20\";');
INSERT INTO `aws_system_setting` VALUES ('48', 'publisher_reputation_factor', 's:2:\"10\";');
INSERT INTO `aws_system_setting` VALUES ('49', 'request_route_custom', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('50', 'upload_size_limit', 's:3:\"512\";');
INSERT INTO `aws_system_setting` VALUES ('51', 'upload_avatar_size_limit', 's:3:\"512\";');
INSERT INTO `aws_system_setting` VALUES ('52', 'topic_title_limit', 's:2:\"12\";');
INSERT INTO `aws_system_setting` VALUES ('53', 'url_rewrite_enable', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('54', 'best_agree_min_count', 's:1:\"3\";');
INSERT INTO `aws_system_setting` VALUES ('55', 'site_close', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('56', 'close_notice', 's:39:\"站点已关闭，管理员请登录。\";');
INSERT INTO `aws_system_setting` VALUES ('57', 'qq_login_enabled', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('58', 'qq_login_app_id', 's:9:\"101142483\";');
INSERT INTO `aws_system_setting` VALUES ('59', 'qq_login_app_key', 's:32:\"17a5a055e69bb12e3c880ca79049e901\";');
INSERT INTO `aws_system_setting` VALUES ('60', 'integral_system_enabled', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('61', 'integral_system_config_register', 's:4:\"2000\";');
INSERT INTO `aws_system_setting` VALUES ('62', 'integral_system_config_profile', 's:3:\"100\";');
INSERT INTO `aws_system_setting` VALUES ('63', 'integral_system_config_invite', 's:3:\"200\";');
INSERT INTO `aws_system_setting` VALUES ('64', 'integral_system_config_best_answer', 's:3:\"200\";');
INSERT INTO `aws_system_setting` VALUES ('65', 'integral_system_config_answer_fold', 's:3:\"-50\";');
INSERT INTO `aws_system_setting` VALUES ('66', 'integral_system_config_new_question', 's:3:\"-20\";');
INSERT INTO `aws_system_setting` VALUES ('67', 'integral_system_config_new_answer', 's:2:\"-5\";');
INSERT INTO `aws_system_setting` VALUES ('68', 'integral_system_config_thanks', 's:3:\"-10\";');
INSERT INTO `aws_system_setting` VALUES ('69', 'integral_system_config_invite_answer', 's:3:\"-10\";');
INSERT INTO `aws_system_setting` VALUES ('70', 'username_rule', 's:1:\"1\";');
INSERT INTO `aws_system_setting` VALUES ('71', 'username_length_min', 's:1:\"2\";');
INSERT INTO `aws_system_setting` VALUES ('72', 'username_length_max', 's:2:\"14\";');
INSERT INTO `aws_system_setting` VALUES ('73', 'category_enable', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('74', 'integral_unit', 's:6:\"金币\";');
INSERT INTO `aws_system_setting` VALUES ('75', 'nav_menu_show_child', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('76', 'anonymous_enable', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('77', 'report_reason', 's:50:\"广告/SPAM\n违规内容\n文不对题\n重复发问\";');
INSERT INTO `aws_system_setting` VALUES ('78', 'allowed_upload_types', 's:41:\"jpg,jpeg,png,gif,zip,doc,docx,rar,pdf,psd\";');
INSERT INTO `aws_system_setting` VALUES ('79', 'site_announce', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('80', 'icp_beian', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('81', 'report_message_uid', 's:1:\"1\";');
INSERT INTO `aws_system_setting` VALUES ('82', 'today_topics', 's:20:\"wecenter二次开发\";');
INSERT INTO `aws_system_setting` VALUES ('83', 'welcome_recommend_users', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('84', 'welcome_message_pm', 's:180:\"尊敬的{username}，您已经注册成为{sitename}的会员，请您在发表言论时，遵守当地法律法规。\n如果您有什么疑问可以联系管理员。\n\n{sitename}\";');
INSERT INTO `aws_system_setting` VALUES ('85', 'time_style', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('86', 'reputation_log_factor', 's:1:\"3\";');
INSERT INTO `aws_system_setting` VALUES ('87', 'advanced_editor_enable', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('88', 'auto_question_lock_day', 's:1:\"0\";');
INSERT INTO `aws_system_setting` VALUES ('89', 'default_timezone', 's:9:\"Etc/GMT-8\";');
INSERT INTO `aws_system_setting` VALUES ('90', 'reader_questions_last_days', 's:2:\"30\";');
INSERT INTO `aws_system_setting` VALUES ('91', 'reader_questions_agree_count', 's:2:\"10\"');
INSERT INTO `aws_system_setting` VALUES ('92', 'weixin_mp_token', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('93', 'new_user_email_setting', 'a:2:{s:9:\"FOLLOW_ME\";s:1:\"N\";s:10:\"NEW_ANSWER\";s:1:\"N\";}');
INSERT INTO `aws_system_setting` VALUES ('94', 'new_user_notification_setting', 'a:0:{}');
INSERT INTO `aws_system_setting` VALUES ('95', 'user_action_history_fresh_upgrade', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('96', 'cache_dir', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('97', 'ucenter_charset', 's:5:\"UTF-8\";');
INSERT INTO `aws_system_setting` VALUES ('98', 'question_topics_limit', 's:2:\"10\";');
INSERT INTO `aws_system_setting` VALUES ('99', 'mail_config', 'a:7:{s:9:\"transport\";s:8:\"sendmail\";s:7:\"charset\";s:5:\"UTF-8\";s:6:\"server\";s:0:\"\";s:3:\"ssl\";s:1:\"0\";s:4:\"port\";s:0:\"\";s:8:\"username\";s:0:\"\";s:8:\"password\";s:0:\"\";}');
INSERT INTO `aws_system_setting` VALUES ('100', 'auto_create_social_topics', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('101', 'weixin_subscribe_message_key', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('102', 'weixin_no_result_message_key', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('103', 'weixin_mp_menu', 'a:0:{}');
INSERT INTO `aws_system_setting` VALUES ('104', 'new_question_force_add_topic', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('105', 'unfold_question_comments', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('106', 'report_diagnostics', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('107', 'weixin_app_id', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('108', 'weixin_app_secret', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('109', 'weixin_account_role', 's:4:\"base\";');
INSERT INTO `aws_system_setting` VALUES ('110', 'weibo_msg_enabled', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('111', 'weibo_msg_published_user', 'a:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('112', 'admin_notifications', 'a:11:{s:15:\"answer_approval\";i:0;s:17:\"question_approval\";i:0;s:16:\"article_approval\";i:0;s:24:\"article_comment_approval\";i:0;s:23:\"unverified_modify_count\";i:0;s:11:\"user_report\";i:0;s:17:\"register_approval\";i:0;s:15:\"verify_approval\";i:0;s:12:\"last_version\";a:2:{s:7:\"version\";s:5:\"3.1.9\";s:9:\"build_day\";s:8:\"20160523\";}s:10:\"sina_users\";N;s:19:\"receive_email_error\";N;}');
INSERT INTO `aws_system_setting` VALUES ('113', 'slave_mail_config', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('114', 'receiving_email_global_config', 'a:2:{s:7:\"enabled\";s:1:\"N\";s:12:\"publish_user\";N;}');
INSERT INTO `aws_system_setting` VALUES ('115', 'last_sent_valid_email_id', 'i:0;');
INSERT INTO `aws_system_setting` VALUES ('116', 'google_login_enabled', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('117', 'google_client_id', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('118', 'google_client_secret', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('119', 'facebook_login_enabled', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('120', 'facebook_app_id', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('121', 'facebook_app_secret', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('122', 'twitter_login_enabled', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('123', 'twitter_consumer_key', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('124', 'twitter_consumer_secret', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('125', 'weixin_encoding_aes_key', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('126', 'integral_system_config_answer_change_source', 's:1:\"Y\";');
INSERT INTO `aws_system_setting` VALUES ('127', 'enable_help_center', 's:1:\"N\";');
INSERT INTO `aws_system_setting` VALUES ('128', 'ucenter_path', 's:0:\"\";');
INSERT INTO `aws_system_setting` VALUES ('129', 'register_agreement', 's:1608:\"当您申请用户时，表示您已经同意遵守本规章。\n欢迎您加入本站点参与交流和讨论，本站点为社区，为维护网上公共秩序和社会稳定，请您自觉遵守以下条款：\n\n一、不得利用本站危害国家安全、泄露国家秘密，不得侵犯国家社会集体的和公民的合法权益，不得利用本站制作、复制和传播下列信息：\n　（一）煽动抗拒、破坏宪法和法律、行政法规实施的；\n　（二）煽动颠覆国家政权，推翻社会主义制度的；\n　（三）煽动分裂国家、破坏国家统一的；\n　（四）煽动民族仇恨、民族歧视，破坏民族团结的；\n　（五）捏造或者歪曲事实，散布谣言，扰乱社会秩序的；\n　（六）宣扬封建迷信、淫秽、色情、赌博、暴力、凶杀、恐怖、教唆犯罪的；\n　（七）公然侮辱他人或者捏造事实诽谤他人的，或者进行其他恶意攻击的；\n　（八）损害国家机关信誉的；\n　（九）其他违反宪法和法律行政法规的；\n　（十）进行商业广告行为的。\n\n二、互相尊重，对自己的言论和行为负责。\n三、禁止在申请用户时使用相关本站的词汇，或是带有侮辱、毁谤、造谣类的或是有其含义的各种语言进行注册用户，否则我们会将其删除。\n四、禁止以任何方式对本站进行各种破坏行为。\n五、如果您有违反国家相关法律法规的行为，本站概不负责，您的登录信息均被记录无疑，必要时，我们会向相关的国家管理部门提供此类信息。\";');

-- ----------------------------
-- Table structure for `aws_topic`
-- ----------------------------
DROP TABLE IF EXISTS `aws_topic`;
CREATE TABLE `aws_topic` (
  `topic_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '话题id',
  `topic_title` varchar(64) DEFAULT NULL COMMENT '话题标题',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  `discuss_count` int(11) DEFAULT '0' COMMENT '讨论计数',
  `topic_description` text COMMENT '话题描述',
  `topic_pic` varchar(255) DEFAULT NULL COMMENT '话题图片',
  `topic_lock` tinyint(2) NOT NULL DEFAULT '0' COMMENT '话题是否锁定 1 锁定 0 未锁定',
  `focus_count` int(11) DEFAULT '0' COMMENT '关注计数',
  `user_related` tinyint(1) DEFAULT '0' COMMENT '是否被用户关联',
  `url_token` varchar(32) DEFAULT NULL,
  `merged_id` int(11) DEFAULT '0',
  `seo_title` varchar(255) DEFAULT NULL,
  `parent_id` int(10) DEFAULT '0',
  `is_parent` tinyint(1) DEFAULT '0',
  `discuss_count_last_week` int(10) DEFAULT '0',
  `discuss_count_last_month` int(10) DEFAULT '0',
  `discuss_count_update` int(10) DEFAULT '0',
  PRIMARY KEY (`topic_id`),
  UNIQUE KEY `topic_title` (`topic_title`),
  KEY `url_token` (`url_token`),
  KEY `merged_id` (`merged_id`),
  KEY `discuss_count` (`discuss_count`),
  KEY `add_time` (`add_time`),
  KEY `user_related` (`user_related`),
  KEY `focus_count` (`focus_count`),
  KEY `topic_lock` (`topic_lock`),
  KEY `parent_id` (`parent_id`),
  KEY `is_parent` (`is_parent`),
  KEY `discuss_count_last_week` (`discuss_count_last_week`),
  KEY `discuss_count_last_month` (`discuss_count_last_month`),
  KEY `discuss_count_update` (`discuss_count_update`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='话题';

-- ----------------------------
-- Records of aws_topic
-- ----------------------------
INSERT INTO `aws_topic` VALUES ('1', '默认话题', null, '0', '默认话题', null, '0', '0', '0', null, '0', null, '0', '0', '0', '0', '0');
INSERT INTO `aws_topic` VALUES ('2', 'PHP', '1477553402', '1', '', null, '0', '1', '0', null, '0', null, '0', '0', '1', '1', '1477553402');
INSERT INTO `aws_topic` VALUES ('3', 'wecenter二次开发', '1477575772', '2', '', null, '0', '1', '0', null, '0', null, '0', '0', '2', '2', '1477618858');

-- ----------------------------
-- Table structure for `aws_topic_focus`
-- ----------------------------
DROP TABLE IF EXISTS `aws_topic_focus`;
CREATE TABLE `aws_topic_focus` (
  `focus_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `topic_id` int(11) DEFAULT NULL COMMENT '话题ID',
  `uid` int(11) DEFAULT NULL COMMENT '用户UID',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`focus_id`),
  KEY `uid` (`uid`),
  KEY `topic_id` (`topic_id`),
  KEY `topic_uid` (`topic_id`,`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='话题关注表';

-- ----------------------------
-- Records of aws_topic_focus
-- ----------------------------
INSERT INTO `aws_topic_focus` VALUES ('1', '2', '1', '1477553402');
INSERT INTO `aws_topic_focus` VALUES ('2', '3', '2', '1477575772');

-- ----------------------------
-- Table structure for `aws_topic_merge`
-- ----------------------------
DROP TABLE IF EXISTS `aws_topic_merge`;
CREATE TABLE `aws_topic_merge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `source_id` int(11) NOT NULL DEFAULT '0',
  `target_id` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) DEFAULT '0',
  `time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `source_id` (`source_id`),
  KEY `target_id` (`target_id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_topic_merge
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_topic_relation`
-- ----------------------------
DROP TABLE IF EXISTS `aws_topic_relation`;
CREATE TABLE `aws_topic_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增 ID',
  `topic_id` int(11) DEFAULT '0' COMMENT '话题id',
  `item_id` int(11) DEFAULT '0',
  `add_time` int(10) DEFAULT '0' COMMENT '添加时间',
  `uid` int(11) DEFAULT '0' COMMENT '用户ID',
  `type` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `topic_id` (`topic_id`),
  KEY `uid` (`uid`),
  KEY `type` (`type`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_topic_relation
-- ----------------------------
INSERT INTO `aws_topic_relation` VALUES ('1', '2', '1', '1477553402', '1', 'article');
INSERT INTO `aws_topic_relation` VALUES ('2', '3', '2', '1477575772', '2', 'article');
INSERT INTO `aws_topic_relation` VALUES ('3', '3', '3', '1477618858', '2', 'article');

-- ----------------------------
-- Table structure for `aws_user_action_history`
-- ----------------------------
DROP TABLE IF EXISTS `aws_user_action_history`;
CREATE TABLE `aws_user_action_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `uid` int(11) NOT NULL COMMENT '用户id',
  `associate_type` tinyint(1) DEFAULT NULL COMMENT '关联类型: 1 问题 2 回答 3 评论 4 话题',
  `associate_action` smallint(3) DEFAULT NULL COMMENT '操作类型',
  `associate_id` int(11) DEFAULT NULL COMMENT '关联ID',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  `associate_attached` int(11) DEFAULT NULL,
  `anonymous` tinyint(1) DEFAULT '0' COMMENT '是否匿名',
  `fold_status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`history_id`),
  KEY `add_time` (`add_time`),
  KEY `uid` (`uid`),
  KEY `associate_id` (`associate_id`),
  KEY `anonymous` (`anonymous`),
  KEY `fold_status` (`fold_status`),
  KEY `associate` (`associate_type`,`associate_action`),
  KEY `associate_attached` (`associate_attached`),
  KEY `associate_with_id` (`associate_id`,`associate_type`,`associate_action`),
  KEY `associate_with_uid` (`uid`,`associate_type`,`associate_action`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='用户操作记录';

-- ----------------------------
-- Records of aws_user_action_history
-- ----------------------------
INSERT INTO `aws_user_action_history` VALUES ('1', '1', '4', '401', '2', '1477553402', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('2', '1', '4', '406', '2', '1477553402', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('3', '1', '1', '501', '1', '1477553403', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('4', '2', '4', '401', '3', '1477575772', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('5', '2', '4', '406', '3', '1477575772', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('6', '2', '1', '501', '2', '1477575772', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('7', '2', '1', '501', '3', '1477618858', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('8', '2', '1', '501', '4', '1477635297', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('9', '4', '1', '101', '1', '1477724625', '-1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('10', '2', '2', '201', '1', '1477788491', '1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('11', '2', '1', '201', '1', '1477788491', '1', '0', '0');
INSERT INTO `aws_user_action_history` VALUES ('12', '2', '1', '501', '5', '1477995072', '-1', '0', '0');

-- ----------------------------
-- Table structure for `aws_user_action_history_data`
-- ----------------------------
DROP TABLE IF EXISTS `aws_user_action_history_data`;
CREATE TABLE `aws_user_action_history_data` (
  `history_id` int(11) unsigned NOT NULL,
  `associate_content` text,
  `associate_attached` text,
  `addon_data` text COMMENT '附加数据',
  PRIMARY KEY (`history_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_user_action_history_data
-- ----------------------------
INSERT INTO `aws_user_action_history_data` VALUES ('1', 'PHP', '', '');
INSERT INTO `aws_user_action_history_data` VALUES ('2', '', '', '');
INSERT INTO `aws_user_action_history_data` VALUES ('3', 'PHP', 'PHP原始为Personal Home Page的缩写，已经正式更名为 &quot;PHP: Hypertext Preprocessor&quot;。注意不是“Hypertext Preprocessor”的缩写，这种将名称放到定义中的写法被称作递归缩写。PHP于1994年由Rasmus Lerdorf创建，刚刚开始是Rasmus Lerdorf为了要维护个人网页而制作的一个简单的用Perl语言编写的程序。这些工具程序用来显示 Rasmus Lerdorf 的个人履历，以及统计网页流量。后来又用C语言重新编写，包括可以访问数据库。他将这些程序和一些表单直译器整合起来，称为 PHP/FI。PHP/FI 可以和数据库连接，产生简单的动态网页程序。\n在1995年以Personal Home Page Tools (PHP Tools) 开始对外发表第一个版本，Lerdorf写了一些介绍此程序的文档。并且发布了PHP1.0！在这的版本中，提供了访客留言本、访客计数器等简单的功能。以后越来越多的网站使用了PHP，并且强烈要求增加一些特性。比如循环语句和数组变量等等；在新的成员加入开发行列之后，Rasmus Lerdorf 在1995年6月8日将 PHP/FI 公开发布，希望可以透过社群来加速程序开发与寻找错误。这个发布的版本命名为 PHP 2，已经有 PHP 的一些雏型，像是类似 Perl的变量命名方式、表单处理功能、以及嵌入到 HTML 中执行的能力。程序语法上也类似 Perl，有较多的限制，不过更简单、更有弹性。PHP/FI加入了对MySQL的支持，从此建立了PHP在动态网页开发上的地位。到了1996年底，有15000个网站使用 PHP/FI。\n[ISAPI筛选器] ISAPI筛选器\n在1997年，任职于 Technion IIT公司的两个以色列程序设计师：Zeev Suraski 和 Andi Gutmans，重写了 PHP 的剖析器，成为 PHP 3 的基础。而 PHP 也在这个时候改称为PHP：Hypertext Preprocessor。经过几个月测试，开发团队在1997年11月发布了 PHP/FI 2。随后就开始 PHP 3 的开放测试，最后在1998年6月正式发布 PHP 3。Zeev Suraski 和 Andi Gutmans 在 PHP 3 发布后开始改写PHP 的核心，这个在1999年发布的剖析器称为 Zend Engine，他们也在以色列的 Ramat Gan 成立了 Zend Technologies 来管理 PHP 的开发。\n在2000年5月22日，以Zend Engine 1.0为基础的PHP 4正式发布，2004年7月13日则发布了PHP 5，PHP 5则使用了第二代的Zend Engine。PHP包含了许多新特色，像是强化的面向对象功能、引入PDO（PHP Data Objects，一个存取数据库的延伸函数库）、以及许多效能上的增强。PHP 4已经不会继续\n[PHP] PHP\n更新，以鼓励用户转移到PHP 5。\n2008年PHP 5成为了PHP唯一的有在开发的PHP版本。将来的PHP 5.3将会加入Late static binding和一些其他的功能强化。PHP 6 的开发也正在进行中，主要的改进有移除register_globals、magic quotes 和 Safe mode的功能。\nPHP最新稳定版本：5.4.30(2013.6.26)\nPHP最新发布的正式版本：5.5.14(2014.6.24)\nPHP最新测试版本：5.6.0 RC2(2014.6.03)\n2013年6月20日，PHP开发团队自豪地宣布推出PHP 5.5.0。此版本包含了大量的新功能和bug修复。需要开发者特别注意的一点是不再支持 Windows XP 和 2003 系统。\n2014年10月16日，PHP开发团队宣布PHP 5.6.2可用。四安全相关的错误是固定在这个版本，包括修复cve-2014-3668，cve-2014-3669和cve-2014-3670。所有的PHP 5.6鼓励用户升级到这个版本。\n ', '');
INSERT INTO `aws_user_action_history_data` VALUES ('4', 'wecenter二次开发', '', '');
INSERT INTO `aws_user_action_history_data` VALUES ('5', '', '', '');
INSERT INTO `aws_user_action_history_data` VALUES ('6', 'wecenter 文章的上一篇，下一篇', '以下为以前写的上一篇，下一篇现帖出来，如果有想法或者问题，欢迎评论\n文章\n1，改程序文件\n找开app/article/main.php\n\n 在$article_info[\'message\'] = FORMAT::parse_attachs(nl2br(FORMAT::parse_bbcode($article_info[\'message\'])));后面加上\n// 上一页\n        $last_pagesdb = $this-&gt;model(\'article\')-&gt;lastpages();\n        //下一页\n        $next_pagesdb =$this-&gt;model(\'article\')-&gt;nextpages();\n\n找到models/article.php  加上后面两句\n\n// 上一页\n    public function lastpages(){\n        // $this-&gt;dao-&gt;where(&quot;catid=$data[catid] and id&lt;$id&quot;)-&gt;order(\'id desc\')-&gt;find();\n         $id = intval($_GET[\'id\']);\n        $sql = &quot;SELECT id,title FROM &quot; . get_table(\'article\').&quot; WHERE id&lt; &quot;.$id.&quot; ORDER BY id DESC LIMIT 1&quot;;\n        $result = $this-&gt;query_all($sql);\n        if($result){\n            return $result;\n        }else{\n            return $result=false;\n        }\n    }\n==================================================================================================\n    // 下一页\n    public function nextpages(){\n         $id = intval($_GET[\'id\']);\n        $sql = &quot;SELECT id,title FROM &quot; . get_table(\'article\').&quot; WHERE id&gt; &quot;.$id.&quot; ORDER BY id ASC LIMIT 1&quot;;\n        $result = $this-&gt;query_all($sql);\n        if($result){\n            return $result;\n        }else{\n            return $result=false;\n        }\n    }\n==================================================================================================\n模板可以用&lt;?php echo $article_info[\'next_pagesdb\'] ?&gt;//调用。\n里面有两个值一个是title  一个是id  直接ymf调取就行', '');
INSERT INTO `aws_user_action_history_data` VALUES ('7', '把WECENTER安装在BBS或者其它二级目录，管理员登录返回地址错误解决方法 ', '[quote]\n找到/system/functions.inc.php\n第48行，把\nfunction base64_current_path()\n{\n  return base64_encode(\'/\' . str_replace(\'/\' . G_INDEX_SCRIPT, \'\', substr($_SERVER[\'REQUEST_URI\'], strlen(dirname($_SERVER[\'PHP_SELF\'])))));\n}\n改成\nfunction base64_current_path()\n{\n    return base64_encode($_SERVER[\'REQUEST_URI\']);\n}\n改了，暂时没有发现问题，如果有问题可以反缀\n[/quote]\n', '');
INSERT INTO `aws_user_action_history_data` VALUES ('8', 'wecenter指导入文章或者问答', '//菜单\n找到system/config/admin_menu.php - 这个文件控制你的后台菜单在你想要的位置加入到菜单\n我是加到了 工具这个菜单下面\n[quote]\n$config[] = array(\n    \'title\' =&gt; AWS_APP::lang()-&gt;_t(\'工具\'),\n    \'cname\' =&gt; \'job\',\n    \'children\' =&gt; array(\n        array(\n            \'id\' =&gt; 501,\n            \'title\' =&gt; AWS_APP::lang()-&gt;_t(\'系统维护\'),\n            \'url\' =&gt; \'admin/tools/\',\n        )\n        ,\n\n        array(\n            \'id\' =&gt; 5002,\n            \'title\' =&gt; AWS_APP::lang()-&gt;_t(\'导入数据\'),\n            \'url\' =&gt; \'admin/import/\'\n        )\n    )\n);\n[/quote]\n[quote]\n//导入核心文件Phpexcel我是放在system\n[/quote]\n[quote]\n新建立一个 app/admin/import.php 的控制器\n[/quote]\n[quote]\n views/default/admin/import.tpl.htm 后台管理页面\n[/quote]\n\n* views/default/admin/data_import_progress.tpl.htm - View 处理进程显示页面\n* uploads/data_import - 上传文件放置目录\n* uploads/data_import/data_import_demo.xlsx - Excel演示文件格式', '');
INSERT INTO `aws_user_action_history_data` VALUES ('9', '嗨。我来逛逛、', '邮箱验证有问题。额，不支持图片插入。', '');
INSERT INTO `aws_user_action_history_data` VALUES ('10', '呵呵。新域名还在备案中，所有这个很多东西都没管，图片是没开权限', '', '');
INSERT INTO `aws_user_action_history_data` VALUES ('11', '呵呵。新域名还在备案中，所有这个很多东西都没管，图片是没开权限', '', '');
INSERT INTO `aws_user_action_history_data` VALUES ('12', '指定某个分组才可以访问文章', '//指定某个分组才可以访问文章 --》可以用做VIP文章或者章节\n==================\n首先找到/views/default/publish/article.tpl.htm\n在约48行，也就是 &lt;!-- end 文章标题 --&gt; 下面加入\n[quote]\n&lt;!-- 指定分组  -By Jerry 2016.11.1--&gt;\n                                &lt;h3&gt;&lt;?php _e(\'文章标题\'); ?&gt;:&lt;/h3&gt;\n                                &lt;?php if ($this-&gt;groups) { ?&gt;\n                                &lt;div class=&quot;aw-publish-title&quot;&gt;\n                                    &lt;select name=&quot;power_group&quot; id=&quot;&quot; class=&quot;form-control&quot;&gt;\n                                    &lt;option value=&quot;0&quot;&gt;不指定用户组&lt;/option&gt;\n                                    &lt;?php foreach ($this-&gt;groups as $key =&gt; $v): ?&gt;\n                                        &lt;option value=&quot;&lt;?php _e($v[\'group_id\']) ?&gt;&quot;&gt;&lt;?php _e($v[\'group_name\']) ?&gt;&lt;/option&gt;\n                                        &lt;?php endforeach ?&gt;\n                                    &lt;/select&gt;\n                                    \n                                &lt;/div&gt;\n                                &lt;?php } ?&gt;\n                                    &lt;!-- end 指定分组  -By Jerry 2016.11.1--&gt;\n[/quote]\n===================================================\n找到/app/publish/main.php\n在约 18行  加入\n[quote]\n//用户组查询 S  -By Jerry 2016.11.1\n        $groups = $this-&gt;model(\'usergroup\')-&gt;get_groups();\n        TPL::assign(\'groups\', $groups);\n        // var_dump($groups);\n        //用户组查询 E  -By Jerry 2016.11.1\n       \n \n[/quote]\n找到/app/publish/ajax.php 把publish_article_action方法改为改为\n[quote]\n public function publish_article_action()\n    {\n        if (!$this-&gt;user_info[\'permission\'][\'publish_article\'])\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'你没有权限发布文章\')));\n        }\n\n        if (!$_POST[\'title\'])\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, - 1, AWS_APP::lang()-&gt;_t(\'请输入文章标题\')));\n        }\n\n        if (get_setting(\'category_enable\') == \'N\')\n        {\n            $_POST[\'category_id\'] = 1;\n        }\n\n        if (!$_POST[\'category_id\'])\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, -1, AWS_APP::lang()-&gt;_t(\'请选择文章分类\')));\n        }\n\n        if (get_setting(\'question_title_limit\') &gt; 0 AND cjk_strlen($_POST[\'title\']) &gt; get_setting(\'question_title_limit\'))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'文章标题字数不得大于 %s 字节\', get_setting(\'question_title_limit\'))));\n        }\n\n        if (!$this-&gt;user_info[\'permission\'][\'publish_url\'] AND FORMAT::outside_url_exists($_POST[\'message\']))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'你所在的用户组不允许发布站外链接\')));\n        }\n\n        if (!$this-&gt;model(\'publish\')-&gt;insert_attach_is_self_upload($_POST[\'message\'], $_POST[\'attach_ids\']))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'只允许插入当前页面上传的附件\')));\n        }\n\n        if (human_valid(\'question_valid_hour\') AND !AWS_APP::captcha()-&gt;is_validate($_POST[\'seccode_verify\']))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'请填写正确的验证码\')));\n        }\n\n        if ($_POST[\'topics\'])\n        {\n            foreach ($_POST[\'topics\'] AS $key =&gt; $topic_title)\n            {\n                $topic_title = trim($topic_title);\n\n                if (!$topic_title)\n                {\n                    unset($_POST[\'topics\'][$key]);\n                }\n                else\n                {\n                    $_POST[\'topics\'][$key] = $topic_title;\n                }\n            }\n\n            if (get_setting(\'question_topics_limit\') AND sizeof($_POST[\'topics\']) &gt; get_setting(\'question_topics_limit\'))\n            {\n                H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'单个文章话题数量最多为 %s 个, 请调整话题数量\', get_setting(\'question_topics_limit\'))));\n            }\n        }\n        if (get_setting(\'new_question_force_add_topic\') == \'Y\' AND !$_POST[\'topics\'])\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'请为文章添加话题\')));\n        }\n\n        // !注: 来路检测后面不能再放报错提示\n        if (!valid_post_hash($_POST[\'post_hash\']))\n        {\n            H::ajax_json_output(AWS_APP::RSM(null, \'-1\', AWS_APP::lang()-&gt;_t(\'页面停留时间过长,或内容已提交,请刷新页面\')));\n        }\n\n        $this-&gt;model(\'draft\')-&gt;delete_draft(1, \'article\', $this-&gt;user_id);\n\n        if ($this-&gt;publish_approval_valid(array(\n                $_POST[\'title\'],\n                $_POST[\'message\']\n            )))\n        {\n            $this-&gt;model(\'publish\')-&gt;publish_approval(\'article\', array(\n                \'title\' =&gt; $_POST[\'title\'],\n                \'message\' =&gt; $_POST[\'message\'],\n                \'category_id\' =&gt; $_POST[\'category_id\'],\n                \'topics\' =&gt; $_POST[\'topics\'],\n                \'permission_create_topic\' =&gt; $this-&gt;user_info[\'permission\'][\'create_topic\'],\n            ), $this-&gt;user_id, $_POST[\'attach_access_key\']);\n            H::ajax_json_output(AWS_APP::RSM(array(\n                \'url\' =&gt; get_js_url(\'/publish/wait_approval/\')\n            ), 1, null));\n        }\n        else\n        {\n            //changed ==by Jerry 2016.11.1\n            $article_id = $this-&gt;model(\'publish\')-&gt;publish_article($_POST[\'title\'],$_POST[\'power_group\'], $_POST[\'message\'], $this-&gt;user_id, $_POST[\'topics\'], $_POST[\'category_id\'], $_POST[\'attach_access_key\'], $this-&gt;user_info[\'permission\'][\'create_topic\']);\n             //changed ==by Jerry 2016.11.1\n\n            if ($_POST[\'_is_mobile\'])\n            {\n                $url = get_js_url(\'/m/article/\' . $article_id);\n            }\n            else\n            {\n                $url = get_js_url(\'/article/\' . $article_id);\n            }\n\n            H::ajax_json_output(AWS_APP::RSM(array(\n                \'url\' =&gt; $url\n            ), 1, null));\n        }\n    }\n[/quote]\n在models下面新建个模型，名字为usergroup.php\n[quote]\n&lt;?php\n\nif (!defined(\'IN_ANWSION\'))\n{\n    die;\n}\n\nclass usergroup_class extends AWS_MODEL\n{\n \n\n    public function get_groups()\n    {\n       \n\n        return $this-&gt;fetch_all(\'users_group\');\n\n    }\n\n   \n}\n\n \n[/quote]\n//找到models/publish.php  更改publish_article方法为\n[quote]\npublic function publish_article($title, $power_group,$message, $uid, $topics = null, $category_id = null, $attach_access_key = null, $create_topic = true)\n    {\n        if ($article_id = $this-&gt;insert(\'article\', array(\n            \'uid\' =&gt; intval($uid),\n            \'title\' =&gt; htmlspecialchars($title),\n            \'message\' =&gt; htmlspecialchars($message),\n            \'category_id\' =&gt; intval($category_id),\n             //changed ==by Jerry 2016.11.1\n            \'power_group\'=&gt;intval($power_group),\n             //changed ==by Jerry 2016.11.1\n            \'add_time\' =&gt; time()\n        )))\n        {\n            set_human_valid(\'question_valid_hour\');\n\n            if (is_array($topics))\n            {\n                foreach ($topics as $key =&gt; $topic_title)\n                {\n                    $topic_id = $this-&gt;model(\'topic\')-&gt;save_topic($topic_title, $uid, $create_topic);\n\n                    $this-&gt;model(\'topic\')-&gt;save_topic_relation($uid, $topic_id, $article_id, \'article\');\n                }\n            }\n\n            if ($attach_access_key)\n            {\n                $this-&gt;model(\'publish\')-&gt;update_attach(\'article\', $article_id, $attach_access_key);\n            }\n\n            $this-&gt;model(\'search_fulltext\')-&gt;push_index(\'article\', $title, $article_id);\n\n            // 记录日志\n            ACTION_LOG::save_action($uid, $article_id, ACTION_LOG::CATEGORY_QUESTION, ACTION_LOG::ADD_ARTICLE, $title, $message, 0);\n\n            $this-&gt;model(\'posts\')-&gt;set_posts_index($article_id, \'article\');\n\n            $this-&gt;shutdown_update(\'users\', array(\n                \'article_count\' =&gt; $this-&gt;count(\'article\', \'uid = \' . intval($uid))\n            ), \'uid = \' . intval($uid));\n        }\n\n        return $article_id;\n    }\n \n[/quote]\n加入以下代码\n[quote]\nsql升级\n \nALTER TABLE `aws_article`\nADD COLUMN `power_group`  int(11) NULL AFTER `sort`;\n[/quote]\n到此处，发布文章权限就搞定了，下面就是验证组了。\n=====================================================\n找到app/article/main.php\n ', '');

-- ----------------------------
-- Table structure for `aws_user_action_history_fresh`
-- ----------------------------
DROP TABLE IF EXISTS `aws_user_action_history_fresh`;
CREATE TABLE `aws_user_action_history_fresh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `history_id` int(11) NOT NULL,
  `associate_id` int(11) NOT NULL,
  `associate_type` tinyint(1) NOT NULL,
  `associate_action` smallint(3) NOT NULL,
  `add_time` int(10) NOT NULL DEFAULT '0',
  `uid` int(10) NOT NULL DEFAULT '0',
  `anonymous` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `associate` (`associate_type`,`associate_action`),
  KEY `add_time` (`add_time`),
  KEY `uid` (`uid`),
  KEY `history_id` (`history_id`),
  KEY `associate_with_id` (`id`,`associate_type`,`associate_action`),
  KEY `associate_with_uid` (`uid`,`associate_type`,`associate_action`),
  KEY `anonymous` (`anonymous`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_user_action_history_fresh
-- ----------------------------
INSERT INTO `aws_user_action_history_fresh` VALUES ('2', '2', '2', '4', '406', '1477553402', '1', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('3', '3', '1', '1', '501', '1477553403', '1', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('5', '5', '3', '4', '406', '1477575772', '2', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('6', '6', '2', '1', '501', '1477575772', '2', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('7', '7', '3', '1', '501', '1477618858', '2', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('8', '8', '4', '1', '501', '1477635297', '2', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('9', '9', '1', '1', '101', '1477724625', '4', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('10', '10', '1', '2', '201', '1477788491', '2', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('11', '11', '1', '1', '201', '1477788491', '2', '0');
INSERT INTO `aws_user_action_history_fresh` VALUES ('12', '12', '5', '1', '501', '1477995072', '2', '0');

-- ----------------------------
-- Table structure for `aws_user_follow`
-- ----------------------------
DROP TABLE IF EXISTS `aws_user_follow`;
CREATE TABLE `aws_user_follow` (
  `follow_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `fans_uid` int(11) DEFAULT NULL COMMENT '关注人的UID',
  `friend_uid` int(11) DEFAULT NULL COMMENT '被关注人的uid',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`follow_id`),
  KEY `fans_uid` (`fans_uid`),
  KEY `friend_uid` (`friend_uid`),
  KEY `user_follow` (`fans_uid`,`friend_uid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户关注表';

-- ----------------------------
-- Records of aws_user_follow
-- ----------------------------
INSERT INTO `aws_user_follow` VALUES ('1', '2', '1', '1477574537');
INSERT INTO `aws_user_follow` VALUES ('3', '4', '1', '1477724347');
INSERT INTO `aws_user_follow` VALUES ('4', '5', '1', '1477875258');

-- ----------------------------
-- Table structure for `aws_users`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users`;
CREATE TABLE `aws_users` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户的 UID',
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `email` varchar(255) DEFAULT NULL COMMENT 'EMAIL',
  `mobile` varchar(16) DEFAULT NULL COMMENT '用户手机',
  `password` varchar(32) DEFAULT NULL COMMENT '用户密码',
  `salt` varchar(16) DEFAULT NULL COMMENT '用户附加混淆码',
  `avatar_file` varchar(128) DEFAULT NULL COMMENT '头像文件',
  `sex` tinyint(1) DEFAULT NULL COMMENT '性别',
  `birthday` int(10) DEFAULT NULL COMMENT '生日',
  `province` varchar(64) DEFAULT NULL COMMENT '省',
  `city` varchar(64) DEFAULT NULL COMMENT '市',
  `job_id` int(10) DEFAULT '0' COMMENT '职业ID',
  `reg_time` int(10) DEFAULT NULL COMMENT '注册时间',
  `reg_ip` bigint(12) DEFAULT NULL COMMENT '注册IP',
  `last_login` int(10) DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` bigint(12) DEFAULT NULL COMMENT '最后登录 IP',
  `online_time` int(10) DEFAULT '0' COMMENT '在线时间',
  `last_active` int(10) DEFAULT NULL COMMENT '最后活跃时间',
  `notification_unread` int(11) NOT NULL DEFAULT '0' COMMENT '未读系统通知',
  `inbox_unread` int(11) NOT NULL DEFAULT '0' COMMENT '未读短信息',
  `inbox_recv` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0-所有人可以发给我,1-我关注的人',
  `fans_count` int(10) NOT NULL DEFAULT '0' COMMENT '粉丝数',
  `friend_count` int(10) NOT NULL DEFAULT '0' COMMENT '观众数',
  `invite_count` int(10) NOT NULL DEFAULT '0' COMMENT '邀请我回答数量',
  `article_count` int(10) NOT NULL DEFAULT '0' COMMENT '文章数量',
  `question_count` int(10) NOT NULL DEFAULT '0' COMMENT '问题数量',
  `answer_count` int(10) NOT NULL DEFAULT '0' COMMENT '回答数量',
  `topic_focus_count` int(10) NOT NULL DEFAULT '0' COMMENT '关注话题数量',
  `invitation_available` int(10) NOT NULL DEFAULT '0' COMMENT '邀请数量',
  `group_id` int(10) DEFAULT '0' COMMENT '用户组',
  `reputation_group` int(10) DEFAULT '0' COMMENT '威望对应组',
  `forbidden` tinyint(1) DEFAULT '0' COMMENT '是否禁止用户',
  `valid_email` tinyint(1) DEFAULT '0' COMMENT '邮箱验证',
  `is_first_login` tinyint(1) DEFAULT '1' COMMENT '首次登录标记',
  `agree_count` int(10) DEFAULT '0' COMMENT '赞同数量',
  `thanks_count` int(10) DEFAULT '0' COMMENT '感谢数量',
  `views_count` int(10) DEFAULT '0' COMMENT '个人主页查看数量',
  `reputation` int(10) DEFAULT '0' COMMENT '威望',
  `reputation_update_time` int(10) DEFAULT '0' COMMENT '威望更新',
  `weibo_visit` tinyint(1) DEFAULT '1' COMMENT '微博允许访问',
  `integral` int(10) DEFAULT '0',
  `draft_count` int(10) DEFAULT NULL,
  `common_email` varchar(255) DEFAULT NULL COMMENT '常用邮箱',
  `url_token` varchar(32) DEFAULT NULL COMMENT '个性网址',
  `url_token_update` int(10) DEFAULT '0',
  `verified` varchar(32) DEFAULT NULL,
  `default_timezone` varchar(32) DEFAULT NULL,
  `email_settings` varchar(255) DEFAULT '',
  `weixin_settings` varchar(255) DEFAULT '',
  `recent_topics` text,
  PRIMARY KEY (`uid`),
  KEY `user_name` (`user_name`),
  KEY `email` (`email`),
  KEY `reputation` (`reputation`),
  KEY `reputation_update_time` (`reputation_update_time`),
  KEY `group_id` (`group_id`),
  KEY `agree_count` (`agree_count`),
  KEY `thanks_count` (`thanks_count`),
  KEY `forbidden` (`forbidden`),
  KEY `valid_email` (`valid_email`),
  KEY `last_active` (`last_active`),
  KEY `integral` (`integral`),
  KEY `url_token` (`url_token`),
  KEY `verified` (`verified`),
  KEY `answer_count` (`answer_count`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_users
-- ----------------------------
INSERT INTO `aws_users` VALUES ('1', 'admin', '502443279@qq.com', null, '6073343c78f77028f06b36e4eff78115', 'jdfu', '000/00/00/01_avatar_min.jpg', null, null, null, null, '0', '1477537163', '2130706433', '1477575474', '249266606', '4086', '1477575548', '0', '0', '0', '0', '0', '0', '1', '0', '0', '1', '10', '1', '5', '0', '1', '0', '0', '0', '8', '0', '1478351528', '1', '2020', '0', null, null, '0', null, null, '', '', 'a:1:{i:0;s:3:\"PHP\";}');
INSERT INTO `aws_users` VALUES ('2', 'Jerry', '852777213@qq.com', '', 'd6cdeba41fb23f69748138b43c33849b', 'pkua', '000/00/00/02_avatar_min.jpg', '1', null, '', '', '0', '1477574537', '249266606', '1477577891', '249266606', '107069', '1478352313', '0', '0', '0', '0', '0', '0', '4', '0', '1', '1', '5', '1', '5', '0', '1', '0', '0', '0', '29', '0', '1478351528', '1', '2035', '0', null, null, '0', null, '', 'a:2:{s:9:\"FOLLOW_ME\";s:1:\"N\";s:10:\"NEW_ANSWER\";s:1:\"N\";}', '', 'a:1:{i:0;s:20:\"wecenter二次开发\";}');
INSERT INTO `aws_users` VALUES ('4', 'lm', '1345632628@qq.com', '', '9c39221daa253c49f2f9464c5e6dbf61', 'mrgq', null, '0', null, null, null, '0', '1477724347', '717610247', '1478168677', '717610247', '1275', '1478180730', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '5', '4', '5', '0', '0', '1', '0', '0', '10', '0', '1478351528', '1', '1985', '0', null, null, '0', null, null, 'a:2:{s:9:\"FOLLOW_ME\";s:1:\"N\";s:10:\"NEW_ANSWER\";s:1:\"N\";}', '', null);
INSERT INTO `aws_users` VALUES ('5', '呵呵呵哒', '634395479@qq.com', '', '793573c201b8cf5f1fafa35860b3ab8d', 'xbkh', null, '1', null, '江苏省', '南京市', '4', '1477875258', '827151994', '1477875285', '827151994', '1', '1477875285', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '5', '3', '5', '0', '0', '1', '0', '0', '6', '0', '1478351528', '1', '2000', null, null, null, '0', null, null, 'a:2:{s:9:\"FOLLOW_ME\";s:1:\"N\";s:10:\"NEW_ANSWER\";s:1:\"N\";}', '', null);

-- ----------------------------
-- Table structure for `aws_users_attrib`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_attrib`;
CREATE TABLE `aws_users_attrib` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `uid` int(11) DEFAULT NULL COMMENT '用户UID',
  `introduction` varchar(255) DEFAULT NULL COMMENT '个人简介',
  `signature` varchar(255) DEFAULT NULL COMMENT '个人签名',
  `qq` bigint(15) DEFAULT NULL,
  `homepage` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户附加属性表';

-- ----------------------------
-- Records of aws_users_attrib
-- ----------------------------
INSERT INTO `aws_users_attrib` VALUES ('1', '1', null, '', null, null);
INSERT INTO `aws_users_attrib` VALUES ('2', '2', null, '', '0', '');
INSERT INTO `aws_users_attrib` VALUES ('4', '4', null, null, null, null);
INSERT INTO `aws_users_attrib` VALUES ('5', '5', null, '帅气', null, null);

-- ----------------------------
-- Table structure for `aws_users_facebook`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_facebook`;
CREATE TABLE `aws_users_facebook` (
  `id` bigint(20) unsigned NOT NULL,
  `uid` int(11) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `locale` varchar(16) DEFAULT NULL,
  `timezone` tinyint(3) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `expires_time` int(10) unsigned NOT NULL DEFAULT '0',
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `access_token` (`access_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_users_facebook
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_users_google`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_google`;
CREATE TABLE `aws_users_google` (
  `id` varchar(64) NOT NULL,
  `uid` int(11) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `locale` varchar(16) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `gender` varchar(8) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `access_token` varchar(128) DEFAULT NULL,
  `refresh_token` varchar(128) DEFAULT NULL,
  `expires_time` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `access_token` (`access_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_users_google
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_users_group`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_group`;
CREATE TABLE `aws_users_group` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(3) DEFAULT '0' COMMENT '0-会员组 1-系统组',
  `custom` tinyint(1) DEFAULT '0' COMMENT '是否自定义',
  `group_name` varchar(50) NOT NULL,
  `reputation_lower` int(11) DEFAULT '0',
  `reputation_higer` int(11) DEFAULT '0',
  `reputation_factor` float DEFAULT '0' COMMENT '威望系数',
  `permission` text COMMENT '权限设置',
  PRIMARY KEY (`group_id`),
  KEY `type` (`type`),
  KEY `custom` (`custom`)
) ENGINE=MyISAM AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='用户组';

-- ----------------------------
-- Records of aws_users_group
-- ----------------------------
INSERT INTO `aws_users_group` VALUES ('1', '0', '0', '超级管理员', '0', '0', '5', 'a:15:{s:16:\"is_administortar\";s:1:\"1\";s:12:\"is_moderator\";s:1:\"1\";s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:13:\"edit_question\";s:1:\"1\";s:10:\"edit_topic\";s:1:\"1\";s:12:\"manage_topic\";s:1:\"1\";s:12:\"create_topic\";s:1:\"1\";s:17:\"redirect_question\";s:1:\"1\";s:13:\"upload_attach\";s:1:\"1\";s:11:\"publish_url\";s:1:\"1\";s:15:\"publish_article\";s:1:\"1\";s:12:\"edit_article\";s:1:\"1\";s:19:\"edit_question_topic\";s:1:\"1\";s:15:\"publish_comment\";s:1:\"1\";}');
INSERT INTO `aws_users_group` VALUES ('2', '0', '0', '前台管理员', '0', '0', '4', 'a:14:{s:12:\"is_moderator\";s:1:\"1\";s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:13:\"edit_question\";s:1:\"1\";s:10:\"edit_topic\";s:1:\"1\";s:12:\"manage_topic\";s:1:\"1\";s:12:\"create_topic\";s:1:\"1\";s:17:\"redirect_question\";s:1:\"1\";s:13:\"upload_attach\";s:1:\"1\";s:11:\"publish_url\";s:1:\"1\";s:15:\"publish_article\";s:1:\"1\";s:12:\"edit_article\";s:1:\"1\";s:19:\"edit_question_topic\";s:1:\"1\";s:15:\"publish_comment\";s:1:\"1\";}');
INSERT INTO `aws_users_group` VALUES ('3', '0', '0', '未验证会员', '0', '0', '0', 'a:5:{s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:11:\"human_valid\";s:1:\"1\";s:19:\"question_valid_hour\";s:1:\"2\";s:17:\"answer_valid_hour\";s:1:\"2\";}');
INSERT INTO `aws_users_group` VALUES ('4', '0', '0', '普通会员', '0', '0', '0', 'a:3:{s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:19:\"question_valid_hour\";s:2:\"10\";s:17:\"answer_valid_hour\";s:2:\"10\";}');
INSERT INTO `aws_users_group` VALUES ('5', '1', '0', '注册会员', '0', '100', '1', 'a:6:{s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:11:\"human_valid\";s:1:\"1\";s:19:\"question_valid_hour\";s:1:\"5\";s:17:\"answer_valid_hour\";s:1:\"5\";s:15:\"publish_comment\";s:1:\"1\";}');
INSERT INTO `aws_users_group` VALUES ('6', '1', '0', '初级会员', '100', '200', '1', 'a:8:{s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:13:\"upload_attach\";s:1:\"1\";s:11:\"publish_url\";s:1:\"1\";s:19:\"question_valid_hour\";s:1:\"5\";s:17:\"answer_valid_hour\";s:1:\"5\";s:15:\"publish_article\";s:1:\"1\";s:19:\"edit_question_topic\";s:1:\"1\";}');
INSERT INTO `aws_users_group` VALUES ('7', '1', '0', '中级会员', '200', '500', '1', 'a:9:{s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:10:\"edit_topic\";s:1:\"1\";s:12:\"create_topic\";s:1:\"1\";s:17:\"redirect_question\";s:1:\"1\";s:13:\"upload_attach\";s:1:\"1\";s:11:\"publish_url\";s:1:\"1\";s:15:\"publish_article\";s:1:\"1\";s:15:\"publish_comment\";s:1:\"1\";}');
INSERT INTO `aws_users_group` VALUES ('8', '1', '0', '高级会员', '500', '1000', '1', 'a:11:{s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:13:\"edit_question\";s:1:\"1\";s:10:\"edit_topic\";s:1:\"1\";s:12:\"create_topic\";s:1:\"1\";s:17:\"redirect_question\";s:1:\"1\";s:13:\"upload_attach\";s:1:\"1\";s:11:\"publish_url\";s:1:\"1\";s:15:\"publish_article\";s:1:\"1\";s:19:\"edit_question_topic\";s:1:\"1\";s:15:\"publish_comment\";s:1:\"1\";}');
INSERT INTO `aws_users_group` VALUES ('9', '1', '0', '核心会员', '1000', '999999', '1', 'a:12:{s:16:\"publish_question\";s:1:\"1\";s:21:\"publish_approval_time\";a:2:{s:5:\"start\";s:0:\"\";s:3:\"end\";s:0:\"\";}s:13:\"edit_question\";s:1:\"1\";s:10:\"edit_topic\";s:1:\"1\";s:12:\"manage_topic\";s:1:\"1\";s:12:\"create_topic\";s:1:\"1\";s:17:\"redirect_question\";s:1:\"1\";s:13:\"upload_attach\";s:1:\"1\";s:11:\"publish_url\";s:1:\"1\";s:15:\"publish_article\";s:1:\"1\";s:19:\"edit_question_topic\";s:1:\"1\";s:15:\"publish_comment\";s:1:\"1\";}');
INSERT INTO `aws_users_group` VALUES ('99', '0', '0', '游客', '0', '0', '0', 'a:9:{s:10:\"visit_site\";s:1:\"1\";s:13:\"visit_explore\";s:1:\"1\";s:12:\"search_avail\";s:1:\"1\";s:14:\"visit_question\";s:1:\"1\";s:11:\"visit_topic\";s:1:\"1\";s:13:\"visit_feature\";s:1:\"1\";s:12:\"visit_people\";s:1:\"1\";s:13:\"visit_chapter\";s:1:\"1\";s:11:\"answer_show\";s:1:\"1\";}');

-- ----------------------------
-- Table structure for `aws_users_notification_setting`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_notification_setting`;
CREATE TABLE `aws_users_notification_setting` (
  `notice_setting_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `uid` int(11) NOT NULL,
  `data` text COMMENT '设置数据',
  PRIMARY KEY (`notice_setting_id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='通知设定';

-- ----------------------------
-- Records of aws_users_notification_setting
-- ----------------------------
INSERT INTO `aws_users_notification_setting` VALUES ('1', '2', 'a:0:{}');
INSERT INTO `aws_users_notification_setting` VALUES ('2', '3', 'a:0:{}');
INSERT INTO `aws_users_notification_setting` VALUES ('3', '4', 'a:0:{}');
INSERT INTO `aws_users_notification_setting` VALUES ('4', '5', 'a:0:{}');

-- ----------------------------
-- Table structure for `aws_users_online`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_online`;
CREATE TABLE `aws_users_online` (
  `uid` int(11) NOT NULL COMMENT '用户 ID',
  `last_active` int(11) DEFAULT '0' COMMENT '上次活动时间',
  `ip` bigint(12) DEFAULT '0' COMMENT '客户端ip',
  `active_url` varchar(255) DEFAULT NULL COMMENT '停留页面',
  `user_agent` varchar(255) DEFAULT NULL COMMENT '用户客户端信息',
  KEY `uid` (`uid`),
  KEY `last_active` (`last_active`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='在线用户列表';

-- ----------------------------
-- Records of aws_users_online
-- ----------------------------
INSERT INTO `aws_users_online` VALUES ('2', '1478352313', '249266505', 'http://cooldreamer.com/ask/?/admin/settings/category-register', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:49.0) Gecko/20100101 Firefox/49.0');

-- ----------------------------
-- Table structure for `aws_users_qq`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_qq`;
CREATE TABLE `aws_users_qq` (
  `id` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL COMMENT '用户在本地的UID',
  `nickname` varchar(64) DEFAULT NULL,
  `openid` varchar(128) DEFAULT '',
  `gender` varchar(8) DEFAULT NULL,
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  `access_token` varchar(64) DEFAULT NULL,
  `refresh_token` varchar(64) DEFAULT NULL,
  `expires_time` int(10) DEFAULT NULL,
  `figureurl` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `add_time` (`add_time`),
  KEY `access_token` (`access_token`),
  KEY `openid` (`openid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_users_qq
-- ----------------------------
INSERT INTO `aws_users_qq` VALUES ('1', '2', 'Jerry-chen', 'C9CC6E1860AF406A7F4A2FC9D747EB68', 'male', '1477577891', '6D24F3D0908F55D0A8A314CE13ABD9F8', '3E4A138247BFBE9BE021D66D8B789C8B', '1486102799', 'http://q.qlogo.cn/qqapp/101142483/C9CC6E1860AF406A7F4A2FC9D747EB68/100');

-- ----------------------------
-- Table structure for `aws_users_sina`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_sina`;
CREATE TABLE `aws_users_sina` (
  `id` bigint(11) NOT NULL COMMENT '新浪用户 ID',
  `uid` int(11) NOT NULL COMMENT '用户在本地的UID',
  `name` varchar(64) DEFAULT NULL COMMENT '微博昵称',
  `location` varchar(255) DEFAULT NULL COMMENT '地址',
  `description` text COMMENT '个人描述',
  `url` varchar(255) DEFAULT NULL COMMENT '用户博客地址',
  `profile_image_url` varchar(255) DEFAULT NULL COMMENT 'Sina 自定义头像地址',
  `gender` varchar(8) DEFAULT NULL,
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  `expires_time` int(10) DEFAULT '0' COMMENT '过期时间',
  `access_token` varchar(64) DEFAULT NULL,
  `last_msg_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `access_token` (`access_token`),
  KEY `last_msg_id` (`last_msg_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_users_sina
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_users_twitter`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_twitter`;
CREATE TABLE `aws_users_twitter` (
  `id` bigint(20) unsigned NOT NULL,
  `uid` int(11) unsigned NOT NULL,
  `name` varchar(128) DEFAULT NULL,
  `screen_name` varchar(128) DEFAULT NULL,
  `location` varchar(64) DEFAULT NULL,
  `time_zone` varchar(64) DEFAULT NULL,
  `lang` varchar(16) DEFAULT NULL,
  `profile_image_url` varchar(255) DEFAULT NULL,
  `add_time` int(10) unsigned NOT NULL DEFAULT '0',
  `access_token` varchar(255) NOT NULL DEFAULT 'a:2:{s:11:"oauth_token";s:0:"";s:18:"oauth_token_secret";s:0:"";}',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `access_token` (`access_token`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_users_twitter
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_users_ucenter`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_ucenter`;
CREATE TABLE `aws_users_ucenter` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0',
  `uc_uid` int(11) DEFAULT '0',
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `uc_uid` (`uc_uid`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_users_ucenter
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_users_weixin`
-- ----------------------------
DROP TABLE IF EXISTS `aws_users_weixin`;
CREATE TABLE `aws_users_weixin` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `openid` varchar(255) NOT NULL,
  `expires_in` int(10) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `scope` varchar(64) DEFAULT NULL,
  `headimgurl` varchar(255) DEFAULT NULL,
  `nickname` varchar(64) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT '0',
  `province` varchar(32) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `country` varchar(32) DEFAULT NULL,
  `add_time` int(10) NOT NULL,
  `latitude` float DEFAULT NULL,
  `longitude` float DEFAULT NULL,
  `location_update` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid` (`uid`),
  KEY `openid` (`openid`),
  KEY `expires_in` (`expires_in`),
  KEY `scope` (`scope`),
  KEY `sex` (`sex`),
  KEY `province` (`province`),
  KEY `city` (`city`),
  KEY `country` (`country`),
  KEY `add_time` (`add_time`),
  KEY `latitude` (`latitude`,`longitude`),
  KEY `location_update` (`location_update`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_users_weixin
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_verify_apply`
-- ----------------------------
DROP TABLE IF EXISTS `aws_verify_apply`;
CREATE TABLE `aws_verify_apply` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `attach` varchar(255) DEFAULT NULL,
  `time` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `data` text,
  `status` tinyint(1) DEFAULT '0',
  `type` varchar(16) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `time` (`time`),
  KEY `name` (`name`,`status`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_verify_apply
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_weibo_msg`
-- ----------------------------
DROP TABLE IF EXISTS `aws_weibo_msg`;
CREATE TABLE `aws_weibo_msg` (
  `id` bigint(20) NOT NULL,
  `created_at` int(10) NOT NULL,
  `msg_author_uid` bigint(20) NOT NULL,
  `text` varchar(255) NOT NULL,
  `access_key` varchar(32) NOT NULL,
  `has_attach` tinyint(1) NOT NULL DEFAULT '0',
  `uid` int(10) NOT NULL,
  `weibo_uid` bigint(20) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `ticket_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_at` (`created_at`),
  KEY `uid` (`uid`),
  KEY `weibo_uid` (`weibo_uid`),
  KEY `question_id` (`question_id`),
  KEY `ticket_id` (`ticket_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='新浪微博消息列表';

-- ----------------------------
-- Records of aws_weibo_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_weixin_accounts`
-- ----------------------------
DROP TABLE IF EXISTS `aws_weixin_accounts`;
CREATE TABLE `aws_weixin_accounts` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `weixin_mp_token` varchar(255) NOT NULL,
  `weixin_account_role` varchar(20) DEFAULT 'base',
  `weixin_app_id` varchar(255) DEFAULT '',
  `weixin_app_secret` varchar(255) DEFAULT '',
  `weixin_mp_menu` text,
  `weixin_subscribe_message_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `weixin_no_result_message_key` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `weixin_encoding_aes_key` varchar(43) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `weixin_mp_token` (`weixin_mp_token`),
  KEY `weixin_account_role` (`weixin_account_role`),
  KEY `weixin_app_id` (`weixin_app_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信多账号设置';

-- ----------------------------
-- Records of aws_weixin_accounts
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_weixin_login`
-- ----------------------------
DROP TABLE IF EXISTS `aws_weixin_login`;
CREATE TABLE `aws_weixin_login` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `token` int(10) NOT NULL,
  `uid` int(10) DEFAULT NULL,
  `session_id` varchar(32) NOT NULL,
  `expire` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `session_id` (`session_id`),
  KEY `token` (`token`),
  KEY `expire` (`expire`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_weixin_login
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_weixin_message`
-- ----------------------------
DROP TABLE IF EXISTS `aws_weixin_message`;
CREATE TABLE `aws_weixin_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weixin_id` varchar(32) NOT NULL,
  `content` varchar(255) NOT NULL,
  `action` text,
  `time` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `weixin_id` (`weixin_id`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_weixin_message
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_weixin_msg`
-- ----------------------------
DROP TABLE IF EXISTS `aws_weixin_msg`;
CREATE TABLE `aws_weixin_msg` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `msg_id` bigint(20) NOT NULL,
  `group_name` varchar(255) NOT NULL DEFAULT '未分组',
  `status` varchar(15) NOT NULL DEFAULT 'unsent',
  `error_num` int(10) DEFAULT NULL,
  `main_msg` text,
  `articles_info` text,
  `questions_info` text,
  `create_time` int(10) NOT NULL,
  `filter_count` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `msg_id` (`msg_id`),
  KEY `group_name` (`group_name`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信群发列表';

-- ----------------------------
-- Records of aws_weixin_msg
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_weixin_qr_code`
-- ----------------------------
DROP TABLE IF EXISTS `aws_weixin_qr_code`;
CREATE TABLE `aws_weixin_qr_code` (
  `scene_id` mediumint(5) NOT NULL AUTO_INCREMENT,
  `ticket` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `subscribe_num` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`scene_id`),
  KEY `ticket` (`ticket`),
  KEY `subscribe_num` (`subscribe_num`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信二维码';

-- ----------------------------
-- Records of aws_weixin_qr_code
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_weixin_reply_rule`
-- ----------------------------
DROP TABLE IF EXISTS `aws_weixin_reply_rule`;
CREATE TABLE `aws_weixin_reply_rule` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account_id` int(10) NOT NULL DEFAULT '0',
  `keyword` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `image_file` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT '',
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `link` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `sort_status` int(10) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `keyword` (`keyword`),
  KEY `enabled` (`enabled`),
  KEY `sort_status` (`sort_status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of aws_weixin_reply_rule
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_weixin_third_party_api`
-- ----------------------------
DROP TABLE IF EXISTS `aws_weixin_third_party_api`;
CREATE TABLE `aws_weixin_third_party_api` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account_id` int(10) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `rank` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account_id` (`account_id`),
  KEY `enabled` (`enabled`),
  KEY `rank` (`rank`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信第三方接入';

-- ----------------------------
-- Records of aws_weixin_third_party_api
-- ----------------------------

-- ----------------------------
-- Table structure for `aws_work_experience`
-- ----------------------------
DROP TABLE IF EXISTS `aws_work_experience`;
CREATE TABLE `aws_work_experience` (
  `work_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增ID',
  `uid` int(11) DEFAULT NULL COMMENT '用户ID',
  `start_year` int(11) DEFAULT NULL COMMENT '开始年份',
  `end_year` int(11) DEFAULT NULL COMMENT '结束年月',
  `company_name` varchar(64) DEFAULT NULL COMMENT '公司名',
  `job_id` int(11) DEFAULT NULL COMMENT '职位ID',
  `add_time` int(10) DEFAULT NULL COMMENT '添加时间',
  PRIMARY KEY (`work_id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='工作经历';

-- ----------------------------
-- Records of aws_work_experience
-- ----------------------------
