 **欢迎star 或者fork 您的支持是我们持续的动力** 
--
内测地址：http://www.thinkask.cn
### 简介

thinkask 是一个基于php7开发、tp5框架、免费开源的，问答管理系统

### 大道至简的开发理念
无论从底层实现还是应用开发，我们都倡导用最少的代码完成相同的功能，正是由于对简单的执着和代码的修炼，让我们长期保持出色的性能和极速的开发体验。在主流PHP开发框架的评测数据中表现卓越，简单和快速开发是我们不变的宗旨。

### 安全性
 XSS安全防护
 表单自动验证
 强制数据类型转换
 输入数据过滤
 表单令牌验证
 防SQL注入
 图像上传检测
 **运行环境** 
  PHP >= 5.4.0 （完美支持PHP7）
  PDO PHP ExtensionMBstring 
  PHP ExtensionCURL 
  PHP Extension

### DocumentRoot指向 
thinkask/public所在目录
---
### URL重写:

apache 重写：
```
<IfModule mod_rewrite.c>
Options +FollowSymlinks -Multiviews
RewriteEngine on

RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ index.php/$1 [QSA,PT,L]
</IfModule>
```
phpstudy重写：
```
<IfModule mod_rewrite.c>
Options +FollowSymlinks -Multiviews
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-d
RewriteCond %{REQUEST_FILENAME} !-f
RewriteRule ^(.*)$ index.php [L,E=PATH_INFO:$1]
</IfModule>
```
---
### 语言
支持多语言扩展，更好，更方便的成为国际化的问答系统


### 开源协议
遵循Apache2开源协议发布，Apache Licence是著名的非盈利开源组织Apache采用的协议。该协议和BSD类似，鼓励代码共享和尊重原作者的著作权，同样允许代码修改，再作为开源或商业软件发布。

### 交流群：   485114585
### 安装说明
---
 **普通安装：** 
1. 首先，把SQL文件里面的数据库导入到MYSQL
2. 修改配置文件database.conf的相关配置选项
---
 **WECENTER库共用** 
如果你使用的是WECENTER3.1.9,可以直接把database配置文件指向你的WECENTER库（注意编辑为UTF8）

---

---
 **相关图片：** 
![输入图片说明](http://git.oschina.net/uploads/images/2016/1106/144002_e6a6489b_92619.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/1106/144012_023785d2_92619.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/1106/144027_97e85406_92619.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/1106/144036_375a6171_92619.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/1106/144048_d5b6b5bf_92619.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/1106/144055_3cdb3826_92619.png "在这里输入图片标题")

---


