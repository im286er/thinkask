<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:40:"../template/5ihelp/admin\user\index.html";i:1478566143;s:43:"../template/5ihelp/admin\public\header.html";i:1478566143;s:43:"../template/5ihelp/admin\public\footer.html";i:1478566143;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title><?php echo $title; ?></title>

    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="description" content="<?php echo $description; ?>">

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->


    <link href="/static/plus/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/plus/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/plus/css/animate.min.css" rel="stylesheet">
    <link href="/static/plus/css/style.min.css?v=4.0.0" rel="stylesheet">
 <link href="/static/plus/css/plugins/chosen/chosen.css" rel="stylesheet">
 <link href="/static/plus/css/plugins/iCheck/custom.css" rel="stylesheet">
 <script src="/static/plus/js/jquery.min.js?v=2.1.4"></script>
 <script src="/static/plus/js/plugins/layer/layer.js"></script>
 <script src="/static/cmz/hdjs.js" type="text/javascript"></script>  
 <script src="/static/cmz/jquery.form.js" type="text/javascript"></script>

<link href="/static/plus/css/plugins/jsTree/style.min.css" rel="stylesheet">


   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    layer.config({
        extend: ['skin/moon/style.css'], //加载新皮肤
        skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    });

    </script>
     <link href="/static/cmz/cmzcss/cmz.css" rel="stylesheet">
</head>
<style type="text/css">
  
  .aw-content-wrap{padding: 15px;}
</style>


<style type="text/css">
    #content-main{padding: 5px;}
</style>
<body class="gray-bg">
    <div class="wrapper wrapper-content">
       <div class="row" style="background:#fff;">
           <div class="col-sm-3">
                <div class="ibox float-e-margins">
                    <div class="ibox-content mailbox-content">
                        <div class="file-manager">
                         <!-- <div data-toggle="buttons-checkbox" class="btn-group">
                                    <a href="javascript:;" url="/" name="结构列表" class="btn btn-primary frAlert" type="button"><i class="fa fa-bars"></i> 分组列表</a>
                                    <a href="javascript:;" url="/" name="新建组织结构" class="btn btn-primary frAlert" type="button"><i class="fa fa-sticky-note"></i> 新建分组</a>
                                  
                                </div> -->
                         
                            <div class="space-25"></div>
                            <h5>分类</h5>
                            <ul class="folder-list m-b-md" style="padding: 0">
                            <?php if(is_array($group) || $group instanceof \think\Collection): $i = 0; $__LIST__ = $group;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                              <li class="active" id="lefList">   
                                <p style="padding-top:15px;"><i class="fa   <?php if($v['group_id']==$group_id): ?>fa-check-square<?php else: ?>fa-square-o<?php endif; ?>"></i> <i class="fa fa-file-text-o"></i> <?php echo $v['group_name']; ?>
                                <a style="padding:5px 10px;margin-right:5px;" class="cmzbtn label label-success pull-right"></a>
                                <a href="<?php echo url('admin/user/index',['group_id'=>$v['group_id']]); ?>" style="padding:5px 10px;margin-right:5px;" class="cmzbtn label label-primary pull-right">成员:共(<?php echo $v['count']; ?>)人</a>
                                </p>
                            </li>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                                
                            </ul>
                          

                            
                            <div class="clearfix"></div>
                               
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 animated fadeInRight">
                <div class="mail-box-header">

                  
                    <h2>
                    成员列表
                </h2>
                   <div class="mail-tools tooltip-demo m-t-md">
                        <div class="btn-group pull-right">
                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-left"></i>
                            </button>
                            <button class="btn btn-white btn-sm"><i class="fa fa-arrow-right"></i>
                            </button>

                        </div>
                        <?php if($group_id>0){ ?>
                        <a href="javascript:;" url="/" name="新建成员" class="frAlert btn btn-white btn-sm" data-toggle="tooltip" data-placement="left"><i class="fa fa-sticky-note"></i> 新建用户</a>
                        <a href="javascript:;" url="/" name="新建成员" class="frAlert btn btn-white btn-sm" data-toggle="tooltip" data-placement="left"><i class="fa fa-sticky-note"></i> 组授权</a>
                        <?php } ?>


                    </div>
                </div>
                 <div class="ibox-content">
                        <?php if($users): ?>
                        <table class="table table-striped table-bordered table-hover dataTables-example">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>图片</th>
                                    <th>姓名</th>
                                    <th>邮箱</th>
                                    <th>会员组</th>
                                     <!-- <th>系统组</th> -->
                                     <th>注册时间</th>
                                     <th>最后活跃</th>
                                     <th>在线时长</th>
                                     <th>威望</th>
                                     <th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                   
                               <?php if(is_array($users) || $users instanceof \think\Collection): $i = 0; $__LIST__ = $users;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                    <tr>
                                    <td><?php echo $v['uid']; ?></td>
                                        <td class="client-avatar"><img src="/uploads/avatar/<?php echo $v['avatar_file']; ?>" alt="<?php echo $v['user_name']; ?>"> </td>
                                        <td><a class="client-link" href="#contact-1" data-toggle="tab"><?php echo $v['user_name']; ?></a>
                                        </td>
                                        <td> <?php echo $v['email']; ?></td>
                                        <td class="contact-type"> </i>
                                        </td>
                                        <!-- <td><span class="label label-primary"></span></td> -->
                                        <td class="client-status"><span class="label label-default label-outline"><?php echo date('Y-m-d',$v['reg_time']); ?></span></td>
                                        <td class="client-status"><span class="label label-default label-outline"><?php echo date('Y-m-d',$v['last_login']); ?></span></td>
                                        <td class="client-status"><span class="label label-default label-outline"><?php echo date('Y-m-d',$v['online_tim']); ?></span></td>
                                        <td class="client-status"><span class="label label-default label-outline"><?php echo $v['reputation']; ?></span></td>
                                         <td class="client-status">   
                                         <a url="/admin/member/edit/userId/<?php echo $v['userId']; ?>/ucat/<?php echo $v['ucat']; ?>" class="btn btn-primary btn-xs frAlert" >修改</a>
                                        <a class="btn btn-danger btn-xs cmzdel" where="userId-<?php echo $v['userId']; ?>" table="users" >删除</a>
                                       <!-- <td class="client-status"><span class="label label-primary">删除</span> -->
                                        </td>
                                    </tr>
                               
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                         
                            </tbody>
                        
                        </table>
                        <?php else: if($group_id>0): ?>
                                 <h5 > <div class="alet alert-info" style="padding:20px;">此分组下面没有用户</div></h5>
                            <?php else: ?>
                                <h5 > <div class="alet alert-danger" style="padding:20px;">请先选择分组</div></h5>
                            <?php endif; endif; ?>

                    </div>
             
            </div>
        </div>
    </div>

    <script src="/static/plus/js/bootstrap.min.js?v=3.3.5"></script>
    <script src="/static/plus/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    
    <script src="/static/plus/js/hplus.min.js?v=4.0.0"></script>
    <script type="text/javascript" src="/static/plus/js/contabs.min.js"></script>
    <script src="/static/plus/js/plugins/pace/pace.min.js"></script>
    <script src="/static/CMZPLUS/hdjs.js" type="text/javascript"></script>  
    
 



    <script src="/static/plus/js/plugins/chosen/chosen.jquery.js"></script>
    <script src="/static/plus/js/plugins/jsKnob/jquery.knob.js"></script>
    <script src="/static/plus/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="/static/plus/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="/static/plus/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
 
    <script src="/static/plus/js/plugins/nouslider/jquery.nouislider.min.js"></script>
 
    <script src="/static/plus/js/plugins/switchery/switchery.js"></script>
    <script src="/static/plus/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>
    <script src="/static/plus/js/plugins/iCheck/icheck.min.js"></script>
    <script src="/static/plus/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/static/plus/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="/static/plus/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="/static/plus/js/plugins/cropper/cropper.min.js"></script>

<script src="/static/plus/js/demo/form-advanced-demo.min.js"></script>

<script type="text/javascript">
    //
    $(".i-checks").iCheck({
        checkboxClass: "icheckbox_square-green",
        radioClass: "iradio_square-green"
    })
    //时间
    $(".input-group.date").datepicker({
        todayBtn: "linked",
        keyboardNavigation: !1,
        forceParse: !1,
        calendarWeeks: !0,
        autoclose: !0,
        format: "yyyy.mm.dd"

    })
</script>
</body>

</html>