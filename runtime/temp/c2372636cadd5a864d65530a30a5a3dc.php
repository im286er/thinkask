<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:42:"../template/5ihelp/index\article_list.html";i:1478276779;s:37:"../template/5ihelp/public\header.html";i:1478527637;s:37:"../template/5ihelp/public\footer.html";i:1478269569;}*/ ?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta name="renderer" content="webkit" />
<title>5ihelp</title>
<meta name="keywords" content="" />
<meta name="description" content=""  />
<!-- <base href="http://git.cooldreamer.com/ask/?/" /> -->
<!-- <link href="http://git.cooldreamer.com/ask/static/css/default/img/favicon.ico?v=20160523" rel="shortcut icon" type="image/x-icon" /> -->

<link rel="stylesheet" type="text/css" href="/static/5iask/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/static/5iask/css/icon.css" />
<link href="/static/5iask/css/default/common.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/css/default/link.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/js/plug_module/style.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<script src="/static/5iask/js/jquery.2.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script src="/static/5iask/js/jquery.form.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script type="text/javascript" src="/static/plus/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>

 <script src="/static/plus/js/plugins/layer/layer.js"></script>
   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    // layer.config(
    //     extend: ['skin/moon/style.css'], //加载新皮肤
    //     skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    // );

    </script>
<!--[if lte IE 8]>
	<script type="text/javascript" src="/static/5iask/js/respond.js"></script>
<![endif]-->
</head>

<body>
	<div class="aw-top-menu-wrap">
		<div class="container">
			<!-- logo -->

			<div class="aw-logo hidden-xs">
				<a href="/"><img style="position: relative;top:-2px;left:10px;" src="/static/images/logo/thinkask_logo_black.png" height="45" alt=""></a>
			</div>
			<!-- end logo -->
			<!-- 搜索框 -->
			<div class="aw-search-box  hidden-xs hidden-sm">
				<form class="navbar-search" action="search/" id="global_search_form" method="post">
					<input class="form-control search-query" type="text" placeholder="搜索问题、话题或人" autocomplete="off" name="q" id="aw-search-query" />
					<span title="搜索" id="global_search_btns" ><i class="icon icon-search"></i></span>
					<div class="aw-dropdown">
						<div class="mod-body">
							<p class="title">输入关键字进行搜索</p>
							<ul class="aw-dropdown-list collapse"></ul>
							<p class="search"><span>搜索:</span><a ></a></p>
						</div>
						<div class="mod-footer" style="display:none;">
							<a href=""  class="pull-right btn btn-mini btn-success publish">发起问题</a>
						</div>
					</div>
				</form>
			</div>
			<!-- end 搜索框 -->
			<!-- 导航 -->
			<div class="aw-top-nav navbar">
				<div class="navbar-header">
				  <button  class="navbar-toggle pull-left">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
				  <ul class="nav navbar-nav">
				  <li><a href="<?php echo url('index/index/index'); ?>" class="<?php if($doaction=="home"){ echo "active";} ?>" ><i class="icon icon-index"></i> 发现</a></li>

					<?php  if($uid>0){ ?>
						<!-- <li><a href="home/"><i class="icon icon-home"></i> 动态</a></li> -->
					<?php }  ?>
					<!-- <li><a href="" class=""><i class="icon icon-list"></i> 发现</a></li> -->
					<li><a href="<?php echo url('index/question/index'); ?>"  class="<?php if($doaction=="question"){ echo "active";} ?>">问题</a></li>
					<li><a href="<?php echo url('index/article/index'); ?>" class="<?php if($doaction=="article"){ echo "active";} ?>">文章</a></li>
					<!-- <li><a href="topic/"><i class="icon icon-topic"></i>话题</a></li> -->
					<?php  if ($uid>0){  ?>
					<li style="display:none;">
						<a href="notifications/" class=""><i class="icon icon-bell"></i> 通知</a>
						<span class="badge badge-important" style="display:none" id="notifications_unread"><?php // // echo $this->user_info['notification_unread']; ?></span>
						<div class="aw-dropdown pull-right hidden-xs">
							<div class="mod-body">
								<ul id="header_notification_list"></ul>
							</div>
							<div class="mod-footer">
								<a href="notifications/">查看全部</a>
							</div>
						</div>
					</li>
					<?php }   ?>
					<!-- <li><a href="help/"> <i class="icon icon-bulb"></i> 帮助</a></li> -->
					<li>
						<a style="font-weight:bold;">· · ·</a>
						<!-- <div class="dropdown-list pull-right">
							<ul id="extensions-nav-list">
								<li><a href="ticket/"><i class="icon icon-order"></i> 工单</a></li>
								<li><a href="project/"><i class="icon icon-activity"></i> 活动</a></li>
							</ul>
						</div> -->
					</li>
				  </ul>
				</nav>
			</div>
			<!-- end 导航 -->
			<!-- 用户栏 -->
			<div class="aw-user-nav">
				<!-- 登陆&注册栏 -->
				<?php  if($uid>0){ ?>
					<a href="people/" style="" class="aw-user-nav-dropdown">
						<img alt="<?php echo $userinfo['user_name']; ?>" src="/uploads/avatar/<?php echo $userinfo['avatar_file']; ?>" />
							<span class="badge badge-important"></span>
					</a>
					<div class="aw-dropdown dropdown-list pull-right" style="">
						<ul class="aw-dropdown-list">
							<li><a href="inbox/"><i class="icon icon-inbox"></i> 私信<span class="badge badge-important collapse" id="inbox_unread">0</span></a></li>
							<li class="hidden-xs"><a href="account/setting/profile/"><i class="icon icon-setting"></i> 设置</a></li>
							<li class="hidden-xs"><a href="<?php echo url('admin/index/index'); ?>"><i class="icon icon-job"></i> 管理</a></li>
							<li><a href="account/logout/"><i class="icon icon-logout"></i></a></li>
						</ul>
					</div>
				<?php  }else{ ?>
					<a class="login btn btn-normal btn-primary" href="<?php echo url('ucenter/user/login'); ?>">登录</a>
					<a class="register btn btn-normal btn-success" href="<?php echo url('ucenter/user/reg'); ?>">注册</a>
				<?php }  ?>	
				<!-- end 登陆&注册栏 -->
			</div>
			<!-- end 用户栏 -->
			<!-- 发起 -->
			<?php if($uid>0){ ?>
			<div class="aw-publish-btn" style="">
				<a id="header_publish" class="btn-primary"href="publish/"><i class="icon icon-ask"></i>发起</a>
				<div class="dropdown-list pull-right">
					<ul>
						<li>
						<a href="<?php echo url('post/publish/question'); ?>">问题</a>
					
						</li>
						<li>
							
						<a href="<?php echo url('post/publish/article'); ?>">文章</a>
						
						</li>
				<!-- <li><a href="ticket/publish/">工单</a></li>
						<li><a href="project/publish/">活动</a></li> -->
				
					</ul>
				</div>
			</div>
			<?php } ?>
			<!-- end 发起 -->
		</div>
	</div>

	<div class="aw-email-verify" style="display:none;">
		<div class="container text-center">
			<a onclick="AWS.ajax_request(G_BASE_URL + '/account/ajax/send_valid_mail/');"></a>
		</div>
	</div>


<div class="aw-container-wrap">
	<div class="container">
		<div class="row">
			<div class="aw-content-wrap clearfix">
				<div class="col-sm-12 col-md-9 aw-main-content">
					<!-- 新消息通知 -->
					<div class="aw-mod aw-notification-box collapse" id="index_notification">
						<div class="mod-head common-head">
							<h2>
								<span class="pull-right"><a href="account/setting/privacy/#notifications" class="text-color-999"><i class="icon icon-setting"></i>通知设置</a></span>
								<i class="icon icon-bell"></i>新通知<em class="badge badge-important" name="notification_unread_num"><?php // // echo $this->user_info['notification_unread']; ?></em>
							</h2>
						</div>
						<div class="mod-body">
							<ul id="notification_list"></ul>
						</div>
						<div class="mod-footer clearfix">
							<a href="javascript:;" onclick="AWS.Message.read_notification(false, 0, false);" class="pull-left btn btn-mini btn-gray">我知道了</a>
							<a href="notifications/" class="pull-right btn btn-mini btn-success">查看所有</a>
						</div>
					</div>
					<!-- end 新消息通知 -->
					<!-- tab切换 -->
					<ul class="nav nav-tabs aw-nav-tabs active hidden-xs">
						<li class=" <?php if($status=="hot"){ echo "active";} ?>">
							<a href="<?php echo url('index/article/index'); ?>?status=hot" id="sort_control_hot" >热门</a>
						</li>
						<li class=" <?php if($status=="recommend"){ echo "active";} ?>">
							<a href="<?php echo url('index/article/index'); ?>?status=recommend" >推荐</a></li>
						<li class=" <?php if($status!="hot"&&$status!="recommend"){ echo "active";} ?>">
							<a href="<?php echo url('index/article/index'); ?>"  >最新</a></li>

						<h2 class="hidden-xs"><i class="icon icon-list"></i> 文章</h2>
					</ul>
					<!-- end tab切换 -->

				
					<!-- 自定义tab切换 -->
					<!-- <div class="aw-tabs"> -->
						<!-- <ul> -->
							<!-- <li class="active"><a href="" day="30">30天</a></li>
						  	<liclass="active"><a href="" day="7">7天</a></li>
						  	<li ><a href="feature_id-" day="1">当天</a></li> -->
						<!-- </ul> -->
					<!-- </div> -->
					<!-- end 自定义tab切换 -->
				

					<div class="aw-mod aw-explore-list">
						<div class="mod-body">
							<div class="aw-common-list">
							<?php if(is_array($list) || $list instanceof \think\Collection): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
								  <div class="aw-item article" data-topic-id="">
								    <a class="aw-user-name hidden-xs" data-id="1" href="http://git.cooldreamer.com/ask/?/people/admin" rel="nofollow">
								      <img src="/uploads/avatar/<?php echo $v['avatar_file']; ?>" alt=""></a>
								    <div class="aw-question-content">
								      <h4>
								        <a href="<?php echo url('index/article/index'); ?>?id=<?php echo $v['id']; ?>"><?php echo $v['title']; ?></a></h4>
								      <p>
								        <a class="aw-question-tags" href=""><?php  $topic = unserialize($v['recent_topics']); echo $topic[0]; ?></a>•
								        <a href="" class="aw-user-name"><?php echo $v['user_name']; ?></a>
								        <span class="text-color-999">发表了文章 • <?php echo $v['comments']; ?> 个评论 • <?php echo $v['views']; ?> 次浏览 • <?php echo date_friendly($v['add_time']); ?></span>
								        <span class="text-color-999 related-topic collapse">• 来自相关话题</span></p>
								      <!-- 文章内容调用 -->
								      <div class="markitup-box">
								      <!--   <img src="http://cooldreamer.org/ask/uploads/article/20161103/cebb5c63d0cf8132fe5699bbcf3ea482.jpg" class="img-polaroid pull-left inline-img" title="5c804cb969fc62fd652c148bbea71204.jpg" alt="5c804cb969fc62fd652c148bbea71204.jpg"> -->
								        <div class="img pull-right">
								        	
								        </div><?php echo msubstr($v['message']); ?></div>
								     <!--  <div class="collapse all-content">
								        <br>
								        <div class="aw-upload-img-list active">
								          <a href="http://cooldreamer.org/ask/uploads/article/20161103/cebb5c63d0cf8132fe5699bbcf3ea482.jpg" target="_blank" data-fancybox-group="thumb" rel="lightbox"></a>
								        </div>
								        <br>木木材af木木材af木木材af</div> -->
								      <!-- end 文章内容调用 --></div>
								  </div>
						<?php endforeach; endif; else: echo "" ;endif; ?>
							</div>
						</div>
						<div class="mod-footer">
							<div class="mod-footer">
							<div class="page-control">
							<ul class="pagination pull-right">
							<li class="active"><a href="javascript:;">1</a></li>
							<li><a href="http://git.cooldreamer.com/ask/?/sort_type-new__day-0__is_recommend-0__page-2">2</a></li>
							<li><a href="http://git.cooldreamer.com/ask/?/sort_type-new__day-0__is_recommend-0__page-2">&gt;</a></li>
							$news.page
							</ul>
							</div>						
							</div>
						</div>
					</div>
				</div>

				<!-- 侧边栏 -->
				<div class="col-sm-12 col-md-3 aw-side-bar hidden-xs hidden-sm">
					<?php // // TPL::output('block/sidebar_feature.tpl.htm'); // // TPL::output('block/sidebar_hot_topics.tpl.htm'); // // TPL::output('block/sidebar_hot_users.tpl.htm'); ?>
				</div>
				<!-- end 侧边栏 -->
			</div>
		</div>
	</div>
</div>
<?php die; ?>
<div class="aw-footer-wrap">
	<div class="aw-footer">
		Copyright © <?php // echo date('Y'); // if(get_setting('icp_beian')){ ?><span class="hidden-xs"> - <?php // echo get_setting('icp_beian'); // } ?>, All Rights Reserved</span>

		<span class="hidden-xs">Powered By <a href="http://www.5ihelp.com" target="blank">5ihelp </a>感谢Wecenter提供站点框架支持</span>

		<?php // if (is_mobile(true)) { ?>
			<div class="container">
				<div class="row">
					<p align="center"><?php // _e('版本切换'); ?>: <b><?php // _e('PC 版'); ?></b> | <a href="m/ignore_ua_check-FALSE"><?php // _e('手机版'); ?></a></p>
				</div>
			</div>
		<?php // } ?>
	</div>
</div>

<a class="aw-back-top hidden-xs" href="javascript:;" onclick="$.scrollTo(1, 600, {queue:true});"><i class="icon icon-up"></i></a>



<!-- DO NOT REMOVE -->
<div id="aw-ajax-box" class="aw-ajax-box"></div>

</body>
</html>
