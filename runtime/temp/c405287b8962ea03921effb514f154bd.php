<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:36:"../template/5ihelp/ucenter\logo.html";i:1478566141;s:37:"../template/5ihelp/public\header.html";i:1478566144;s:37:"../template/5ihelp/public\footer.html";i:1478566144;}*/ ?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta name="renderer" content="webkit" />
<title>5ihelp</title>
<meta name="keywords" content="" />
<meta name="description" content=""  />
<!-- <base href="http://git.cooldreamer.com/ask/?/" /> -->
<!-- <link href="http://git.cooldreamer.com/ask/static/css/default/img/favicon.ico?v=20160523" rel="shortcut icon" type="image/x-icon" /> -->

<link rel="stylesheet" type="text/css" href="/static/5iask/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/static/5iask/css/icon.css" />
<link href="/static/5iask/css/default/common.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/css/default/link.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/js/plug_module/style.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<script src="/static/5iask/js/jquery.2.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script src="/static/5iask/js/jquery.form.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script type="text/javascript" src="/static/plus/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>

 <script src="/static/plus/js/plugins/layer/layer.js"></script>
   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    // layer.config(
    //     extend: ['skin/moon/style.css'], //加载新皮肤
    //     skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    // );

    </script>
<!--[if lte IE 8]>
	<script type="text/javascript" src="/static/5iask/js/respond.js"></script>
<![endif]-->
</head>

<body>
	<div class="aw-top-menu-wrap">
		<div class="container">
			<!-- logo -->

			<div class="aw-logo hidden-xs">
				<a href="/"><img style="position: relative;top:-2px;left:10px;" src="/static/images/logo/thinkask_logo_black.png" height="45" alt=""></a>
			</div>
			<!-- end logo -->
			<!-- 搜索框 -->
			<div class="aw-search-box  hidden-xs hidden-sm">
				<form class="navbar-search" action="search/" id="global_search_form" method="post">
					<input class="form-control search-query" type="text" placeholder="搜索问题、话题或人" autocomplete="off" name="q" id="aw-search-query" />
					<span title="搜索" id="global_search_btns" ><i class="icon icon-search"></i></span>
					<div class="aw-dropdown">
						<div class="mod-body">
							<p class="title">输入关键字进行搜索</p>
							<ul class="aw-dropdown-list collapse"></ul>
							<p class="search"><span>搜索:</span><a ></a></p>
						</div>
						<div class="mod-footer" style="display:none;">
							<a href=""  class="pull-right btn btn-mini btn-success publish">发起问题</a>
						</div>
					</div>
				</form>
			</div>
			<!-- end 搜索框 -->
			<!-- 导航 -->
			<div class="aw-top-nav navbar">
				<div class="navbar-header">
				  <button  class="navbar-toggle pull-left">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
				  <ul class="nav navbar-nav">
				  <li><a href="<?php echo url('index/index/index'); ?>" class="<?php if($doaction=="home"){ echo "active";} ?>" ><i class="icon icon-index"></i> 发现</a></li>

					<?php  if($uid>0){ ?>
						<!-- <li><a href="home/"><i class="icon icon-home"></i> 动态</a></li> -->
					<?php }  ?>
					<!-- <li><a href="" class=""><i class="icon icon-list"></i> 发现</a></li> -->
					<li><a href="<?php echo url('index/question/index'); ?>"  class="<?php if($doaction=="question"){ echo "active";} ?>">问题</a></li>
					<li><a href="<?php echo url('index/article/index'); ?>" class="<?php if($doaction=="article"){ echo "active";} ?>">文章</a></li>
					<!-- <li><a href="topic/"><i class="icon icon-topic"></i>话题</a></li> -->
					<?php  if ($uid>0){  ?>
					<li style="display:none;">
						<a href="notifications/" class=""><i class="icon icon-bell"></i> 通知</a>
						<span class="badge badge-important" style="display:none" id="notifications_unread"><?php // // echo $this->user_info['notification_unread']; ?></span>
						<div class="aw-dropdown pull-right hidden-xs">
							<div class="mod-body">
								<ul id="header_notification_list"></ul>
							</div>
							<div class="mod-footer">
								<a href="notifications/">查看全部</a>
							</div>
						</div>
					</li>
					<?php }   ?>
					<!-- <li><a href="help/"> <i class="icon icon-bulb"></i> 帮助</a></li> -->
					<li>
						<a style="font-weight:bold;">· · ·</a>
						<!-- <div class="dropdown-list pull-right">
							<ul id="extensions-nav-list">
								<li><a href="ticket/"><i class="icon icon-order"></i> 工单</a></li>
								<li><a href="project/"><i class="icon icon-activity"></i> 活动</a></li>
							</ul>
						</div> -->
					</li>
				  </ul>
				</nav>
			</div>
			<!-- end 导航 -->
			<!-- 用户栏 -->
			<div class="aw-user-nav">
				<!-- 登陆&注册栏 -->
				<?php  if($uid>0){ ?>
					<a href="people/" style="" class="aw-user-nav-dropdown">
						<img alt="<?php echo $userinfo['user_name']; ?>" src="/uploads/avatar/<?php echo $userinfo['avatar_file']; ?>" />
							<span class="badge badge-important"></span>
					</a>
					<div class="aw-dropdown dropdown-list pull-right" style="">
						<ul class="aw-dropdown-list">
							<li><a href="inbox/"><i class="icon icon-inbox"></i> 私信<span class="badge badge-important collapse" id="inbox_unread">0</span></a></li>
							<li class="hidden-xs"><a href="account/setting/profile/"><i class="icon icon-setting"></i> 设置</a></li>
							<li class="hidden-xs"><a href="<?php echo url('admin/index/index'); ?>"><i class="icon icon-job"></i> 管理</a></li>
							<li><a href="account/logout/"><i class="icon icon-logout"></i></a></li>
						</ul>
					</div>
				<?php  }else{ ?>
					<a class="login btn btn-normal btn-primary" href="<?php echo url('ucenter/user/login'); ?>">登录</a>
					<a class="register btn btn-normal btn-success" href="<?php echo url('ucenter/user/reg'); ?>">注册</a>
				<?php }  ?>	
				<!-- end 登陆&注册栏 -->
			</div>
			<!-- end 用户栏 -->
			<!-- 发起 -->
			<?php if($uid>0){ ?>
			<div class="aw-publish-btn" style="">
				<a id="header_publish" class="btn-primary"href="publish/"><i class="icon icon-ask"></i>发起</a>
				<div class="dropdown-list pull-right">
					<ul>
						<li>
						<a href="<?php echo url('post/publish/question'); ?>">问题</a>
					
						</li>
						<li>
							
						<a href="<?php echo url('post/publish/article'); ?>">文章</a>
						
						</li>
				<!-- <li><a href="ticket/publish/">工单</a></li>
						<li><a href="project/publish/">活动</a></li> -->
				
					</ul>
				</div>
			</div>
			<?php } ?>
			<!-- end 发起 -->
		</div>
	</div>

	<div class="aw-email-verify" style="display:none;">
		<div class="container text-center">
			<a onclick="AWS.ajax_request(G_BASE_URL + '/account/ajax/send_valid_mail/');"></a>
		</div>
	</div>

<link href="/static/5iask/js/plug_module/style.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/css/default/login.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<div id="wrapper">
  <div class="aw-login-box">
    <div class="mod-body clearfix" style="">
      <div class="content pull-left">
        <!-- <h1 class="logo"><a href=""></a></h1> -->
        <div style="background:#303036">
          <p style="width:120px;margin:0 auto;"><img style="height:70px;" src="/static/images/logo/login_logo.png" alt="" /></p>
          
        </div>
        <h2></h2>
        <form id="login_form" method="post" class="cmzForm"  action="<?php echo url('ajax/user/logo'); ?>">
          <ul>
            <li>
              <input type="text" id="aw-login-user-name" class="form-control" placeholder="邮箱 / 用户名" name="user_name" />
            </li>
            <li>
              <input type="password" id="aw-login-user-password" class="form-control" placeholder="密码" name="password" />
            </li>
            <li class="alert alert-danger collapse error_message">
              <i class="icon icon-delete"></i> <em></em>
            </li>
            <li class="last">
              <a href="javascript:;" id="login_submit" class="pull-right btn btn-large btn-primary cmzPost" returnurl="">登录</a>
              <label>
                <input type="checkbox" value="1" name="net_auto_login" />
               记住我
              </label>
              <a href="account/find_password/">&nbsp;&nbsp;忘记密码</a>
            </li>
          </ul>
        </form>
      </div>
      <div class="side-bar pull-left">
          <h3>第三方账号登录</h3>
            <a href="account/openid/weibo/bind/<?php // echo $return_url; ?>" class="btn btn-block btn-weibo"><i class="icon icon-weibo"></i> 微博登录</a>
            <a href="account/openid/qq/bind/<?php // echo $return_url; ?>" class="btn btn-block btn-qq"><i class="icon icon-qq"></i> QQ 登录</a>
            <!-- <a href="" class="btn btn-block btn-wechat">
              <i class="icon icon-wechat"></i> 微信扫一扫登录
              <div class="img">
                <img src="" />
              </div>
            </a>
            <a href="" class="btn btn-block btn-google"> <i class="icon icon-google"></i> Google 登录</a>
            <a href="" class="btn btn-block btn-facebook"> <i class="icon icon-facebook"></i> Facebook 登录</a>
            <a href="" class="btn btn-block btn-twitter"> <i class="icon icon-twitter"></i> Twitter 登录</a>
         -->
     
      </div>
    </div>
    <div class="mod-footer">
      <span>还没有账号?</span>&nbsp;&nbsp;
      <a href="account/register/">立即注册</a>&nbsp;&nbsp;•&nbsp;&nbsp;
      <a href="">游客访问</a>&nbsp;&nbsp;•&nbsp;&nbsp;
      <!-- <a href="reader/"><问答阅读</a> -->
    </div>
  </div>
</div>

<canvas id="canvas" style="z-index:-5;position:absolute;left:0px; top:0px;" width="1920" height="919"></canvas>
<?php die; ?>
<div class="aw-footer-wrap">
	<div class="aw-footer">
		Copyright © <?php // echo date('Y'); // if(get_setting('icp_beian')){ ?><span class="hidden-xs"> - <?php // echo get_setting('icp_beian'); // } ?>, All Rights Reserved</span>

		<span class="hidden-xs">Powered By <a href="http://www.5ihelp.com" target="blank">5ihelp </a>感谢Wecenter提供站点框架支持</span>

		<?php // if (is_mobile(true)) { ?>
			<div class="container">
				<div class="row">
					<p align="center"><?php // _e('版本切换'); ?>: <b><?php // _e('PC 版'); ?></b> | <a href="m/ignore_ua_check-FALSE"><?php // _e('手机版'); ?></a></p>
				</div>
			</div>
		<?php // } ?>
	</div>
</div>

<a class="aw-back-top hidden-xs" href="javascript:;" onclick="$.scrollTo(1, 600, {queue:true});"><i class="icon icon-up"></i></a>



<!-- DO NOT REMOVE -->
<div id="aw-ajax-box" class="aw-ajax-box"></div>

</body>
</html>
