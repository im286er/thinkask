<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:40:"../template/5ihelp/publish\question.html";i:1478566141;s:37:"../template/5ihelp/public\header.html";i:1478566144;s:37:"../template/5ihelp/public\footer.html";i:1478566144;}*/ ?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta name="renderer" content="webkit" />
<title>5ihelp</title>
<meta name="keywords" content="" />
<meta name="description" content=""  />
<!-- <base href="http://git.cooldreamer.com/ask/?/" /> -->
<!-- <link href="http://git.cooldreamer.com/ask/static/css/default/img/favicon.ico?v=20160523" rel="shortcut icon" type="image/x-icon" /> -->

<link rel="stylesheet" type="text/css" href="/static/5iask/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/static/5iask/css/icon.css" />
<link href="/static/5iask/css/default/common.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/css/default/link.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/js/plug_module/style.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<script src="/static/5iask/js/jquery.2.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script src="/static/5iask/js/jquery.form.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script type="text/javascript" src="/static/plus/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>

 <script src="/static/plus/js/plugins/layer/layer.js"></script>
   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    // layer.config(
    //     extend: ['skin/moon/style.css'], //加载新皮肤
    //     skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    // );

    </script>
<!--[if lte IE 8]>
	<script type="text/javascript" src="/static/5iask/js/respond.js"></script>
<![endif]-->
</head>

<body>
	<div class="aw-top-menu-wrap">
		<div class="container">
			<!-- logo -->

			<div class="aw-logo hidden-xs">
				<a href="/"><img style="position: relative;top:-2px;left:10px;" src="/static/images/logo/thinkask_logo_black.png" height="45" alt=""></a>
			</div>
			<!-- end logo -->
			<!-- 搜索框 -->
			<div class="aw-search-box  hidden-xs hidden-sm">
				<form class="navbar-search" action="search/" id="global_search_form" method="post">
					<input class="form-control search-query" type="text" placeholder="搜索问题、话题或人" autocomplete="off" name="q" id="aw-search-query" />
					<span title="搜索" id="global_search_btns" ><i class="icon icon-search"></i></span>
					<div class="aw-dropdown">
						<div class="mod-body">
							<p class="title">输入关键字进行搜索</p>
							<ul class="aw-dropdown-list collapse"></ul>
							<p class="search"><span>搜索:</span><a ></a></p>
						</div>
						<div class="mod-footer" style="display:none;">
							<a href=""  class="pull-right btn btn-mini btn-success publish">发起问题</a>
						</div>
					</div>
				</form>
			</div>
			<!-- end 搜索框 -->
			<!-- 导航 -->
			<div class="aw-top-nav navbar">
				<div class="navbar-header">
				  <button  class="navbar-toggle pull-left">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
				  <ul class="nav navbar-nav">
				  <li><a href="<?php echo url('index/index/index'); ?>" class="<?php if($doaction=="home"){ echo "active";} ?>" ><i class="icon icon-index"></i> 发现</a></li>

					<?php  if($uid>0){ ?>
						<!-- <li><a href="home/"><i class="icon icon-home"></i> 动态</a></li> -->
					<?php }  ?>
					<!-- <li><a href="" class=""><i class="icon icon-list"></i> 发现</a></li> -->
					<li><a href="<?php echo url('index/question/index'); ?>"  class="<?php if($doaction=="question"){ echo "active";} ?>">问题</a></li>
					<li><a href="<?php echo url('index/article/index'); ?>" class="<?php if($doaction=="article"){ echo "active";} ?>">文章</a></li>
					<!-- <li><a href="topic/"><i class="icon icon-topic"></i>话题</a></li> -->
					<?php  if ($uid>0){  ?>
					<li style="display:none;">
						<a href="notifications/" class=""><i class="icon icon-bell"></i> 通知</a>
						<span class="badge badge-important" style="display:none" id="notifications_unread"><?php // // echo $this->user_info['notification_unread']; ?></span>
						<div class="aw-dropdown pull-right hidden-xs">
							<div class="mod-body">
								<ul id="header_notification_list"></ul>
							</div>
							<div class="mod-footer">
								<a href="notifications/">查看全部</a>
							</div>
						</div>
					</li>
					<?php }   ?>
					<!-- <li><a href="help/"> <i class="icon icon-bulb"></i> 帮助</a></li> -->
					<li>
						<a style="font-weight:bold;">· · ·</a>
						<!-- <div class="dropdown-list pull-right">
							<ul id="extensions-nav-list">
								<li><a href="ticket/"><i class="icon icon-order"></i> 工单</a></li>
								<li><a href="project/"><i class="icon icon-activity"></i> 活动</a></li>
							</ul>
						</div> -->
					</li>
				  </ul>
				</nav>
			</div>
			<!-- end 导航 -->
			<!-- 用户栏 -->
			<div class="aw-user-nav">
				<!-- 登陆&注册栏 -->
				<?php  if($uid>0){ ?>
					<a href="people/" style="" class="aw-user-nav-dropdown">
						<img alt="<?php echo $userinfo['user_name']; ?>" src="/uploads/avatar/<?php echo $userinfo['avatar_file']; ?>" />
							<span class="badge badge-important"></span>
					</a>
					<div class="aw-dropdown dropdown-list pull-right" style="">
						<ul class="aw-dropdown-list">
							<li><a href="inbox/"><i class="icon icon-inbox"></i> 私信<span class="badge badge-important collapse" id="inbox_unread">0</span></a></li>
							<li class="hidden-xs"><a href="account/setting/profile/"><i class="icon icon-setting"></i> 设置</a></li>
							<li class="hidden-xs"><a href="<?php echo url('admin/index/index'); ?>"><i class="icon icon-job"></i> 管理</a></li>
							<li><a href="account/logout/"><i class="icon icon-logout"></i></a></li>
						</ul>
					</div>
				<?php  }else{ ?>
					<a class="login btn btn-normal btn-primary" href="<?php echo url('ucenter/user/login'); ?>">登录</a>
					<a class="register btn btn-normal btn-success" href="<?php echo url('ucenter/user/reg'); ?>">注册</a>
				<?php }  ?>	
				<!-- end 登陆&注册栏 -->
			</div>
			<!-- end 用户栏 -->
			<!-- 发起 -->
			<?php if($uid>0){ ?>
			<div class="aw-publish-btn" style="">
				<a id="header_publish" class="btn-primary"href="publish/"><i class="icon icon-ask"></i>发起</a>
				<div class="dropdown-list pull-right">
					<ul>
						<li>
						<a href="<?php echo url('post/publish/question'); ?>">问题</a>
					
						</li>
						<li>
							
						<a href="<?php echo url('post/publish/article'); ?>">文章</a>
						
						</li>
				<!-- <li><a href="ticket/publish/">工单</a></li>
						<li><a href="project/publish/">活动</a></li> -->
				
					</ul>
				</div>
			</div>
			<?php } ?>
			<!-- end 发起 -->
		</div>
	</div>

	<div class="aw-email-verify" style="display:none;">
		<div class="container text-center">
			<a onclick="AWS.ajax_request(G_BASE_URL + '/account/ajax/send_valid_mail/');"></a>
		</div>
	</div>


<div class="aw-container-wrap">
	<div class="container aw-publish">
		<div class="row">
			<div class="aw-content-wrap clearfix">
				<div class="col-sm-12 col-md-9 aw-main-content">
					<!-- tab 切换 -->
					<ul class="nav nav-tabs aw-nav-tabs active">
						
						<!-- <li><a href="project/publish/">活动</a></li>
						<li><a href="ticket/publish/">工单</a></li> -->
						<li><a href="<?php echo url('post/publish/article'); ?>">文章</a></li>
						<li class="active"><a href="<?php echo url('post/publish/question'); ?>">问题</a></li>
						<h2 class="hidden-xs"><i class="icon icon-ask"></i> 问题</h2>
					</ul>
					<!-- end tab 切换 -->
					<form action="<?php echo url('ajax/question/edit'); ?>" method="post" class="cmzForm" onsubmit="return false;">
						
						
						<div class="aw-mod aw-mod-publish">
							<div class="mod-body">
								<h3>问题标题:</h3>
								<!-- 问题标题 -->
								<div class="aw-publish-title">
									<input type="text" name="question_content" value="<?php echo $question_content; ?>" class="form-control" placeholder="问题标题..." />
								
									
								<div class="dropdown">
										<select name="category_id" id="" class="form-control">
												<option value="">选择分类</option>
												<?php foreach ($category as  $v): ?>
												<option <?php if($v['id']==$category_id): ?> selected="" <?php endif; ?>value="<?php echo $v['id']; ?>" value="<?php echo $v['id']; ?>"><?php echo $v['title']; ?></option>
											<?php endforeach ?>
											</select>
									
								
								</div>
								</div>

								
								<!-- end 问题标题 -->

								<h3><?php e('问题补充 (选填)'); ?>:</h3>
								<div class="aw-mod aw-editor-box">
									<div class="mod-head">
										<div class="wmd-panel">
										<script type="text/javascript" src="/static/plus/js/plugins/ueditor/ueditor.config.js"></script>
					                        <script type="text/javascript" src="/static/plus/js/plugins/ueditor/ueditor.all.js"></script>
					                        <script type="text/javascript" src="/static/plus/js/plugins/ueditor/ueditor.parse.js"></script>
					                        <textarea name="question_detail" class="question_detail-baiduedit"  style="display:none;"><?php echo $question_detail; ?></textarea>
					                         <script id="question_detail" type="text/plain" style="300"><?php echo $question_detail; ?></script>
					                        <script type="text/javascript">
					                            var ue = UE.getEditor("question_detail");
					                            ue.addListener( "selectionchange", function () {
					                                $(".question_detail-baiduedit").val(ue.getContent())
					                               
					                            } );
					                            
					                       </script>
								           
								        </div>
									</div>
									<!-- <div class="mod-body">
										<span class="pull-right text-color-999" id="question_detail_message">&nbsp;</span>
										
										<div class="aw-upload-box">
											<a class="btn btn-default">上传附件</a>
											<div class="upload-container"></div>
											<span class="text-color-999 aw-upload-tips hidden-xs"><?php e('允许'); ?> : </span>
										</div>
										
									</div> -->
								</div>
								<?php if (!$this->question_info['question_id']) { ?>
								<h3><?php e('添加话题'); ?>:</h3>
								<div class="aw-topic-bar" data-type="publish">
									<div class="tag-bar clearfix">
										<?php if ($_GET['topic_title']) { ?>
										<span class="topic-tag">
											<a class="text"><?php echo urldecode($_GET['topic_title']); ?></a>
											<a class="close" >
												<i class="icon icon-delete"></i>
											</a>
											<input type="hidden" value="<?php echo urldecode($_GET['topic_title']); ?>" name="topics[]" />
										</span>
										<?php } if ($_POST['topics']) { foreach ($_POST['topics'] AS $key => $val) { ?>
										<span class="topic-tag">
											<a class="text"><?php echo $val; ?></a>
											<input type="hidden" value="<?php echo $val; ?>" name="topics[]" />
										</span>
										<?php } } ?>

										<span class="aw-edit-topic icon-inverse"><i class="icon icon-edit"></i> <?php e('编辑话题'); ?></span>
										</span>
									</div>
								</div>
								<?php if ($this->recent_topics) { ?>
								<h3><?php e('最近话题'); ?>:</h3>
								<div class="aw-topic-bar">
									<div class="topic-bar clearfix">
										<?php foreach($this->recent_topics as $key => $val) { ?>
										<span class="topic-tag" >
											<a class="text">
												<?php echo $val; ?>
											</a>
										</span>
										<?php } ?>
									</div>
								</div>
								<?php } } if ($this->human_valid) { ?>
								<div class="aw-auth-img clearfix">
									<input class="form-control pull-right" type="text" name="seccode_verify" placeholder="验证码" />
									<em class="auth-img pull-right"><img src=""  id="captcha"  /></em>
								</div>
								<?php } ?>
							</div>
							<div class="mod-footer clearfix">
								<a href="integral/rule/" target="_blank">[<?php e('积分规则'); ?>]</a>
								<span class="aw-anonymity">
									<?php if ($this->question_info['question_id'] AND ($this->user_info['permission']['is_administortar'] OR $this->user_info['permission']['is_moderator'])) { ?>
									<label><input type="checkbox" class="pull-left" value="1" name="do_delete" />
										<?php e('删除问题'); ?></label>
									<?php } ?>
									
									<label><input type="checkbox" class="pull-left" value="1" name="anonymous" />
										<?php e('匿名'); ?></label>
									

								</span>
								<input type="hidden" value="<?php echo $question_id; ?>" name="id">
								<a class="btn btn-large btn-success btn-publish-submit cmzPost"  >确认发起</a>
							</div>
						</div>
					</form>
				</div>
				<!-- 侧边栏 -->
				<div class="col-sm-12 col-md-3 aw-side-bar hidden-xs">
					<!-- 问题发起指南 -->
					<div class="aw-mod publish-help">
						<div class="mod-head">
							<h3>问题发起指南</h3>
						</div>
						<div class="mod-body">
							<p><b>• 问题标题:</b> 请用准确的语言描述您发布的问题思想</p>
							<p><b>• 问题补充:</b> 详细补充您的问题内容, 并提供一些相关的素材以供参与者更多的了解您所要问题的主题思想</p>
							<p><b>• 选择话题:</b> 选择一个或者多个合适的话题, 让您发布的问题得到更多有相同兴趣的人参与. 所有人可以在您发布问题之后添加和编辑该问题所属的话题</p>
							
							<p><b>• 关于积分：</b>发起一个问题会消耗您 50s 个积分,每多一个回复你将获得50个积分的奖励 为了您的利益, 在发起问题的时候希望能够更好的描述您的问题以及多使用站内搜索功能</b></p>
							
						</div>
					</div>
					<!-- end 问题发起指南 -->
				</div>
				<!-- end 侧边栏 -->
			</div>
		</div>
	</div>
</div>


<?php die; ?>
<div class="aw-footer-wrap">
	<div class="aw-footer">
		Copyright © <?php // echo date('Y'); // if(get_setting('icp_beian')){ ?><span class="hidden-xs"> - <?php // echo get_setting('icp_beian'); // } ?>, All Rights Reserved</span>

		<span class="hidden-xs">Powered By <a href="http://www.5ihelp.com" target="blank">5ihelp </a>感谢Wecenter提供站点框架支持</span>

		<?php // if (is_mobile(true)) { ?>
			<div class="container">
				<div class="row">
					<p align="center"><?php // _e('版本切换'); ?>: <b><?php // _e('PC 版'); ?></b> | <a href="m/ignore_ua_check-FALSE"><?php // _e('手机版'); ?></a></p>
				</div>
			</div>
		<?php // } ?>
	</div>
</div>

<a class="aw-back-top hidden-xs" href="javascript:;" onclick="$.scrollTo(1, 600, {queue:true});"><i class="icon icon-up"></i></a>



<!-- DO NOT REMOVE -->
<div id="aw-ajax-box" class="aw-ajax-box"></div>

</body>
</html>
