<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:43:"../template/5ihelp/admin\question\list.html";i:1478576099;s:43:"../template/5ihelp/admin\public\header.html";i:1478566143;s:43:"../template/5ihelp/admin\public\footer.html";i:1478566143;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title><?php echo $title; ?></title>

    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="description" content="<?php echo $description; ?>">

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->


    <link href="/static/plus/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/plus/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/plus/css/animate.min.css" rel="stylesheet">
    <link href="/static/plus/css/style.min.css?v=4.0.0" rel="stylesheet">
 <link href="/static/plus/css/plugins/chosen/chosen.css" rel="stylesheet">
 <link href="/static/plus/css/plugins/iCheck/custom.css" rel="stylesheet">
 <script src="/static/plus/js/jquery.min.js?v=2.1.4"></script>
 <script src="/static/plus/js/plugins/layer/layer.js"></script>
 <script src="/static/cmz/hdjs.js" type="text/javascript"></script>  
 <script src="/static/cmz/jquery.form.js" type="text/javascript"></script>

<link href="/static/plus/css/plugins/jsTree/style.min.css" rel="stylesheet">


   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    layer.config({
        extend: ['skin/moon/style.css'], //加载新皮肤
        skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    });

    </script>
     <link href="/static/cmz/cmzcss/cmz.css" rel="stylesheet">
</head>
<style type="text/css">
  
  .aw-content-wrap{padding: 15px;}
</style>


<form role="form" method="post" action="<?php echo url('ajax/setting/'); ?>" class ="cmzForm">
<style type="text/css">
    .tab-pane{padding-top: 20px;padding: 20px;}
</style>
    <div class="row">
    <div class="col-sm-12">
    <div class="tabs-container">
            <div style="clear:both;height:20px"></div>
            <div class="tab-content">
                <div id="tab-1" class="tab-pane active">
                     <div class="ibox float-e-margins">
                            <div class="row">
                                <div class="col-sm-12 b-r">
                                   <div class="ibox-title">
                                        <a class="label label-primary pull-right tip" id="admin" msg="问题列表">问题列表<i class="fa fa-question"></i></a>
                                        <h5>文章</h5>
                                    </div>
                                    <div class="ibox-content">
                                     
<div class="aw-content-wrap">
    <div class="mod">
        <div class="mod-head">
            <h3>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#list" data-toggle="tab"><?php e('问题列表'); ?></a></li>
                    <li><a href="#search" data-toggle="tab"><?php e('搜索'); ?></a></li>
                </ul>
            </h3>
        </div>
        <div class="mod-body tab-content">
            <div class="tab-pane active" id="list">
                <?php if ($_GET['action'] == 'search') { ?>
                <div class="alert alert-info"><?php e('找到 %s 条符合条件的内容', intval($this->question_count)); ?></div>
                <?php } ?>

                <form id="batchs_form" action="admin/ajax/question_manage/" method="post">
                <input type="hidden" id="action" name="action" value="" />
                <div class="table-responsive">
                <?php if ($list) { ?>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th><input type="checkbox" class="check-all"></th>
                                <th><?php e('问题标题'); ?></th>
                                <th><?php e('回答'); ?></th>
                                <th><?php e('关注'); ?></th>
                                <th><?php e('浏览'); ?></th>
                                <th><?php e('作者'); ?></th>
                                <th><?php e('发布时间'); ?></th>
                                <th><?php e('最后更新'); ?></th>
                                <th><?php e('操作'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($list AS $key => $v) { ?>
                            <tr>
                                <td><input type="checkbox" name="question_ids[]" value="<?php echo $v['question_id']; ?>"></td>
                                <td><a href="<?php echo url('index/question/index'); ?>?id=<?php echo $v['question_id']; ?>" target="_blank"><?php echo $v['question_content']; ?></a></td>
                                <td><?php if ($v['answer_count']) { ?><?php echo $v['answer_count']; } else { ?>0<?php } ?></td>
                                <td><?php echo $v['focus_count']; ?></td>
                                <td><?php echo $v['view_count']; ?></td>
                                <td><a href="" target="_blank"><?php echo $v['user_name']; ?></a></td>
                                <td><?php echo date_friendly($v['add_time']); ?></td>
                                <td><?php echo date_friendly($v['update_time']); ?></td>
                                <td><a target="_blank" class="btn btn-primary btn-xs" href="<?php echo url('post/publish/question'); ?>?id=<?php echo $v['question_id']; ?>">修改</a></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                <?php } ?>
                </div>
                </form>
                <div class="mod-table-foot">
                    <?php echo $this->pagination; ?>

                    <a class="btn btn-danger" ><?php e('删除'); ?></a>
                </div>
            </div>

            <div class="tab-pane" id="search">
                <form method="post" action="<?php echo url('admin/question/list'); ?>" onsubmit="return false;" id="search_form" class="form-horizontal" role="form">

                    <input name="action" type="hidden" value="search" />

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label"><?php e('关键词'); ?>:</label>

                        <div class="col-sm-5 col-xs-8">
                            <input class="form-control" type="text" value="<?php echo rawurldecode($_GET['keyword']); ?>" name="keyword" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label"><?php e('分类'); ?>:</label>

                        <div class="col-sm-5 col-xs-8">
                            <select name="category_id" id="category_id" class="form-control">
                                <option value="0"></option>
                                <?php if(is_array($category) || $category instanceof \think\Collection): $i = 0; $__LIST__ = $category;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                <option value="<?php echo $v['id']; ?>"><?php echo $v['title']; ?></option>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">发起时间范围:</label>

                        <div class="col-sm-6 col-xs-9">
                            <div class="row">
                                <div class="col-xs-11  col-sm-5 mod-double">
                                    <input type="text" class="form-control mod-data" value="" name="start_date" />
                                    <i class="icon icon-date"></i>
                                </div>
                                <span class="mod-symbol col-xs-1 col-sm-1">
                                -
                                </span>
                                <div class="col-xs-11 col-sm-5">
                                    <input type="text" class="form-control mod-data" value="" name="end_date" />
                                    <i class="icon icon-date"></i>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">作者:</label>

                        <div class="col-sm-5 col-xs-8">
                            <input class="form-control" type="text" value="<?php echo $_GET['user_name']; ?>" name="user_name" />
                        </div>
                    </div>

                   <!--  <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">回复数:</label>

                        <div class="col-sm-6 col-xs-9">
                            <div class="row">
                                <div class="col-xs-11  col-sm-5 mod-double">
                                    <input type="text" class="form-control" name="answer_count_min" value="" />
                                </div>
                                <span class="mod-symbol col-xs-1 col-sm-1">
                                -
                                </span>
                                <div class="col-xs-11 col-sm-5">
                                    <input type="text" class="form-control" name="answer_count_max" value="" />
                                </div>
                            </div>
                        </div>
                    </div> -->

                   <!--  <div class="form-group">
                        <label class="col-sm-2 col-xs-3 control-label">是否有最佳回复:</label>

                        <div class="col-sm-5 col-xs-8">
                            <div class="checkbox mod-padding">
                                <label><input type="checkbox" value="1" name="best_answer"checked="checked"> <?php e('有最佳回复'); ?></label>
                            </div>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-5 col-xs-8">
                            <a type="button"  class="btn btn-primary search"><?php e('搜索'); ?></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.search').click(function(event) {
        keyword=$("input[name='keyword']").val();
        category_id = $("#category_id option:selected").val(); 
        user_name= $("input[name='user_name']").val();
        // answer_count_min= $("input[name='answer_count_min']").val();
        // answer_count_max= $("input[name='answer_count_max']").val();
        // best_answer= $("input[name='best_answer']").val();
        window.location.href="?keyword="+keyword+"&category_id="+category_id+"&user_name="+user_name
    });
</script>
                                    </div>
                                </div>
                                
                            </div>  
                            <div style="clear:both;height:5px"></div> 
                            <div class="hr-line-dashed"></div>
                             
                             
                             <div style="clear:both;height:5px"></div>    
                    </div>
                </div>
            </div>

        </div>
    </div>
        
           
</div>
<div style="clear:both;"></div>
 <div>
     
  
    </div>


           
   </form>


    <script src="/static/plus/js/bootstrap.min.js?v=3.3.5"></script>
    <script src="/static/plus/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    
    <script src="/static/plus/js/hplus.min.js?v=4.0.0"></script>
    <script type="text/javascript" src="/static/plus/js/contabs.min.js"></script>
    <script src="/static/plus/js/plugins/pace/pace.min.js"></script>
    <script src="/static/CMZPLUS/hdjs.js" type="text/javascript"></script>  
    
 



    <script src="/static/plus/js/plugins/chosen/chosen.jquery.js"></script>
    <script src="/static/plus/js/plugins/jsKnob/jquery.knob.js"></script>
    <script src="/static/plus/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="/static/plus/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="/static/plus/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
 
    <script src="/static/plus/js/plugins/nouslider/jquery.nouislider.min.js"></script>
 
    <script src="/static/plus/js/plugins/switchery/switchery.js"></script>
    <script src="/static/plus/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>
    <script src="/static/plus/js/plugins/iCheck/icheck.min.js"></script>
    <script src="/static/plus/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/static/plus/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="/static/plus/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="/static/plus/js/plugins/cropper/cropper.min.js"></script>

<script src="/static/plus/js/demo/form-advanced-demo.min.js"></script>

<script type="text/javascript">
    //
    $(".i-checks").iCheck({
        checkboxClass: "icheckbox_square-green",
        radioClass: "iradio_square-green"
    })
    //时间
    $(".input-group.date").datepicker({
        todayBtn: "linked",
        keyboardNavigation: !1,
        forceParse: !1,
        calendarWeeks: !0,
        autoclose: !0,
        format: "yyyy.mm.dd"

    })
</script>
</body>

</html>