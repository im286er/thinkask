<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:42:"../template/5ihelp/admin\index\tomain.html";i:1478566143;s:43:"../template/5ihelp/admin\public\header.html";i:1478566143;s:43:"../template/5ihelp/admin\public\footer.html";i:1478566143;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title><?php echo $title; ?></title>

    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="description" content="<?php echo $description; ?>">

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->


    <link href="/static/plus/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/plus/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/plus/css/animate.min.css" rel="stylesheet">
    <link href="/static/plus/css/style.min.css?v=4.0.0" rel="stylesheet">
 <link href="/static/plus/css/plugins/chosen/chosen.css" rel="stylesheet">
 <link href="/static/plus/css/plugins/iCheck/custom.css" rel="stylesheet">
 <script src="/static/plus/js/jquery.min.js?v=2.1.4"></script>
 <script src="/static/plus/js/plugins/layer/layer.js"></script>
 <script src="/static/cmz/hdjs.js" type="text/javascript"></script>  
 <script src="/static/cmz/jquery.form.js" type="text/javascript"></script>

<link href="/static/plus/css/plugins/jsTree/style.min.css" rel="stylesheet">


   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    layer.config({
        extend: ['skin/moon/style.css'], //加载新皮肤
        skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    });

    </script>
     <link href="/static/cmz/cmzcss/cmz.css" rel="stylesheet">
</head>
<style type="text/css">
  
  .aw-content-wrap{padding: 15px;}
</style>


<body class="gray-bg">
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>系统信息</h5>
                               
                            </div>
                            <div class="ibox-content">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-hover margin bottom">
                                            <tbody>
                                                <tr>
                                                    <td>Thinkask程序版本</td>
                                                    <td >Alpha  V1.0</td>
                                                </tr>
                                                <tr>
                                                    <td>服务器系统及 PHP</td>
                                                    <td ><?php echo PHP_OS."/".phpversion(); ?></td>
                                                </tr>
                                               <tr>
                                                    <td>AGENT</td>
                                                    <td ><?php echo $_SERVER['HTTP_USER_AGENT']; ?></td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Apache </td>
                                                    <td ><?php echo $_SERVER['SERVER_SOFTWARE']; ?></td>
                                                </tr>

                                                 <tr>
                                                    <td>PHP运行方式 </td>
                                                    <td ><?php echo php_sapi_name(); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>MySql版本 </td>
                                                    <td ><?php  ?></td>
                                                </tr>
                                                <tr>
                                                    <td>上传附件限制 </td>
                                                    <td ><?php echo ini_get('upload_max_filesize'); ?></td>
                                                </tr>
                                                <tr>
                                                    <td>执行时间限制 </td>
                                                    <td ><?php echo ini_get('max_execution_time'); ?>秒</td>
                                                </tr>
                                                 <tr>
                                                    <td>剩余空间 </td>
                                                    <td ><?php echo round((@disk_free_space(".") / (1024 * 1024)), 2) . 'M'; ?></td>
                                                </tr>

                                               
                                            </tbody>
                                        </table>
                                    </div>
                              
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>Thinkask开发团队</h5>
                             
                            </div>
                            <div class="ibox-content">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <table class="table table-hover margin bottom">
                                            <tbody>
                                               
                                   

                                               
                                            </tbody>
                                        </table>
                                    </div>
                              
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



    <script src="/static/plus/js/bootstrap.min.js?v=3.3.5"></script>
    <script src="/static/plus/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    
    <script src="/static/plus/js/hplus.min.js?v=4.0.0"></script>
    <script type="text/javascript" src="/static/plus/js/contabs.min.js"></script>
    <script src="/static/plus/js/plugins/pace/pace.min.js"></script>
    <script src="/static/CMZPLUS/hdjs.js" type="text/javascript"></script>  
    
 



    <script src="/static/plus/js/plugins/chosen/chosen.jquery.js"></script>
    <script src="/static/plus/js/plugins/jsKnob/jquery.knob.js"></script>
    <script src="/static/plus/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="/static/plus/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="/static/plus/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
 
    <script src="/static/plus/js/plugins/nouslider/jquery.nouislider.min.js"></script>
 
    <script src="/static/plus/js/plugins/switchery/switchery.js"></script>
    <script src="/static/plus/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>
    <script src="/static/plus/js/plugins/iCheck/icheck.min.js"></script>
    <script src="/static/plus/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/static/plus/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="/static/plus/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="/static/plus/js/plugins/cropper/cropper.min.js"></script>

<script src="/static/plus/js/demo/form-advanced-demo.min.js"></script>

<script type="text/javascript">
    //
    $(".i-checks").iCheck({
        checkboxClass: "icheckbox_square-green",
        radioClass: "iradio_square-green"
    })
    //时间
    $(".input-group.date").datepicker({
        todayBtn: "linked",
        keyboardNavigation: !1,
        forceParse: !1,
        calendarWeeks: !0,
        autoclose: !0,
        format: "yyyy.mm.dd"

    })
</script>
</body>

</html>
