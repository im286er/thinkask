<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:42:"../template/thinkask/admin\user\group.html";i:1478566143;s:45:"../template/thinkask/admin\public\header.html";i:1478566143;s:45:"../template/thinkask/admin\public\footer.html";i:1478566143;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
        <title><?php echo $title; ?></title>

    <meta name="keywords" content="<?php echo $keywords; ?>">
    <meta name="description" content="<?php echo $description; ?>">

    <!--[if lt IE 8]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->


    <link href="/static/plus/css/bootstrap.min.css?v=3.3.5" rel="stylesheet">
    <link href="/static/plus/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="/static/plus/css/animate.min.css" rel="stylesheet">
    <link href="/static/plus/css/style.min.css?v=4.0.0" rel="stylesheet">
 <link href="/static/plus/css/plugins/chosen/chosen.css" rel="stylesheet">
 <link href="/static/plus/css/plugins/iCheck/custom.css" rel="stylesheet">
 <script src="/static/plus/js/jquery.min.js?v=2.1.4"></script>
 <script src="/static/plus/js/plugins/layer/layer.js"></script>
 <script src="/static/cmz/hdjs.js" type="text/javascript"></script>  
 <script src="/static/cmz/jquery.form.js" type="text/javascript"></script>

<link href="/static/plus/css/plugins/jsTree/style.min.css" rel="stylesheet">


   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    layer.config({
        extend: ['skin/moon/style.css'], //加载新皮肤
        skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    });

    </script>
     <link href="/static/cmz/cmzcss/cmz.css" rel="stylesheet">
</head>
<style type="text/css">
  
  .aw-content-wrap{padding: 15px;}
</style>


<style type="text/css">
    #content-main{padding: 5px;}
</style>
<div class="aw-content-wrap">
    <div class="mod">
        <div class="mod-head">
            <h3>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#_members" data-toggle="tab" id="members"><?php e('会员组'); ?></a></li>
                    <li><a href="#_system" data-toggle="tab" id="system"><?php e('系统组'); ?></a></li>
                    <li><a href="#_custom" data-toggle="tab" id="custom"><?php e('特殊组'); ?></a></li>
                    <!-- <li><a href="admin/ticket/service_group_list/"><?php// e('客服组'); ?></a></li> -->
                </ul>
            </h3>
        </div>
        <div class="mod-body tab-content">
            <div class="tab-pane active" id="_members">
                <div class="table-responsive">
                    <form id="batchs_form" action="admin/ajax/save_user_group/" method="post" onsubmit="return false;">
                        <table class="table table-striped" id="members_table">
                            <thead>
                                <tr>
                                    <th><input type="checkbox" class="check-all"></th>
                                    <th>ID</th>
                                    <th><?php e('会员组名称'); ?></th>
                                    <th><?php e('威望介于'); ?></th>
                                    <th><?php e('威望系数'); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php if ($group) { foreach ($group AS $key => $val) { if($val['type']==1): ?>
                                <tr>
                                    <td><input type="checkbox" value="<?php echo $val['group_id']; ?>" name="group_ids[]"></td>
                                    <td><?php echo $val['group_id']; ?></td>
                                    <td>
                                        <input class="form-control" type="text" name="group[<?php echo $val['group_id']; ?>][group_name]" value="<?php echo $val['group_name']; ?>" />
                                    </td>
                                    <td class="form-inline">
                                        <div class="form-group">
                                            <input type="text" name="group[<?php echo $val['group_id']; ?>][reputation_lower]" value="<?php echo $val['reputation_lower']; ?>" class="form-control control-value" />
                                            ~
                                            <input type="text" name="group[<?php echo $val['group_id']; ?>][reputation_higer]" class="form-control max-value" value="<?php echo $val['reputation_higer']; ?>" />
                                        </div>
                                    </td>
                                    <td><input type="text" name="group[<?php echo $val['group_id']; ?>][reputation_factor]" class="form-control" value="<?php echo $val['reputation_factor']; ?>" /></td>
                                    <td><a href="admin/user/group_edit/group_id-<?php echo $val['group_id']; ?>" title="<?php e('权限编辑'); ?>" class="icon icon-edit md-tip"></a></td>
                                </tr>
                                <?php endif; } } ?>
                                <tr id="members_add_form" class="collapse">
                                    <td></td>
                                    <td></td>
                                    <td>
                                        <input type="text" class="form-control" name="group_new[group_name][]" placeholder="<?php e('用户组名称'); ?>" />
                                    </td>
                                    <td class="form-inline">
                                        <div class="form-group">
                                            <input class="new_input" type="text" name="group_new[reputation_lower][]" />
                                            ~ <input type="text" class="input-small max-value form-control" name="group_new[reputation_higer][]" />
                                        </div>
                                    </td>
                                    <td><input type="text" class="input-small form-control" name="group_new[reputation_factor][]" /></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="mod-table-foot">
                            <a class="btn btn-primary add"><?php e('新增'); ?></a>
                            <a class="btn btn-primary" ><?php e('保存'); ?></a>
                            <a class="btn btn-danger" ><?php e('删除'); ?></a>
                        </div>
                    </form>
                </div>
            </div>

            <div class="tab-pane" id="_system">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th><?php e('系统组名称'); ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($group AS $key => $val) { if($val['type']==0): ?>
                        <tr>
                            <td><?php echo $val['group_id']; ?></td>
                            <td><?php echo $val['group_name']; ?></td>
                            <td><a href="admin/user/group_edit/group_id-<?php echo $val['group_id']; ?>" title="<?php e('权限编辑'); ?>" class="icon icon-edit md-tip"></a></td>
                        </tr>
                        <?php endif; } ?>
                    </tbody>
                </table>
            </div>

            <div class="tab-pane" id="_custom">
                <form method="post" action="admin/ajax/save_custom_user_group/" id="custom_form">
                <table class="table table-striped" id="custom_table">
                    <thead>
                        <tr>
                            <th><input type="checkbox" class="check-all"></th>
                            <th>ID</th>
                            <th><?php e('名称'); ?></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if ($group) { foreach ($group AS $key => $val) { if($val['type']==1): ?>
                        <tr>
                            <td><input type="checkbox" value="<?php echo $val['group_id']; ?>" name="group_ids[]"></td>
                            <td><?php echo $val['group_id']; ?></td>
                            <td><input type="text" class="form-control" name="group[<?php echo $val['group_id']; ?>][group_name]" value="<?php echo $val['group_name']; ?>" /></td>
                            <td><a href="admin/user/group_edit/group_id-<?php echo $val['group_id']; ?>" title="<?php e('权限编辑'); ?>" class="icon icon-edit md-tip"></a></td>
                        </tr>
                        <?php endif; } } ?>
                        <tr id="custom_add_form" class="collapse">
                            <td></td>
                            <td></td>
                            <td><input type="text" class="form-control" name="group_new[group_name][]" placeholder="<?php e('用户组名称'); ?>" /></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                </form>
                <div class="mod-table-foot">
                    <a class="btn btn-primary" ><?php e('新增'); ?></a>
                    <a class="btn btn-primary" ><?php e('保存'); ?></a>
                    <a class="btn btn-danger" ><?php e('删除'); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

    <script src="/static/plus/js/bootstrap.min.js?v=3.3.5"></script>
    <script src="/static/plus/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    
    <script src="/static/plus/js/hplus.min.js?v=4.0.0"></script>
    <script type="text/javascript" src="/static/plus/js/contabs.min.js"></script>
    <script src="/static/plus/js/plugins/pace/pace.min.js"></script>
    <script src="/static/CMZPLUS/hdjs.js" type="text/javascript"></script>  
    
 



    <script src="/static/plus/js/plugins/chosen/chosen.jquery.js"></script>
    <script src="/static/plus/js/plugins/jsKnob/jquery.knob.js"></script>
    <script src="/static/plus/js/plugins/jasny/jasny-bootstrap.min.js"></script>
    <script src="/static/plus/js/plugins/datapicker/bootstrap-datepicker.js"></script>
    <script src="/static/plus/js/plugins/prettyfile/bootstrap-prettyfile.js"></script>
 
    <script src="/static/plus/js/plugins/nouslider/jquery.nouislider.min.js"></script>
 
    <script src="/static/plus/js/plugins/switchery/switchery.js"></script>
    <script src="/static/plus/js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>
    <script src="/static/plus/js/plugins/iCheck/icheck.min.js"></script>
    <script src="/static/plus/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="/static/plus/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
    <script src="/static/plus/js/plugins/clockpicker/clockpicker.js"></script>
    <script src="/static/plus/js/plugins/cropper/cropper.min.js"></script>

<script src="/static/plus/js/demo/form-advanced-demo.min.js"></script>

<script type="text/javascript">
    //
    $(".i-checks").iCheck({
        checkboxClass: "icheckbox_square-green",
        radioClass: "iradio_square-green"
    })
    //时间
    $(".input-group.date").datepicker({
        todayBtn: "linked",
        keyboardNavigation: !1,
        forceParse: !1,
        calendarWeeks: !0,
        autoclose: !0,
        format: "yyyy.mm.dd"

    })
</script>
</body>

</html>