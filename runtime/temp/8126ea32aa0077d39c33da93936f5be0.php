<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:37:"../template/5ihelp/index\article.html";i:1478566141;s:37:"../template/5ihelp/public\header.html";i:1478566144;s:37:"../template/5ihelp/public\footer.html";i:1478566144;}*/ ?>
<!DOCTYPE html>
<html>
<head>
<meta content="text/html;charset=utf-8" http-equiv="Content-Type" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta name="renderer" content="webkit" />
<title>5ihelp</title>
<meta name="keywords" content="" />
<meta name="description" content=""  />
<!-- <base href="http://git.cooldreamer.com/ask/?/" /> -->
<!-- <link href="http://git.cooldreamer.com/ask/static/css/default/img/favicon.ico?v=20160523" rel="shortcut icon" type="image/x-icon" /> -->

<link rel="stylesheet" type="text/css" href="/static/5iask/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="/static/5iask/css/icon.css" />
<link href="/static/5iask/css/default/common.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/css/default/link.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<link href="/static/5iask/js/plug_module/style.css?v=<?php echo time(); ?>" rel="stylesheet" type="text/css" />
<script src="/static/5iask/js/jquery.2.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script src="/static/5iask/js/jquery.form.js?v=<?php echo time(); ?>" type="text/javascript"></script>
<script type="text/javascript" src="/static/plus/js/bootstrap.min.js?v=<?php echo time(); ?>"></script>

 <script src="/static/plus/js/plugins/layer/layer.js"></script>
   <!-- //公共的JS-FUNCTION函数 -->
   <script src="/static/cmz/PublicVlidate.js" type="text/javascript"></script>
   <script src="/static/cmz/Models.js" type="text/javascript"></script>
   <!-- //JS 控制器处理  -->
   <script src="/static/cmz/action.js" type="text/javascript"></script>

   <script src="/static/cmz/install.js" type="text/javascript"></script>
   <script src="/static/plus/js/jquery.zclip.min.js" type="text/javascript"></script>

    <script type="text/javascript">
    // layer.config(
    //     extend: ['skin/moon/style.css'], //加载新皮肤
    //     skin: 'layer-ext-moon' //一旦设定，所有弹层风格都采用此主题。
    // );

    </script>
<!--[if lte IE 8]>
	<script type="text/javascript" src="/static/5iask/js/respond.js"></script>
<![endif]-->
</head>

<body>
	<div class="aw-top-menu-wrap">
		<div class="container">
			<!-- logo -->

			<div class="aw-logo hidden-xs">
				<a href="/"><img style="position: relative;top:-2px;left:10px;" src="/static/images/logo/thinkask_logo_black.png" height="45" alt=""></a>
			</div>
			<!-- end logo -->
			<!-- 搜索框 -->
			<div class="aw-search-box  hidden-xs hidden-sm">
				<form class="navbar-search" action="search/" id="global_search_form" method="post">
					<input class="form-control search-query" type="text" placeholder="搜索问题、话题或人" autocomplete="off" name="q" id="aw-search-query" />
					<span title="搜索" id="global_search_btns" ><i class="icon icon-search"></i></span>
					<div class="aw-dropdown">
						<div class="mod-body">
							<p class="title">输入关键字进行搜索</p>
							<ul class="aw-dropdown-list collapse"></ul>
							<p class="search"><span>搜索:</span><a ></a></p>
						</div>
						<div class="mod-footer" style="display:none;">
							<a href=""  class="pull-right btn btn-mini btn-success publish">发起问题</a>
						</div>
					</div>
				</form>
			</div>
			<!-- end 搜索框 -->
			<!-- 导航 -->
			<div class="aw-top-nav navbar">
				<div class="navbar-header">
				  <button  class="navbar-toggle pull-left">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				</div>
				<nav role="navigation" class="collapse navbar-collapse bs-navbar-collapse">
				  <ul class="nav navbar-nav">
				  <li><a href="<?php echo url('index/index/index'); ?>" class="<?php if($doaction=="home"){ echo "active";} ?>" ><i class="icon icon-index"></i> 发现</a></li>

					<?php  if($uid>0){ ?>
						<!-- <li><a href="home/"><i class="icon icon-home"></i> 动态</a></li> -->
					<?php }  ?>
					<!-- <li><a href="" class=""><i class="icon icon-list"></i> 发现</a></li> -->
					<li><a href="<?php echo url('index/question/index'); ?>"  class="<?php if($doaction=="question"){ echo "active";} ?>">问题</a></li>
					<li><a href="<?php echo url('index/article/index'); ?>" class="<?php if($doaction=="article"){ echo "active";} ?>">文章</a></li>
					<!-- <li><a href="topic/"><i class="icon icon-topic"></i>话题</a></li> -->
					<?php  if ($uid>0){  ?>
					<li style="display:none;">
						<a href="notifications/" class=""><i class="icon icon-bell"></i> 通知</a>
						<span class="badge badge-important" style="display:none" id="notifications_unread"><?php // // echo $this->user_info['notification_unread']; ?></span>
						<div class="aw-dropdown pull-right hidden-xs">
							<div class="mod-body">
								<ul id="header_notification_list"></ul>
							</div>
							<div class="mod-footer">
								<a href="notifications/">查看全部</a>
							</div>
						</div>
					</li>
					<?php }   ?>
					<!-- <li><a href="help/"> <i class="icon icon-bulb"></i> 帮助</a></li> -->
					<li>
						<a style="font-weight:bold;">· · ·</a>
						<!-- <div class="dropdown-list pull-right">
							<ul id="extensions-nav-list">
								<li><a href="ticket/"><i class="icon icon-order"></i> 工单</a></li>
								<li><a href="project/"><i class="icon icon-activity"></i> 活动</a></li>
							</ul>
						</div> -->
					</li>
				  </ul>
				</nav>
			</div>
			<!-- end 导航 -->
			<!-- 用户栏 -->
			<div class="aw-user-nav">
				<!-- 登陆&注册栏 -->
				<?php  if($uid>0){ ?>
					<a href="people/" style="" class="aw-user-nav-dropdown">
						<img alt="<?php echo $userinfo['user_name']; ?>" src="/uploads/avatar/<?php echo $userinfo['avatar_file']; ?>" />
							<span class="badge badge-important"></span>
					</a>
					<div class="aw-dropdown dropdown-list pull-right" style="">
						<ul class="aw-dropdown-list">
							<li><a href="inbox/"><i class="icon icon-inbox"></i> 私信<span class="badge badge-important collapse" id="inbox_unread">0</span></a></li>
							<li class="hidden-xs"><a href="account/setting/profile/"><i class="icon icon-setting"></i> 设置</a></li>
							<li class="hidden-xs"><a href="<?php echo url('admin/index/index'); ?>"><i class="icon icon-job"></i> 管理</a></li>
							<li><a href="account/logout/"><i class="icon icon-logout"></i></a></li>
						</ul>
					</div>
				<?php  }else{ ?>
					<a class="login btn btn-normal btn-primary" href="<?php echo url('ucenter/user/login'); ?>">登录</a>
					<a class="register btn btn-normal btn-success" href="<?php echo url('ucenter/user/reg'); ?>">注册</a>
				<?php }  ?>	
				<!-- end 登陆&注册栏 -->
			</div>
			<!-- end 用户栏 -->
			<!-- 发起 -->
			<?php if($uid>0){ ?>
			<div class="aw-publish-btn" style="">
				<a id="header_publish" class="btn-primary"href="publish/"><i class="icon icon-ask"></i>发起</a>
				<div class="dropdown-list pull-right">
					<ul>
						<li>
						<a href="<?php echo url('post/publish/question'); ?>">问题</a>
					
						</li>
						<li>
							
						<a href="<?php echo url('post/publish/article'); ?>">文章</a>
						
						</li>
				<!-- <li><a href="ticket/publish/">工单</a></li>
						<li><a href="project/publish/">活动</a></li> -->
				
					</ul>
				</div>
			</div>
			<?php } ?>
			<!-- end 发起 -->
		</div>
	</div>

	<div class="aw-email-verify" style="display:none;">
		<div class="container text-center">
			<a onclick="AWS.ajax_request(G_BASE_URL + '/account/ajax/send_valid_mail/');"></a>
		</div>
	</div>


<div class="aw-container-wrap">
    <div class="container">
        <div class="row">
            <div class="aw-content-wrap clearfix">
                <div class="col-sm-12 col-md-9 aw-main-content aw-article-content">
                    <div class="aw-mod aw-topic-bar" id="question_topic_editor" data-type="article" data-id="<?php // echo $this->article_info['id']; ?>">
                        <div class="tag-bar clearfix">
                            <?php // if ($this->article_topics) { // foreach($this->article_topics as $key => $val) { ?>
                            <span class="topic-tag" data-id="<?php // echo $val['topic_id']; ?>">
                                <a class="text" href="topic/<?php // echo $val['url_token']; ?>"><?php // echo $val['topic_title']; ?></a>
                            </span>
                            <?php // } // } // if ($this->user_info['permission']['is_administortar'] OR $this->user_info['permission']['is_moderator']) { ?><span class="icon-inverse aw-edit-topic"><i class="icon icon-edit"></i> <?php // if (sizeof($this->article_topics) == 0) { // _e('添加话题')// } ?></span><?php // } ?>
                        </div>
                    </div>
                    <div class="aw-mod aw-question-detail">
                        <div class="mod-head">
                            <h1>
                                <?php echo $title; ?>
                            </h1>

                            <?php if ($userinfo['uid']==$uid) { ?>
                            <div class="operate clearfix">
                                <!-- 下拉菜单 -->
                                <div class="btn-group pull-left">
                                    <a class="btn btn-gray dropdown-toggle" data-toggle="dropdown" href="javascript:;">...</a>
                                    <div class="dropdown-menu aw-dropdown pull-right" role="menu" aria-labelledby="dropdownMenu">
                                        <ul class="aw-dropdown-list">
                                            <li>
                                                <a href="javascript:;" >
                                                <?php  if ($lock) { ?>
                                                解除锁定
                                                <?php  } else { ?>
                                                锁定文章
                                                <?php  } ?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" >删除文章</a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" >
                                                <?php  if ($is_recommend) { ?>
                                                取消推荐
                                                <?php  } else { ?>
                                                推荐文章
                                                <?php  } ?>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" >添加到帮助中心</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- end 下拉菜单 -->
                            </div>

                            <?php  } ?>
                        </div>
                        <div class="mod-body">
                       
                            <div class="content markitup-box" style="min-height:300px;">
                            <?php if($userinfo['group_id']!=$power_group&&$userinfo['group_id']!=1&&$userinfo['group_id']!=2){ echo "没有查看权限";}else{echo $message;} ?>

                              <!--   <div class="aw-upload-img-list">
                                    <a href="" target="_blank" data-fancybox-group="thumb" rel="lightbox"><img src="" class="img-polaroid" alt="" /></a>
                                </div> -->
                             
                         <!--  附件区域 -->
                                <?php // if ($this->article_info['attachs']) {  ?>
                                <ul class="aw-upload-file-list" style="display:none;">
                                    <?php // foreach ($this->article_info['attachs'] AS $attach) { // if (!$attach['is_image'] AND (!$this->article_info['attachs_ids'] OR !in_array($attach['id'], $this->article_info['attachs_ids']))) { ?>
                                        <li><a href="<?php // echo download_url($attach['file_name'], $attach['attachment']); ?>"><i class="icon icon-attach"></i> <?php // echo $attach['file_name']; ?></a></li>
                                    <?php // } // } ?>
                                </ul>
                               <!--  附件区域 -->
                            </div>
                            
                            <div class="meta clearfix">
                                <div class="aw-article-vote pull-left">
                                    <a href="javascript:;" class="agree" ><i class="icon icon-agree"></i> <b><?php // echo $this->article_info['votes']; ?></b></a>
                                   
                                    <a href="javascript:;" class="disagree active" ><i class="icon icon-disagree"></i></a>
                                  
                                </div>

                                <span class="pull-right  more-operate">
                                   <?php if($userinfo['uid']==$uid): ?>
                                    <a class="text-color-999" href="<?php echo url('post/publish/article'); ?>?id=<?php echo $id; ?>"><i class="icon icon-edit"></i> 编辑</a>
                                   <?php endif; if($userinfo['uid']>0): ?>
                                        <a href="javascript:;"  class="text-color-999"><i class="icon icon-favor"></i>收藏
                                   <?php endif; ?>
                                   
                                    <a class="text-color-999 dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon icon-share"></i> 分享
                                    </a>
                                    <div aria-labelledby="dropdownMenu" role="menu" class="aw-dropdown shareout pull-right">
                                        <ul class="aw-dropdown-list">
                                            <li><a ><i class="icon icon-weibo"></i> 微博</a></li>
											<li><a ><i class="icon icon-qzone"></i> QZONE</a></li>
											<li><a ><i class="icon icon-wechat"></i> 微信</a></li>
                                        </ul>
                                    </div>

                                    <em class="text-color-999"><?php echo date_friendly($add_time,604800,'Y-m-d'); ?></em>
                                </span>
                            </div>
                        </div>
                        <div class="mod-footer">
                            <?php // if ($this->article_info['vote_users']) { ?>
                            <div class="aw-article-voter">
                                <?php // foreach ($this->article_info['vote_users'] AS $key => $val) { ?>
                                <a href="people/<?php // echo $val['url_token']; ?>" class="voter" data-toggle="tooltip" data-placement="right" data-original-title="<?php // echo $val['user_name']; ?>"><img alt="<?php // echo $val['user_name']; ?>" src="<?php // echo get_avatar_url($val['uid'], 'mid'); ?>" /></a>
                                <?php // } ?>
                                <!--<a class="more-voters">...</a>-->
                            </div>
                            <?php // } ?>
                        </div>
                    </div>

                    <!-- 文章评论 -->
                    <div class="aw-mod">
                        <div class="mod-head common-head">
                            <h2><?php echo $comments; ?> 个评论</h2>
                        </div>

                        <div class="mod-body aw-feed-list">
                           

                            
                                <div class="aw-item" id="answer_list_<?php // echo $val['id']; ?>">
                                    <div class="mod-head">
                                        <a class="aw-user-img aw-border-radius-5" href="people/">
                                            <img src="/uploads/avatar/<?php echo $userinfo['avatar_file']; ?>" alt="<?php echo $userinfo['user_name']; ?>" />
                                        </a>
                                        <p>
                                            <a href="people/"></a> 回复 <a href="people/"><?php // echo $val['at_user_info']['user_name']; ?></a><?php // } ?>
                                        </p>
                                    </div>
                                    <div class="mod-body">
                                        <div class="markitup-box">
                                            <?php // echo nl2br($val['message']); ?>
                                        </div>
                                    </div>
                                    <div class="mod-footer">
                                        <div class="meta">
                                            <span class="pull-right text-color-999"><?php // echo date_friendly($val['add_time']); ?></span>
                                            <?php  if ($uid>0) { ?>
                                                <a class="text-color-999  active ?>" >
                                                	<i class="icon icon-agree"></i>  我已赞赞</a>
                                                <a class="aw-article-comment text-color-999" data-id="<?php // echo $val['user_info']['uid']; ?>"><i class="icon icon-comment"></i> 回复</a>
                                            
                                                <a class="text-color-999" ><i class="icon icon-trash"></i> 删除</a>
                                           <?php } ?>
                                        </div>
                                    </div>
                                </div>
                          
                        </div>

                        <?php // if ($_GET['item_id']) { ?>
                        <div class="mod-footer">
                                <a href="article" class="aw-load-more-content">
                                    <span>查看全部评论</span>
                                </a>
                        </div>
                        <?php // } // if ($this->pagination) { ?>
                            <div class="clearfix"><?php // echo $this->pagination; ?></div>
                        <?php // } ?>
                    </div>
                    <!-- end 文章评论 -->

                    <!-- 回复编辑器 -->
                    <div class="aw-mod aw-article-replay-box">
                        <a name="answer_form"></a>
                        <?php  if ($lock>0) { ?>
                        <p align="center">该文章目前已经被锁定, 无法添加新评论</p>
                        <?php } else if (!$uid) { ?>
                        <p align="center">要回复文章请先<a href="<?php echo url('ucenter/user/login'); ?>">登录</a>或<a href="<?php echo url('ucenter/user/reg'); ?>">注册</a>');</p>
                        <?php  } else { ?>
                        <form action="article/ajax/save_comment/" onsubmit="return false;" method="post" id="answer_form">
                        <input type="hidden" name="post_hash" value="<?php // echo new_post_hash(); ?>" />
                        <input type="hidden" name="article_id" value="<?php // echo $this->article_info['id']; ?>" />
                        <div class="mod-head">
                            <a href="people/" class="aw-user-name"><img alt="<?php // echo $this->user_info['user_name']; ?>" src="/uploads/avatar/<?php echo $userinfo['avatar_file']; ?>" /></a>
                        </div>
                        <div class="mod-body">
                            <textarea rows="3" name="message" id="comment_editor" class="form-control autosize" placeholder="写下你的评论..."  /></textarea>
                        </div>
                        <div class="mod-footer clearfix">
                            <a href="javascript:;" class="btn btn-normal btn-success pull-right btn-submit btn-reply">回复</a>
                       <!--      <em class="auth-img pull-right"><img src=""  id="captcha" /></em>
                            <input class="pull-right form-control" type="text" name="seccode_verify" placeholder="验证码" /> -->
                        
                        </div>
                        </form>
                	   <?php } ?>
                    </div>
                    <!-- end 回复编辑器 -->
                </div>
                <!-- 侧边栏 -->
                <div class="col-sm-12 col-md-3 aw-side-bar hidden-sm hidden-xs">
                    <!-- 发起人 -->
                    <?php // if ($this->article_info['anonymous'] == 0) { ?>
                    <div class="aw-mod user-detail">
                        <div class="mod-head">
                            <h3>发起人</h3>
                        </div>
                        <div class="mod-body">
                            <dl>
                                <dt class="pull-left aw-border-radius-5">
                                    <a href="people/"><img alt="<?php echo $user_name; ?>" src="/uploads/avatar/<?php echo $avatar_file; ?>" /></a>
                                </dt>
                                <dd class="pull-left">
                                    <a class="aw-user-name" href="people/" ></a>
                                   
                                        <!-- <i class="icon-v" title="企业认证 个人认证"></i> -->
                                   

                                   >
                                    <a class="icon-inverse follow tooltips icon icon-plus active" ></a>
                                
                                    <p></p>
                                </dd>
                            </dl>
                        </div>
                        <div class="mod-footer clearfix">
                            
                            <div class="aw-topic-bar">
                                <div class="topic-bar clearfix">
                                    <span class="pull-left text-color-999">
                                       擅长话题: &nbsp;
                                    </span>
                                   
                                    <span class="topic-tag">
                                        <a href="topic/" class="text" data-id=""></a>
                                    </span>
                                   
                                </div>
                            </div>
                           
                        </div>
                    </div>
                    <?php // } ?>
                    <!-- end 发起人 -->

                    <?php // if ($this->recommend_posts) { ?>
                    <!-- 推荐内容 -->
                    <div class="aw-mod">
                        <div class="mod-head">
                            <h3>推荐内容</h3>
                        </div>
                        <div class="mod-body">
                            <ul>
                                
                                <li>
                                 
                                    <a href="question/<?php // echo $val['question_id']; ?>"><?php // echo $val['question_content']; ?></a>
                                    
                                    <a href="article/<?php // echo $val['id']; ?>"><?php // echo $val['title']; ?></a>
                                 
                                </li>
                               
                            </ul>
                        </div>
                    </div>
                    <!-- end 推荐内容 -->
                    <?php // } // if ($this->question_related_list) { ?>
                    <!-- 相关问题 -->
                    <div class="aw-mod aw-text-align-justify question-related-list">
                        <div class="mod-head">
                            <h3><?php // _e('相关问题'); ?></h3>
                        </div>
                        <div class="mod-body font-size-12">
                            <ul>
                                <?php // foreach($this->question_related_list AS $key => $val) { ?>
                                <li><a href="question/<?php // echo $val['question_id']; ?>"><?php // echo $val['question_content']; ?></a></li>
                                <?php // } ?>
                            </ul>
                        </div>
                    </div>
                    <!-- end 相关问题 -->
                    <?php // } ?>
                </div>
                <!-- end 侧边栏 -->
            </div>
        </div>
    </div>
</div>
<?php die; ?>
<div class="aw-footer-wrap">
	<div class="aw-footer">
		Copyright © <?php // echo date('Y'); // if(get_setting('icp_beian')){ ?><span class="hidden-xs"> - <?php // echo get_setting('icp_beian'); // } ?>, All Rights Reserved</span>

		<span class="hidden-xs">Powered By <a href="http://www.5ihelp.com" target="blank">5ihelp </a>感谢Wecenter提供站点框架支持</span>

		<?php // if (is_mobile(true)) { ?>
			<div class="container">
				<div class="row">
					<p align="center"><?php // _e('版本切换'); ?>: <b><?php // _e('PC 版'); ?></b> | <a href="m/ignore_ua_check-FALSE"><?php // _e('手机版'); ?></a></p>
				</div>
			</div>
		<?php // } ?>
	</div>
</div>

<a class="aw-back-top hidden-xs" href="javascript:;" onclick="$.scrollTo(1, 600, {queue:true});"><i class="icon icon-up"></i></a>



<!-- DO NOT REMOVE -->
<div id="aw-ajax-box" class="aw-ajax-box"></div>

</body>
</html>
