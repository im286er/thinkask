<?php
namespace app\admin\controller;
use think\Controller;

class Index extends controller
{
  public function index(){
  	//菜单
  	$this->assign('menu',config('adminmenu'));
  	return $this->fetch('admin/index/index');
  }
  public function toMain(){
  	return $this->fetch('admin/index/tomain');
  }
}
