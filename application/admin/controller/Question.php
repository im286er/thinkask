<?php
namespace app\admin\controller;
use app\common\controller\Base;

class Question extends Base
{
 public function list(){
 	$this->assign('category',$category = model('Base')->getall('category'));
 	$where=[];
 	if(current($this->request->only(['keyword']))) $where['question_content']=['like','%'.current($this->request->only(['keyword'])).'%'];;
 	if(current($this->request->only(['category_id']))) $where['category_id']=current($this->request->only(['category_id']));
 	// show($where);
 	if(current($this->request->only(['user_name']))) $where['user_name']=current($this->request->only(['user_name']));
 	// $user_name = $this->request->only(['user_name']);
 	// $answer_count_min = $this->request->only(['answer_count_min']);
 	// $answer_count_max = $this->request->only(['answer_count_max']);
 	// $best_answer = $this->request->only(['best_answer']);
    $this->assign('list',$re = model('Base')->getall('question',['where'=>$where,'alias'=>'qu','join'=>[['aws_users u','u.uid=qu.published_uid']]]));
    if($where){
    	$this->assign('searchcount',count($re));

    }
    return $this->fetch('admin/question/list');
  }


}
