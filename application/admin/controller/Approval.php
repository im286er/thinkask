<?php
namespace app\admin\controller;
use app\common\controller\Base;
class Approval extends Base{
   /**
    * [index 内容审核]
    * @return [type] [description]
    */
    public function content(){
    	
    return $this->fetch('admin/approval/content');
  }
  /**
   * [user 用户审核]
   * @return [type] [description]
   */
  public function user(){
    return $this->fetch('admin/approval/user');
  }
  /**
   * [report_list 用户举报]
   * @return [type] [description]
   */
  public function report_list(){
    return $this->fetch('admin/approval/report_list');
  }

}