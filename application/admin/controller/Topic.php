<?php
namespace app\admin\controller;
use think\Controller;

class Topic extends controller
{
	/**
	 * [list 列表]
	 * @return [type] [description]
	 */
  public function list(){

  	$this->assign('list',model('Topic')->getList());
  	return $this->fetch('admin/topic/list');
  }
  /**
   * [parent 根话题 ]
   * @return [type] [description]
   */
 public function  parent(){

 
  	return $this->fetch('admin/topic/parent');
  }
  /**
   * [creat 新建话题]
   * @return [type] [description]
   */
 public function  creat(){

 
  	return $this->fetch('admin/topic/creat');
  }
}
