<?php 
/*
+--------------------------------------------------------------------------
|   WeCenter [#免费开发#]
|   ========================================
|   by Jerry
|   http://www.5ihelp.com
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\behavior;
use \think\Controller;
use \think\Request;
use \think\Session;
use \think\Cookie;
use \think\Hook;
use \think\Lang;
// echo "当前模块名称是" . $request->module();
// echo "当前控制器名称是" . $request->controller();
// echo "当前操作名称是" . $request->action();
class Power extends Controller
{
    protected   $request;
    public function run(&$params)
    {
        $this->checkSession();
        $this->nowAction();
        $this->request = Request::instance();
        Lang::load(APP_PATH . 'common/lang/zh-cn.php');
    }
    private function checkSession(){
    	if(Session::get('thinkask_uid')){
    		$this->assign('uid',Session::get('thinkask_uid'));
            $userinfo = model('users')->getUserByUid(Session::get('thinkask_uid'));
            $this->assign('userinfo',$userinfo);
            if($this->request->module()=="admin"&&$userinfo['group_id']!=1){
                $this->error('您没有操作权限');
            }
    	}else{
    		$this->assign('uid',"0");
    	}
    }
    private function nowAction(){
        
        $module = $this->request->module();
        $controller = $this->request->controller();

        if(strtolower($module)=="index"&&strtolower($controller)=="index"){
            $doaction = "home";
        }


        if(strtolower($module)=="index"&&strtolower($controller)=="article"){
            
            $doaction = "article";
        }
        if(strtolower($module)=="index"&&strtolower($controller)=="question"){
           $doaction = "question";
        }
         $this->assign('doaction',$doaction);


    }

}