<?php 


return [
     'window_action'  => 'Windows',
     'home_action'  => '首页',
     'theme_action'  => '模板',

     //error
     'close_all'=>'关闭所有',
     'error'=>'错误',
     'action_error'=>'操作错误',
     'power_error'=>'权限错误',
     '404_error'=>'404错',
     //menu
     'admin_menu_index'=>'首页',
     'admin_glob_set'=>'全局设置',
     'admin_web_set'=>'站点信息',
     'admn_reg_view'=>'注册访问',
     'admin_web_function'=>'站点功能',
     'admin_content_set'=>'内容设置',
     'admin_score'=>'威望积分',
     'admin_user_power'=>'用户权限',
     'admi_mail_set'=>'邮件设置',
     'admin_open'=>'开放平台',
     'admin_performance_optimization'=>'性能优化',
     'admin_visual_set'=>'界面设置',

     'admin_content_manager'=>'内容管理',
     'admin_question_manager'=>'问题管理',
     'admin_article_manager'=>'文章管理',
     'admin_topic_manager'=>'话题管理',

     'admin_user_center'=>'用户中心',
     'admin_user_list'=>'用户列表',
     'admin_user_group'=>'用户组',
     'admin_invitation'=>'批量邀请',
     'admin_job_management'=>'职位管理',

     'admin_audits_management'=>'审核管理',
     'admin_authentication_audit'=>'认证审核',
     'admin_reg_audit'=>'注册审核',
     'admin_user_accusation'=>'用户举报',

     'admin_content_set'=>'内容设置',
     'admin_navigation_set'=>'导航设置',
     'admin_category_set'=>'分类设置',
     'admin_subject_set'=>'专题设置',
     'admin_page_set'=>'页面设置',
     'admin_help'=>'帮助中心',


     'admin_sns_api'=>'第三方扩展开发',
     'admin_wexin_menu'=>'微信菜单',
     'admin_wexin_reply'=>'微信自定义回复',
     'admin_wexin_api'=>'微信第三方接入',
     'admin_wexin_code'=>'微信二维码管理',
     'admin_wexin_msg_group'=>'微信消息群发',
     'admin_webo_msg_get'=>'微博消息接收',
     'admin_mail_import'=>'邮件导入',

     'admin_mail_manager'=>'邮件管理',
     'admin_duty_manager'=>'任务管理',
     'admin_user_group_manager'=>'用户群管理',

     'admin_tool'=>'工具',
     'admin_system_maintenance'=>'系统维护',
     'admin_collection'=>'采集',
  
     'admin_data type error' => '数据类型错误',


     //相关按钮
     'BACK'=>'后退',
     'REFRESH'=>'刷新',
     'NEW_WINDWOWS_OPEN'=>'新窗口打开',
     'ClangEAN_CACHE'=>'清空缓存',
     'GO_FORWARD'=>'前进',


     'update_success'=>'更新成功',

];