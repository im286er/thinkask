<?php
/*
+--------------------------------------------------------------------------
|   WeCenter [#免费开发#]
|   ========================================
|   by Jerry
|   http://www.5ihelp.com
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\common\model;
use think\Model;
use think\Db;
class Question extends Model
{  
    protected $request;
    /**
     * [getList 列表]
     * @param  string $status [description]
     * @return [type]         [description]
     */
    static public function getList($status="news")
    { 
        switch ($status) {
                case 'hot':
                   $order = "views_count desc";
                    break;
                case 'recommend':
                   $order ="is_recommend desc";   
                    break;
               case 'unresponsive':
                   $order ="answer_userscase desc";   
                    break;
                default:
                    $order="published_uid desc";
                    break;
            }
        $join = [
                    ['aws_users us','a.published_uid=us.uid'],
                   
                ];
    	
    return $list = Db::name('question')->alias('a')->order($order)->join($join)->select();

    }
    public function edit($data){
    	$data['message'] = htmlspecialchars($data['message']);
    	$data['title'] = htmlspecialchars($data['title']);
    	$data['article_id']  = (int)$data['article_id'];
    	if($data['article_id']>0){
    		return $this->publish($data);
    	}else{
    		return $this->add($data);
    	}
    }
    private function add($data){
    	return  Db::name('question')->insertGetId($data);
    }
    private function publish($data){

    }
    /**
     * [getArById 根据ID查找出内容]
     * @param  [type] $id [description]
     * @return [type]     [description]
     */
    public function getDetailById($id){
        $join = [
                    ['aws_users us','a.published_uid=us.uid'],
                ];
        return Db::name('question')->alias('a')->join($join)->where('a.question_id',$id)->find();
    }
    public function getTopicById($id){
         $join = [
                    ['aws_topic tpc','rela.topic_id=tpc.topic_id'],
                ];
         return Db::name('topic_relation')->alias('rela')->join($join)->where('rela.type','question')->where('rela.item_id',$id)->select();
    }


}