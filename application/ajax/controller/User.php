<?php
/*
+--------------------------------------------------------------------------
|   WeCenter [#免费开发#]
|   ========================================
|   by Jerry
|   http://www.5ihelp.com
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
// echo '请求方法：' . $request->method() . '<br/>';
//         echo '资源类型：' . $request->type() . '<br/>';
//         echo '访问IP：' . $request->ip() . '<br/>';
//         echo '是否AJax请求：' . var_export($request->isAJax(), true) . '<br/>';
//         echo '请求参数：';
//         dump($request->param());
//         echo '请求参数：仅包含name';
//         dump($request->only(['name']));
//         echo '请求参数：排除name';
//         dump($request->except(['name']));
//         // 获取当前域名
// echo 'domain: ' . $request->domain() . '<br/>';
// // 获取当前入口文件
// echo 'file: ' . $request->baseFile() . '<br/>';
// // 获取当前URL地址 不含域名
// echo 'url: ' . $request->url() . '<br/>';
// // 获取包含域名的完整URL地址
// echo 'url with domain: ' . $request->url(true) . '<br/>';
// // 获取当前URL地址 不含QUERY_STRING
// echo 'url without query: ' . $request->baseUrl() . '<br/>';
// // 获取URL访问的ROOT地址
// echo 'root:' . $request->root() . '<br/>';
// // 获取URL访问的ROOT地址
// echo 'root with domain: ' . $request->root(true) . '<br/>';
// // 获取URL地址中的PATH_INFO信息
// echo 'pathinfo: ' . $request->pathinfo() . '<br/>';
// // 获取URL地址中的PATH_INFO信息 不含后缀
// echo 'pathinfo: ' . $request->path() . '<br/>';
// // 获取URL地址中的后缀信息
// echo 'ext: ' . $request->ext() . '<br/>';
namespace app\ajax\controller;
use app\ajax\model\User as askuser;
use think\Controller;
use think\Request;
use think\Session;
use think\Cookie;
class User extends Controller
{

	public function _initialize()
    {
       
    }
    /**
     * [logo 登陆]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function logo(Request $request)
    {
        $request = Request::instance();
        if($request->isAJax()){
            $user_name = $request->only(['user_name']);
            $password = $request->only(['password']);
            $returnurl = $request->only(['returnurl']);
            if(empty($user_name['user_name'])||empty($password['password'])){
                $this->error('用户名或者密码不能为空');
            }
            $re  = askuser::checkLogin($user_name['user_name']);
            if(is_array($re)&&!empty($re)){
                //比对密码  
                if($re['password']==encode_pwd($password['password'],$re['salt'])){
                    Session::set('thinkask_uid',$re['uid']);
                    Session::set('thinkask_uid',$re['uid']);
                    // 设置Cookie 有效期为 3600秒
                    Cookie::set('thinkask_uid',$re['uid']);

                    $this->success('登陆成功','',"");
                }else{
                    $this->error('密码错误');
                }
            }else{
                $this->error('用户名不存在');
            }
          
        }

    }
}
