<?php
/*
+--------------------------------------------------------------------------
|   WeCenter [#免费开发#]
|   ========================================
|   by Jerry
|   http://www.5ihelp.com
|   ========================================
|   如果有兴趣可以加群{开发交流群} 485114585
|   ========================================
|   更改插件记得先备份，先备份，先备份，先备份
|   ========================================
+---------------------------------------------------------------------------
 */
namespace app\ajax\controller;
use app\ajax\model\User as askuser;
use app\ajax\model\Article as Articlea;
use app\common\controller\Base;
class Question extends Base
{
	public function _initialize()
    {
       //用户是否登陆
       if($this->getuid()<0||!$this->request->isAJax()){
        return ;
       }
    }
    public function edit(){
    
        $id = (int)current($this->request->only(['id']));
       //字段判断
       if($question_content = $this->request->only(['question_content'])){
            if(empty($question_content['question_content'])){
                $this->error('标题不能为空');
            }
       }
        if($category_id = $this->request->only(['category_id'])){
            if(empty($category_id['category_id'])||$category_id['category_id']<1){
                $this->error('请选择分类');
            }
       }
       $ardb = $this->request->param();
       $ardb['add_time'] = time();
       $ardb['category_id'] = $ardb['category_id'];
       $ardb['published_uid'] = $this->getuid();
      
       if($id>0){
        //修改
         model('Base')->getedit('question',['where'=>"question_id=$id"],$ardb);
         model('Base')->getedit('posts_index',['where'=>"post_id=$id"],['update_time'=>time()]);
         $this->success('操作成功',url('index/question/index')."?id=".$id);
       }else{
        // 新加
           $id = model('Base')->getadd('question',$ardb);
           if($id){
            //POST_INDEX
            $data['post_id']  =$id;
            $data['post_type']="question";
            $data['add_time']=$ardb['add_time'];
            $data['update_time']=time();
            //是否匿名
            // $data['anonymous']
            $data['uid']=$this->getuid();
            model('Base')->getadd('posts_index',$data);
           // echo "string"
            $this->success('操作成功',url('index/question/index')."?id=".$id);
          
        }
       }  
     



    }


}
