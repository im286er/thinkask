<?php
namespace app\ucenter\controller;
use app\common\controller\Base;
class User extends Base
{
    public function login()
    {
    	if($this->getuid()>0){
    		$url = $this->request->only(['gourl'])??"";
    		$url = empty($url)?"/":encode($url);
    		$this->redirect($url);
    	}
      return $this->fetch('ucenter/logo');

    }
    public function reg(){
    	return $this->fetch('ucenter/reg');
    }
}
