<?php
namespace app\index\controller;
use think\Controller;
use think\Request;
use think\Session;
use think\Cookie;
use think\Hook;

class Question extends Controller
{
    protected $request;
    public function _initialize()
    {
      $this->request = Request::instance();

    }
    public function index()
    {
        $id = $this->request->only(['id']);
        $status = $this->request->only(['status']);
        $status = $status['status'];
        $id = (int) $id['id'];
        if($id<1){
            $list = model('Question')->getList($status);
            $this->assign('status',$status);
            $this->assign('list',$list);
            $this->fetch('index/question_list');
        }else{
            //内容和发表人
            $this->assign($re = model('Question')->getDetailById($id));
            //话题
            $this->assign('topic',$topic = model('Question')->getTopicById($id));
            // show($re);
            return $this->fetch('index/question');  
        }
        
    }

}
