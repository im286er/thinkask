<?php
namespace app\index\controller;
use app\common\controller\Base;


class Article extends base
{
    public function _initialize()
    {
    
    }
    public function index()
    {
         $id = $this->request->only(['id']);
        $status = $this->request->only(['status']);
        $status = $status['status'];
        $id = (int) $id['id'];
        if($id<1){
            switch ($status) {
                case 'hot':
                   $order = "views desc";
                    break;
                case 'recommend':
                   $order ="is_recommend desc";   
                    break;

                default:
                    $order="id desc";
                    break;
            }
            $list = model('Article')->getArList($order);
            $this->assign('status',$status);
            $this->assign('list',$list);
            $this->fetch('index/article_list');
        }else{
            //最新
            $this->assign($re = model('Article')->getArById($id));
            // $this->assign('voteinfo',$voteinfo=model('Base')->getall('article_vote',['where'=>"item_uid={$re['uid']}"]));
            // show($voteinfo);
            return $this->fetch('index/article');  
        }
        
    }

}
